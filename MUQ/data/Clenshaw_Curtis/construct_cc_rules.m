for i=0:15
    n = 2^i;
    [x,w] = compute_cc(n);
    
    if(i==0)
   dlmwrite(['cc_rule_' num2str(n) '_w.txt'], w', 'precision', '%1.16f', 'delimiter', ' ');
   dlmwrite(['cc_rule_' num2str(n) '_x.txt'], x, 'precision', '%1.16f', 'delimiter', ' ');
    else
        dlmwrite(['cc_rule_' num2str(n+1) '_w.txt'], w', 'precision', '%1.16f', 'delimiter', ' ');
   dlmwrite(['cc_rule_' num2str(n+1) '_x.txt'], x, 'precision', '%1.16f', 'delimiter', ' ');
    end
   
end


