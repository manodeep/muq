import sys
import matplotlib.pyplot as plt
import numpy as np
import H5Read

if __name__ == "__main__":
    
    if len(sys.argv) != 6:
        print "Error! Usage: python ConvergenceGenzPlot.py <filename> <type> <fnum> <dim> <timescale>"
        exit()
    
    (fevals, LinfErr, L2Err) = H5Read.read( sys.argv[1], 
                                            int(sys.argv[2]), 
                                            int(sys.argv[3]), 
                                            int(sys.argv[4]), 
                                            int(sys.argv[5]) )
    
    mean_fevals = np.mean( fevals, axis=0 )
    mean_L2Err = np.mean( L2Err, axis=0 )

    plt.figure()
    plt.loglog( mean_fevals, mean_L2Err, 'k-o' )
    for i in range(8):
        plt.scatter(fevals[:,i], L2Err[:,i], c='k')
    plt.show(False)
