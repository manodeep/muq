/** \example polynomialchaos-example-2.cpp
 *  <h1>Introduction</h1>
 *  <p> This example will demonstrate the use of adaptive sparse quadrature to compute the integral of the two dimensional function \f\[sin(x_1-0.5)x_2^2 + x_1^4\f\] over \f$[x_1,x_2] \in [-1,1]^2\f$.
 *  </p>
 *
 *  <h3>Line by line explanation</h3>
 *  <p>See example-2.cpp for finely documented code.</p>
 *
 */
//Standard includes
#include <Eigen/Core>
#include <Eigen/QR>
#include <iostream>
#include <MUQ/Modelling/ModPieceTemplates.h>

//A tool from modelling to avoid unnecessary, repetitive calls to a ModPiece.
#include <MUQ/Modelling/CachedModPiece.h>

// The Smolyak quadrature constructs multivariate quadrature rules from 1D quadratures rules.  In this example, we will use a 1D Gauss-Patterson rule defined in this file.
#include "MUQ/Utilities/Quadrature/GaussPattersonQuadrature1D.h"

// Include the variable collection class, which keeps track of what 1D quadrature rules to use in each dimension
#include "MUQ/Utilities/VariableCollection.h"

// Include the SmolyakQuadrature class.  This class does all the work of adaptively stitching together 1D quadrature rules. 
#include "MUQ/Approximation/smolyak/SmolyakQuadrature.h"


//Logging utilities.
#include <MUQ/Utilities/LogConfig.h>


/* use the muq::modelling namespace */
using namespace muq::Modelling;

//Pull in the namespace of the PolynomialChaos package.
using namespace muq::Approximation;
using namespace muq::Utilities;

/* use the std namespace for cout and endl
 */
using namespace std;


// This ModPiece defines the function we want to integrate.
class ExampleModPiece : public OneInputNoDerivModPiece {
public:

  /*
   * This function has two inputs and one output.
   */
  ExampleModPiece() : OneInputNoDerivModPiece(2, 1) {}

  /* We define a default virtual destructor for the model to eliminate issues when the destructor is called from
   * a pointer to a parent of this class (which commonly occurs because of MUQ's extensive use of smart pointers).
   */
  virtual ~ExampleModPiece() = default;

private:

  /*
   * The function that evaluates \f\[sin(x_1-0.5)x_2^2 + x_1^4\f\]
   */
  virtual Eigen::VectorXd EvaluateImpl(Eigen::VectorXd const& input) override
  {
    Eigen::VectorXd output(1);
    output(0) = sin(input(0)-0.5)*pow(input(1),2.0) + pow(input(0),4.0);
    return output;
  }
};


int main(int argc, char **argv)
{
  //Initialization boilerplate for logging. Works regardless of whether GLOG is installed.
  muq::Utilities::InitializeLogging(argc, argv);

  /* Create an instance of the ModPiece, getting a shared_ptr to it. Use C++11's fancy auto to avoid typing the return
   * type, since it's obvious.
   */
  auto trueModel = make_shared<ExampleModPiece>();

  /* Wrap the model with a cache, so it won't call trueModel at the same point twice. We could omit this step, but
   * the (0,0) point is used repeatedly, and that might be expensive when working with expensive functions. */
  auto cachedTrueModel = make_shared<CachedModPiece>(trueModel);
  
  /*
   * We need to tell the adaptive quadrature scheme what 1D quadrature rules to use.  While we could use the same interface as the polynomial
   * chaos construction in example-1.cpp (i.e. Setting the variable type to Legendre of Gaussian, which would yield Gauss-Patterson or Gauss-Hermite
   * quadrature rules), here we will manually specify the quadrature rules for each dimension.  We will use Gauss-Patterson quadrature because it is nested
   * and thus well-suited for adaptive quadrature.  Other rules can of course be used, look at children of the muq::Utilities::QuadratureFamily1D class
   * for a complete list of options.
   */

  // Define the 1D quadrature rule that will be the basis for the adaptive multivariate quadrature scheme
  auto quadRule = make_shared<GaussPattersonQuadrature1D>();
  
  // Create a variable collection that will hold the quadrature rules for each dimension.
  auto varCollection = make_shared<VariableCollection>();
  
  // Add the Gauss-Patterson rule for the first component.  Typically, the second argument to PushVariable would define the polynomial family to use in a PCE expansion.  Here we are only interested in integrating the function, so a nullptr is passed instead.
  varCollection->PushVariable("x1", nullptr, quadRule);
  
  // Add the Gauss-Patterson rule for the second component.  Note that the first and second components do not need to use the same rule.
  varCollection->PushVariable("x2", nullptr, quadRule);

  /* Create an instance of the adaptive SmolyakQuadrature class.  The first argument is the variable collection telling the Smolyak class what 1D rules to use.  The second argument is the function we want to integrate.*/
  SmolyakQuadrature quad(varCollection, cachedTrueModel);
  
  /* Set the initial quadrature order to use.  With initialOrder=3, the adaptive scheme will start with a third order GaussPatterson rule in each dimension. */
  int initialOrder = 3;
  
  /* Set the error tolerance.  Internally, we have an error indicator for the quadrature progress.  When that error indicator is less than this threshhold, the adaptive algorithm will terminate. */
  double errorTol = 1e-5;
  
  /* Perform the integration.  Using the initial quadrature order and error tolerance, run the adaptive integration algorithm.  The result will be a vector containing the integral.  The dimension of the output vector is the same as the output dimension of the function we are integrating.  In this example, the output size will be 1 because we are integrating a scalar function.
  */
  Eigen::VectorXd integral = quad.StartAdaptiveToTolerance(initialOrder, errorTol);

  // Report the numerically compute integral.
  cout << "The numerically computed integral of f(x) is " << integral(0) << endl;
  
  // Now report an analytic result
  cout << "The true analytical integral is " << (cos(-1.5)-cos(0.5))*2.0/3.0 + 4.0/5.0 << endl;
  
}

