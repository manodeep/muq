
#ifndef COVKERNELPYTHON_H_
#define COVKERNELPYTHON_H_

#include <boost/python.hpp>
#include <boost/python/def.hpp>
#include <boost/python/class.hpp>

#include "MUQ/Utilities/python/PythonTranslater.h"

#include "MUQ/Geostatistics/CovKernel.h"

namespace muq {
namespace Geostatistics{
/// Tell python about muq::geostats::CovKernel
void ExportCovKernel();
} // namespace geostats
} // namespace muq

#endif // ifndef COVKERNELPYTHON_H_
