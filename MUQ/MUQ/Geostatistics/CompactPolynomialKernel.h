
#ifndef COMPACTPOLYNOMIALKERNEL_H
#define COMPACTPOLYNOMIALKERNEL_H

#include "MUQ/Geostatistics/IsotropicCovKernel.h"

namespace muq {
namespace Geostatistics {
/**
 *  @class CompactPolynomialKernel
 *
 *  Wendland compact polynomial kernel, taken from:
 *  http://www.gaussianprocess.org/gpml/chapters/RW4.pdf
 *
 *  Currently the kpp_D_2 kernel
 *
 *  The length scale is built in, that is, k=0 beyond r=1.
 *
 */
class CompactPolynomialKernel : public IsotropicCovKernel {
public:

  REGISTER_KERNEL_CONSTRUCTOR(CompactPolynomialKernel)
  /** Construct the kernel from the characteristic length scale, power, and variance.
   *  @param[in] LengthIn characteristic length scale
   *  @param[in] PowerIn power in exponential decay
   *  @param[in] VarIn variance - i.e. diagonal of covariance matrix
   */
  CompactPolynomialKernel(boost::property_tree::ptree& ptree);

  CompactPolynomialKernel(double VarIn = 1);

  virtual ~CompactPolynomialKernel() = default;

  virtual double          DistKernel(double d) const override;

  /** Set the covariance kernel parameters (variance) from a vector of parameters.
   *  @param[in] theta MuqVector or parameter values for the covariance kernel.
   */
  virtual void            SetParms(const Eigen::VectorXd& theta) override;

  virtual void            GetGrad(double d, Eigen::VectorXd& grad) const override;

  virtual Eigen::VectorXd GetParms() override
  {
    Eigen::VectorXd Out(1); Out(0) = logVar; return Out;
  }

  virtual double KernelRadialDerivative(double const r) override;

protected:

  double logVar;
};

// class CovKernel
} // namespace Geostatistics
} // namespace muq


#endif // COMPACTPOLYNOMIALKERNEL_H
