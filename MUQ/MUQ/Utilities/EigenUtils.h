
#ifndef EIGENUTILS_H_
#define EIGENUTILS_H_

#include <assert.h>
#include <iostream>
#include <fstream>
#include <sstream>

#include <Eigen/Core>


#include <boost/serialization/split_free.hpp>
#include <boost/serialization/export.hpp>
#include <boost/serialization/tracking.hpp>

#include <boost/archive/text_iarchive.hpp>
#include <boost/archive/text_oarchive.hpp>

/**
 * This directory includes some helpful utilities for the eigen toolkit,
 * especially regarding predicates for testing.
 */

namespace Eigen {
///Define some unsigned types
typedef Matrix<unsigned int, Dynamic, Dynamic> MatrixXu;
typedef Matrix<unsigned int, Dynamic, 1>       VectorXu;
typedef Matrix<unsigned int, 1, Dynamic>       RowVectorXu;
}


namespace muq {
namespace Utilities {
/**
 * Find non zero entries in a column vector
 **/
Eigen::VectorXu FindNonZeroInVector(Eigen::VectorXd const& input);

template<typename derived>
void            PrintMatrixToFile(Eigen::MatrixBase<derived> const& input, std::string const& filename)
{
  std::ofstream outFile(filename.c_str());

  assert(outFile.good());

  outFile.precision(15);
  outFile << input;
  outFile.close();
}

template<typename derivedA, typename derivedB>
bool MatrixEqual(Eigen::MatrixBase<derivedA> const& a, Eigen::MatrixBase<derivedB> const& b)
{
  //check rows and cols equal
  if ((a.cols() != b.cols()) || (a.rows() != b.rows())) {
    return false;
  }

  //return true if they're all the same
  return (a.array() == b.array()).all();
}

template<typename derivedA, typename derivedB>
bool MatrixApproxEqual(Eigen::MatrixBase<derivedA> const& a, Eigen::MatrixBase<derivedB> const& b, double const tol)
{
  if ((a.cols() != b.cols()) || (a.rows() != b.rows())) { //check the length first
    return false;
  }

  //return true if all the differences are less than the tol

  return ((a - b).array().abs() < tol).all();
}

///Apply a function to the columns of the input matrix. Assumes the input dimensionality has been checked.
Eigen::MatrixXd ApplyVectorFunction(std::function<Eigen::VectorXd(
                                                    Eigen::VectorXd const&)> fn, unsigned const outputDim,
                                    Eigen::MatrixXd const& inputs);

///Call an external executable, passing files through /tmp/.
Eigen::MatrixXd ApplyExecutableFunction(std::string const    & executable,
                                        unsigned const         outputDim,
                                        Eigen::MatrixXd const& inputs);


template<typename _Scalar, int _Rows, int _Cols>
void SaveMat(const std::string& filename, Eigen::Matrix<_Scalar, _Rows, _Cols> const& mat)
{
  // create a text archive
  std::ofstream ofs(filename.c_str());

  assert(ofs.good());
  {
    boost::archive::text_oarchive oa(ofs);
    oa << mat;
  }
  ofs.close();
}

template<typename _Scalar, int _Rows, int _Cols>
void LoadMat(const std::string& filename, Eigen::Matrix<_Scalar, _Rows, _Cols>& mat)
{
  // create a text archive
  std::ifstream ifs(filename.c_str());

  assert(ifs.good());
  {
    boost::archive::text_iarchive ia(ifs);
    ia >> mat;
  }
  ifs.close();
}
} // namespace Utilities
} // namespace MUQ


///This block makes Mat, Row, and Col types serializable using the built-in binary format.
namespace boost {
namespace serialization {
// Copy for MatrixXd  *************************************
template<class Archive, typename _Scalar, int _Rows, int _Cols>
void save(Archive& ar, Eigen::Matrix<_Scalar, _Rows, _Cols> const& mat, const unsigned int version)
{
  typename  Eigen::Matrix<_Scalar, _Rows, _Cols>::Index numRows = mat.rows(), numCols = mat.cols();

  ar << numRows;
  ar << numCols;

  for (typename  Eigen::Matrix<_Scalar, _Rows, _Cols>::Index i = 0; i < mat.rows(); ++i) {
    for (typename  Eigen::Matrix<_Scalar, _Rows, _Cols>::Index j = 0; j < mat.cols(); ++j) {
      //store value explicitly as a scalar, the save it
      _Scalar tmp = mat(i, j);
      ar << tmp;
    }
  }
}

template<class Archive, typename _Scalar, int _Rows, int _Cols>
void load(Archive& ar,  Eigen::Matrix<_Scalar, _Rows, _Cols>& mat, const unsigned int version)
{
  typename  Eigen::Matrix<_Scalar, _Rows, _Cols>::Index numRows = 0, numCols = 0;
  ar >> numRows;
  ar >> numCols;

  mat.resize(numRows, numCols); // resize the derived object

  for (typename  Eigen::Matrix<_Scalar, _Rows, _Cols>::Index i = 0; i < mat.rows(); ++i) {
    for (typename  Eigen::Matrix<_Scalar, _Rows, _Cols>::Index j = 0; j < mat.cols(); ++j) {
      //read value and store it
      _Scalar tmp;
      ar    >> tmp;
      mat(i, j) = tmp;
    }
  }
}

template<class Archive, typename _Scalar, int _Rows, int _Cols>
void serialize(Archive& ar, Eigen::Matrix<_Scalar, _Rows, _Cols>& mat, const unsigned int file_version)
{
  split_free(ar, mat, file_version);
}
} // namespace serialization
} // namespace boost

BOOST_CLASS_TRACKING(Eigen::MatrixXd,    boost::serialization::track_never); BOOST_CLASS_TRACKING(Eigen::MatrixXu,
                                                                                                  boost::serialization::track_never);
BOOST_CLASS_TRACKING(Eigen::VectorXd,    boost::serialization::track_never);
BOOST_CLASS_TRACKING(Eigen::VectorXu,    boost::serialization::track_never);
BOOST_CLASS_TRACKING(Eigen::RowVectorXd, boost::serialization::track_never);
BOOST_CLASS_TRACKING(Eigen::RowVectorXu, boost::serialization::track_never);


#endif /* EIGENUTILS_H_ */
