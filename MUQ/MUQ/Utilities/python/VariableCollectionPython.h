#ifndef VARIABLECOLLECTIONPYTHON_H_
#define VARIABLECOLLECTIONPYTHON_H_

#include <boost/python.hpp>
#include <boost/python/def.hpp>
#include <boost/python/class.hpp>

namespace muq {
  namespace Utilities {
    void ExportVariableCollection();
  }
}

#endif