
#ifndef EIGENTESTUTILS_H_
#define EIGENTESTUTILS_H_

#include <Eigen/Core>
#include <gtest/gtest.h>

#include <MUQ/Utilities/EigenUtils.h>

///A gtest predicate for exact equality, used: EXPECT_PRED_FORMAT2(MatrixEqual,expected,actual)
namespace muq {
namespace Utilities {
template<typename derivedA, typename derivedB>
::testing::AssertionResult MatrixEqual(const char *m_expr, const char *n_expr, derivedA m, derivedB n)
{
  if (MatrixEqual(m, n)) {
    return ::testing::AssertionSuccess();
  }

  return ::testing::AssertionFailure() << "Value of: " << m_expr << " == " << n_expr << "\nExpected: \n" << m <<
      "\nActual: \n" << n;
}

///A gtest predicate for approximate equality, used: EXPECT_PRED_FORMAT3(MatrixApproxEqual,expected,actual,eps)
template<typename derivedA, typename derivedB>
::testing::AssertionResult MatrixApproxEqual(const char *m_expr,
                                             const char *n_expr,
                                             const char *eps_expr,
                                             derivedA    m,
                                             derivedB    n,
                                             double      eps)
{
  if (MatrixApproxEqual(m, n, eps)) {
    return ::testing::AssertionSuccess();
  }

  return ::testing::AssertionFailure() << "Value of: " << m_expr << " == " << n_expr << " to within eps = " << eps <<
      "\nExpected: \n" << m << "\nActual: \n" << n;
}
}
}
#endif //EIGENTESTUTILS_H_
