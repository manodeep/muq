#ifndef MULTIINDEXPOOL_H_
#define MULTIINDEXPOOL_H_

#include <vector>
#include <memory>

#include "MUQ/Utilities/multiIndex/MultiIndex.h"

#include <boost/archive/text_oarchive.hpp>
#include <boost/archive/text_iarchive.hpp>
#include <boost/serialization/vector.hpp>
#include <boost/serialization/map.hpp>
#include <boost/serialization/set.hpp>
#include <boost/serialization/utility.hpp>
#include <boost/serialization/shared_ptr.hpp>

namespace muq {
  namespace Utilities{
    
    class MultiIndexPool{
      friend class boost::serialization::access;
    public:
      
      MultiIndexPool() : currentId(-1){};
      //MultiIndexPool(std::shared_ptr<MultiIndexPool> otherPool);
      
      /** Get a new ID for a multiindex set.  This increments the currentId variable.  The ID will be used in calls to the SetActive function.
          @return An integer holding the new ID.
       */
      unsigned int GetNextId(){currentId++; return currentId;};
      
      /** Add a multiindex to this pool. The multiindex will be inactive until the SetActive function is used.
          @param[in] newMulti A shared_ptr to the multiindex we want to add.
          @return An integer into the allMultis vector with the location of this multiindex.
       */
      unsigned int AddMulti(std::shared_ptr<MultiIndex> newMulti);
      
      /** Store that a certain multiindex is active in a particular set.
          @param[in] globalInd The index of the multiindex in the allMultis vector
          @param[in] setId The ID of the MultiIndexSet that calls this function.
          @param[in] activeInd The active index in the set setId corresponding to the globalInd in this pool.
       */
      void SetActive(int globalInd, int setId, int activeInd);
      
      
      /** Get the multiindex stored at a particular linear index.
          @param[in] globalInd The linear index into the allMultis function.
          @return A constant shared_ptr to the multiindex.
       */
      const std::shared_ptr<MultiIndex> GetMulti(int globalInd) const{return allMultis.at(globalInd);};
      
      std::shared_ptr<MultiIndex>& at(int globalInd){return allMultis.at(globalInd);};
      const std::shared_ptr<MultiIndex>& at(int globalInd) const{return allMultis.at(globalInd);};
      
      std::shared_ptr<MultiIndex>& operator[](int globalInd){return allMultis[globalInd];};
      const std::shared_ptr<MultiIndex>& operator[](int globalInd) const{return allMultis[globalInd];};
      
      unsigned int size() const{return allMultis.size();};
      
      bool IsActive(unsigned int i) const{return all2active.at(i).size()>0;};
      const std::vector<std::pair<unsigned int, unsigned int>>& GetActiveIndices(unsigned int i) const{return all2active.at(i);};
      
    private:
      int currentId;
      
      /** 0-index is the global linear multiindex, then comes a list of the all the sets that have that term activated.  The first entry in the pair is the ID of the set and the second is the active index in that set corresponding to this multiindex.  For example, all2active[1][0].first corresponds to the ID of the first MultiIndexSet that used the second multiindex as active.  Similarly, all2active[1][0].second corresponds to the active index in that set which maps to the multiindex with global index 1.
       */
      std::vector<std::vector<std::pair<unsigned int, unsigned int>>> all2active;
      
      /// A vector holding all of the multiindices
      std::vector<std::shared_ptr<MultiIndex>> allMultis;
      
      /// A map from the multiindex to it's global index.  This is used in the AddMulti function to check if a multiindex already exists in this pool
      std::map<std::shared_ptr<MultiIndex>, unsigned int, MultiPtrComp> multi2all;
      
      
      template<class Archive>
      void serialize(Archive & ar, const unsigned int version){
        ar & currentId;
        ar & all2active;
        ar & allMultis;
        ar & multi2all;
      };
      
    };
  }
}



#endif