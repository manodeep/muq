#ifndef EXPGROWTHQUADRATUREFAMILY1D_H_
#define EXPGROWTHQUADRATUREFAMILY1D_H_

#include <boost/serialization/access.hpp>

#include "MUQ/Utilities/Quadrature/QuadratureFamily1D.h"

namespace muq {
namespace Utilities {
/**
 * This class is designed to provide a different growth rule for 1D
 * quadrature rules. For example, Gaussian quadrature rules progress
 * linearly in size and order, but we might prefer an exponential
 * growth, to simulate something like the behavior of a nested rule.
 *
 * The constructor takes any other quadrature rule to rescale. This class
 * simply calls that rule with rescaled order.
 *
 * New order progression: 1 2 4 8 16 ...
 */
class ExpGrowthQuadratureFamily1D : public QuadratureFamily1D {
public:

  ///A smart pointer
  typedef std::shared_ptr<ExpGrowthQuadratureFamily1D> Ptr;

  ///Constructor takes the rule you wish to rescale the growth of.
  ExpGrowthQuadratureFamily1D(QuadratureFamily1D::Ptr inputRule);

  virtual ~ExpGrowthQuadratureFamily1D();

  ///Get the order of the polynomial this order integration rule is good for.
  virtual unsigned int GetPrecisePolyOrder(unsigned int const order) const;

private:

  ///Private default constructor for serialization
  ExpGrowthQuadratureFamily1D();

  ///Make class serializable
  friend class boost::serialization::access;

  ///Make class serializable
  template<class Archive>
  void serialize(Archive& ar, const unsigned int version);

  ///Implementations must provide a way of deriving the nth rule.

  /**
   * Compute the order rule, and return the nodes and weights in the rowvecs.
   */
  virtual void ComputeNodesAndWeights(unsigned int const                   order,
                                      std::shared_ptr<Eigen::RowVectorXd>& nodes,
                                      std::shared_ptr<Eigen::RowVectorXd>& weights) const;

  ///The base quadrature rule to rescale
  QuadratureFamily1D::Ptr baseQuadrature1DRule;
};
}
}

#endif /* EXPGROWTHQUADRATUREFAMILY1D_H_ */
