
#ifndef GAUSSPROBABILISTHERMITEQUADRATURE1D_H_
#define GAUSSPROBABILISTHERMITEQUADRATURE1D_H_

#include <boost/serialization/access.hpp>

#include <Eigen/Core>

#include "MUQ/Utilities/Quadrature/GaussianQuadratureFamily1D.h"

namespace muq {
namespace Utilities {
/**
 * Impelements 1D gaussian quadrature for exp(-0.5*x^2) weighted integrals by extending the
 * abstract class GaussianQuadratureFamily1D with the ProbabilistHermite polynomials. */
class GaussProbabilistHermiteQuadrature1D : public GaussianQuadratureFamily1D {
public:

  GaussProbabilistHermiteQuadrature1D() = default;
  virtual ~GaussProbabilistHermiteQuadrature1D() = default;

  ///Implements the function that provides the monic  coefficients.
  virtual Eigen::RowVectorXd GetMonicCoeff(unsigned int const j) const;

  ///Implements the function that provides the integral of the weight function over the domain.
  virtual double             IntegralOfWeight() const;

private:

  ///Make class serializable
  friend class boost::serialization::access;

  ///Make class serializable
  template<class Archive>
  void serialize(Archive& ar, const unsigned int version);
};
}
}
#endif /* GAUSSPROBABILISTHERMITEQUADRATURE1D_H_ */
