#ifndef GAUSSPATTERSONQUADRATURE1D_H_
#define GAUSSPATTERSONQUADRATURE1D_H_

#include <boost/serialization/access.hpp>

#include "MUQ/Utilities/EigenUtils.h"

#include "MUQ/Utilities/Quadrature/QuadratureFamily1D.h"

namespace muq {
namespace Utilities {
/**
 * Implements Gauss-Patterson Quadrature, which is based on the 3-point Gauss-Legendre
 * rule, in the interval [-1,1] (w(x) = 1). Nodes are pre-computed and indexed by level.
 * Levels 1-8,  corresponding to 1, 3, 7, 15, 31, 63, 127, or 255 points are available.
 *
 * There are two styles of indexing. Standard has the above eight indices. Non-standard
 * mimics the indexing style of Gaussian quadrature, so 1-1pt, 2-3pt, 3-3pt, 4-7pt, 5-7pt, 6-7pt, and so on.
 * Non-standard indexing is not advised and is included for the HYCOM analysis.
 **/
class GaussPattersonQuadrature1D : public QuadratureFamily1D {
public:

  GaussPattersonQuadrature1D(bool standardIndexing = true);
  virtual ~GaussPattersonQuadrature1D();

  typedef std::shared_ptr<GaussPattersonQuadrature1D> Ptr;

  ///Uses a lookup table to return the approximate orders

  /**
   * Taken from http://people.sc.fsu.edu/~jburkardt/cpp_src/patterson_rule/patterson_rule.html
   * Uses the actual accuracy, using the symmetry of the rule, which buys us +1 for all
   * but the first rule.
   **/
  unsigned int GetPrecisePolyOrder(unsigned int const order) const override;

  /// Returns the maximum level available in ComputeNodesAndWeights
  unsigned int GetMaximumPolyOrder() const override{return 7;};
  
private:

  bool standardIndexing;

  ///Make class serializable
  friend class boost::serialization::access;

  template<class Archive>
  void         serialize(Archive& ar, const unsigned int version);

  ///Return the pre-computed nodes and weights
  virtual void ComputeNodesAndWeights(unsigned int const                   order,
                                      std::shared_ptr<Eigen::RowVectorXd>& nodes,
                                      std::shared_ptr<Eigen::RowVectorXd>& weights) const;
};
}
}
#endif /* GAUSSPATTERSONQUADRATURE1D_H_ */
