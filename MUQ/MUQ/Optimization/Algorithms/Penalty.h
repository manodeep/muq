
#ifndef _Penalty_h
#define _Penalty_h

#include "MUQ/Optimization/Algorithms/OptAlgBase.h"

namespace muq {
namespace Optimization {
/** @class Penalty
 *  @ingroup Optimization
 *  @brief Quadratic penalty approach for solving general constrained nonlinear optimization problems
 *  @details This class uses a penalty method to transform a constrained optimization problem into an unconstrained
 * problem. The idea is to create a series of unconstrained subproblems that are solved by any unconstrained optimizer.
 *  These uncsontrained problems have an increasing penalty for violating the constraints.  When the penalty is large
 * enough, the solution to the unconstrained problem should converge to the true constrained solution.  Note that if the
 * optimizer requires derivatives, then derivatives of both the constraints and objective will be required.  Similarly,
 * if the optimizer is a derivative free algorithm, then no derivative information about the constraints or the
 * objective will be used.  Furthermore, the way penalty methods handle constraints does not preclude infeasible
 * evaluations.  If the objective cannot be evaluated for at an infeasible point, you should use an alternative
 * constraint handler, like muq::optimization::Barrier.
 */
class Penalty : public OptAlgBase {
public:

  REGISTER_OPT_CONSTRUCTOR(Penalty)
  /** Construct a penalty based optimization solver from the settings in a ptree.  The ptree should use some base
   * optimization algorithm as Opt.Method and should have an Opt.ConstraintHandler set to Penalty.
   *  @param[in] ProbPtr A shared pointer to the optimization problem object
   *  @param[in] properties Parameter list containing optimization method and parameters
   */
  Penalty(std::shared_ptr<OptProbBase> ProbPtr, boost::property_tree::ptree& properties);


  /** Solve the optimization problem using x0 as a starting point for the iteration.
   *  @param[in] x0 The initial point for the optimizer.
   *  @return A vector holding the best point found by the optimizer.  Note that the optimizer may not have converged
   * and the GetStatus() function should be called to ensure that the algorithm converged and this vector is meaningful.
   */
  virtual Eigen::VectorXd solve(const Eigen::VectorXd& x0) override;

protected:

  /// the current penalty weight
  double penaltyCoeff;

  /// how multiplicative factor increasing penaltyCoeff at each outer iteration
  double penaltyScale;

  // the maximum number of
  int maxOuterIts;

  // property list for unconstrained solver
  boost::property_tree::ptree uncProperties;

private:

  REGISTER_OPT_DEC_TYPE(Penalty)
};

//class Penalty
} // namespace Optimization
} // namespace muq

#endif // ifndef _Penalty_h
