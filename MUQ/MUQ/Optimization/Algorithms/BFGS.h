
#ifndef _BFGS_h
#define _BFGS_h

#include "MUQ/Optimization/Algorithms/GradLineSearch.h"

namespace muq {
namespace Optimization {
/** @class BFGS_Line
 *   @ingroup Optimization
 *   @brief BFGS method with linesearch
 *   @details This class implements the usual full storage BFGS algorithm using a line search to ensure global
 *      convergence.
 */
class BFGS_Line : public GradLineSearch {
public:

  REGISTER_OPT_CONSTRUCTOR(BFGS_Line)
  /** parameter list constructor
   *  @param[in] ProbPtr A shared_ptr to the optimization problem we want to solve.
   *  @param[in] properties A parameter list containing any parameters that were set by the user.  If the parameters
   * were
   *     not set, default values will be used.
   */
  BFGS_Line(std::shared_ptr<OptProbBase> ProbPtr, boost::property_tree::ptree& properties);

  /** step function, return the BFGS direction
   *  @return A vector holding the BFGS direction scaled by the step size.
   */
  virtual Eigen::VectorXd step();

protected:

  // the gradient at the previous step
  Eigen::VectorXd OldGrad;
  Eigen::VectorXd OldState;

  // the approximate inverse hessian matrix
  Eigen::MatrixXd ApproxHessInv;

  // how many times has the step function been called?
  int stepCalls;

private:

  REGISTER_OPT_DEC_TYPE(BFGS_Line)
};
} // namespace Optimization
} // namespace muq
#endif // ifndef _BFGS_h
