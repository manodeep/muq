#ifndef POISEDNESSCOST_H_
#define POISEDNESSCOST_H_

#include "MUQ/Utilities/multiIndex/MultiIndexSet.h"

#include "MUQ/Optimization/Problems/OptProbBase.h"

#include "MUQ/Utilities/VariableCollection.h"

namespace muq {
namespace Approximation {
    /// The cost function for lambda-poisedness
    /**
       This is maximized in a ball to locally bound the error
     */
class PoisednessCost : public muq::Optimization::OptProbBase {
    public:

      /**
	 @param[in] terms The terms in the polynomial expansion 
	 @param[in] variables Variables to specify the tensor space
	 @param[in] V The Vandermonde matrix
	 @param[in] K The kernel matrix
	 @param[in] dim The dimension of the points
       */
PoisednessCost(std::shared_ptr<muq::Utilities::MultiIndexSet> const& terms, std::shared_ptr<muq::Utilities::VariableCollection> const& variables, Eigen::MatrixXd const& V, Eigen::MatrixXd const& K, unsigned int const dim);

      /// Compute the norm of the Lagrange polynomials
      /**
	 @param[in] in The point
	 \return Norm of the Lagrange polynmials
       */
      virtual double eval(Eigen::VectorXd const& in) override;

    private:

      /// Evaluate the basis functions at the point
      Eigen::VectorXd EvaluateBasis(Eigen::VectorXd const& point);

      /// Compute the norm of the Lagrange polynomials gradient
      /**
	 @param[in] in The point
	 @param[out] gradient The graident
	 \return Max distance from neighbors
       */
      virtual double grad(Eigen::VectorXd const& in, Eigen::VectorXd& gradient) override;

      /// Cholesky decomposition of the matix we need to invert
      Eigen::MatrixXd cholDecomp;

      /// The Vandermonde matrix 
      const Eigen::MatrixXd Vand;

      /// The terms in the polynomial expansion
      const std::shared_ptr<muq::Utilities::MultiIndexSet> terms;

      /// Variables to specify the tensor space
      const std::shared_ptr<muq::Utilities::VariableCollection> variables;
    };
  } // namespace Approximation 
} // namespace muq

#endif
