#ifndef POLYNOMIALREGRESSOR_H_
#define POLYNOMIALREGRESSOR_H_

#include <Eigen/QR>

#include "MUQ/Utilities/multiIndex/MultiIndexSet.h"

#include "MUQ/Utilities/VariableCollection.h"

#include "MUQ/Approximation/Regression/Regressor.h"

namespace muq {
namespace Approximation {
/// Polynomial regression using a Vandermonde matrix
/**
 *  Polynomial regression in multiple dimensions.  The function muq::Approximation::PolynomialRegessor::Fit fits the
 * polynomial to data using least squares.  The matrix used in the least squares problem is a Vandermonde matrix
 * computed with either Legendre or Hermite polynomials (Legendre are the default).
 */
class PolynomialRegressor : public Regressor {
public:

  /// Constructor
  /**
   *  Ptree options:
   *  <ol type="1">
   *  <li> Order of the polynomial regression (<EM>PolynomialRegressor.Order</EM>)
   *      <ul>
   *      <li> Defaults to 0
   *      </ul>
   *  <li> The polynomial basis (<EM>PolynomialRegressor.Basis</EM>)
   *      <ul>
   *      <li> "Legendre" (default) or "Hermite"
   *      </ul>
   *  <li> Use cubic weighting function? (<EM>PolynomialRegressor.CubicWeights</EM>)
   *      <ul>
   *      <li> Defaults to false
   *      <li> True: Use a cubic weighting function based on distance.  If \f$n\f$ points are required to interpolate,
   * the closest \f$n\f$ points are weighted 1.  After those points, the weights of the other points decay cubically.
   *      <li> False: All points weighted equally
   *      <li> Only supported if the order is less than 3
   *      </ul>
   *  <li> Cross validate fit? (<EM>PolynomialRegressor.CrossValidate</EM>)
   *      <ul>
   *      <li> Defaults to false
   *      <li> True: perform leave-one-out cross validation
   *      <li> False: do not cross validate
   *      </ul>
   *  <li> Maximum cross validation samples (<EM>PolynomialRegressor.MaxCVSamples</EM>)
   *      <ul>
   *      <li> Defaults to 30
   *      <li> Leave-one-out cross validation leaves out at most this number of points
   *      </ul>
   *  </ol>
   *  @param[in] inDim The input size.
   *  @param[in] outDim The output size.
   *  @param[in] para Ptree with polynomial regressor options
   */
  PolynomialRegressor(unsigned int const inDim, unsigned int const outDim, boost::property_tree::ptree const& para);

  virtual ~PolynomialRegressor() = default;
  
#if MUQ_PYTHON == 1
  /// Python constructor
  /**
   *  @param[in] inDim The input size.
   *  @param[in] outDim The output size.
   *  @param[in] para Ptree with polynomial regressor options
   */
  PolynomialRegressor(unsigned int const inDim, unsigned int const outDim, boost::python::dict const& para);
#endif // MUQ_PYTHON == 1

  // so that the child can call Fit(xs, ys)
  using Regressor::Fit;

  /// Fit the approximation (regression) given input points and their outputs
  /**
   *  Fit a polynomial to a set of points.
   *  @param[in] xs The points (each column is a point)
   *  @param[in] ys The output at each point (each column is an output)
   *  @param[in] center Shift the origin to this point.
   */
  virtual void Fit(Eigen::MatrixXd const& xs, Eigen::MatrixXd const& ys, Eigen::VectorXd const& center) override;


  unsigned     GetNumberOfCVFits() const;

  /// Evaluate the cross validations fits
  /**
   *  @param[in] inputs The input point to the regressor
   *  \return Each column is the regression resulting from leave-one-out cross validation
   */
  Eigen::MatrixXd AllCrossValidationEvaluate(Eigen::VectorXd const& inputs) const;

  ///Get the evaluation of one of the cross validation fits
  /**
   * @param inputs The input point to the regressor
   * @param i The index of the cross validation fit to use
   * @return Eigen::VectorXd The CV evaluation
   */
  Eigen::VectorXd OneCrossValidationEvaluate(Eigen::VectorXd const& inputs, unsigned const i) const;

  ///Get the Jacobian of one of the cross validation fits
  /**
   * @param inputs The input point to the regressor
   * @param i The index of the cross validation fit to use
   * @return Eigen::MatrixXd The CV Jacobian
   */
  Eigen::MatrixXd OneCrossValidationJacobian(Eigen::VectorXd const& inputs, unsigned const i) const;

#if MUQ_PYTHON == 1
  /// Python cross validation evaluate
  /**
   *  @param[in] inputs The input point to the regressor
   *  \return Each column is the regression resulting from leave-one-out cross validation
   */
  boost::python::list PyCrossValidationEvaluate(boost::python::list const& inputs) const;
#endif // MUQ_PYTHON == 1

  /// Get the number of points required to get an exact interpolation
  /**
   *  \return The number of points required to get an exact interpolation
   */
  int NumPointsForInterpolation() const;

  /// Turn on cubic weighting
  /**
   *  Set muq::Approximation::PolynomialRegressor::cubicWeights to true.
   */
  void CubicWeightingOn();

  /// Turn off cubic weighting
  /**
   *  Set muq::Approximation::PolynomialRegressor::cubicWeights to false.
   */
  void CubicWeightingOff();

  /// Turn on cross validation
  /**
   *  Set muq::Approximation::PolynomialRegressor::crossValidate to true.
   */
  void CrossValidationOn();

  /// Turn off cross validation
  /**
   *  Set muq::Approximation::PolynomialRegressor::crossValidate to false.
   */
  void CrossValidationOff();

  /// Set the maximum number of cross validation samples
  /**
   *  @parm[in] cvSamps New maximum number of cross validation samples
   */
  void SetMaxCVSamples(unsigned int const cvSamps);

  /// Get the regression order
  /**
   *  \return The regression order
   */
  unsigned int RegressionOrder() const;

  /// Compute the lambda poisedness constant
  /**
   *  Compute the lambda poisedness constant at a point within a given radius
   *  @param[in] xs The points (each column is a point)
   *  @param[in] point The point where we want the lambda poisedness constant
   *  @param[in] para A ptree containing optimization options
   *  \return The lambda poisedness constant
   */
  double PoisednessConstant(Eigen::MatrixXd const& xs, Eigen::VectorXd const& point,
                            boost::property_tree::ptree para) const;

  /// Compute the error contribution from the noisiness of the function
  /**
   *  @param[in] xs The points (each column is a point)
   *  @param[in] point The point where we want the lambda poisedness constant
   *  @param[in] var The variance of the samples
   */
  double NoisyError(Eigen::MatrixXd const& xs, Eigen::VectorXd const& point, double const var) const;

private:

  /// The available polynomial basis
  enum BasisTypes {
    /// Legendre polynomials
    Legendre,

    /// Hermite polynomials
    Hermite
  };

  /// A map from string to the basis type; either Legendre or Hermite
  static std::map<std::string, BasisTypes> basisTypeMap;

  /// Center the points
  /**
   *  @param[in,out] xs The points before and after centering
   *  \return The maximum distance between point \f$\mathbf{x}_i\f$ and the center
   */
  void CenterPoints(Eigen::MatrixXd& xs);

  /// Construct the Vandermonde matrix
  /**
   *  @param[in] xs The points (each column is a point)
   */
  Eigen::MatrixXd VandermondeMatrix(Eigen::MatrixXd const& xs) const;

  ///Helper function performs evaluation with a specified set of coeff - unifies most code for nominal and cv
  // evaluations.
  /**
   *  @param[in] inputs The point to evaluate at
   *  @param[in] coeffToUse The coefficients to use
   **/
  Eigen::VectorXd EvaluateWithCoefficients(Eigen::VectorXd const& inputs, Eigen::MatrixXd const& coeffToUse) const;


  ///Helper function performs evaluation with a specified set of coeff - unifies most code for nominal and cv
  // evaluations.
  /**
   *  @param[in] inputs The point to evaluate at
   *  @param[in] coeffToUse The coefficients to use
   **/
  Eigen::MatrixXd JacobianWithCoefficients(Eigen::VectorXd const& inputs, Eigen::MatrixXd const& coeffToUse) const;


  /// Evaluate the regression polynomial
  /**
   *  Fit must be called before this polynomial can be evaluated.
   *  @param[in] inputs The point where we want to evaluate the polynomial
   */
  virtual Eigen::VectorXd EvaluateImpl(Eigen::VectorXd const& inputs) override;

  /// Evaluate the regression polynomial's Jacobian
  /**
   *  Fit must be called before this polynomial can be evaluated.
   *  @param[in] inputs The point where we want to evaluate the polynomial
   */
  virtual Eigen::MatrixXd JacobianImpl(Eigen::VectorXd const& inputs) override;

  /// Compute the weights for each point
  /**
   *  If \f$n\f$ points are required for interpolation, the closest \f$n\f$ points are weighted 1.  The weights of all
   * other points decays cubically according to distance from the center.  Requires the order to be less than 3.
   *  @param[in] xs The points (after centering)
   *  \return The weights
   */
  Eigen::VectorXd CubicWeights(Eigen::MatrixXd const& xs) const;

  /// Preform the cross validation
  /**
   *  @param[in] solver The QR decomposition of the full Vandermonde matrix
   *  @param[in] rhs The right hand side using all the data
   */
  void CrossValidate(Eigen::ColPivHouseholderQR<Eigen::MatrixXd> const& solver, Eigen::MatrixXd const& rhs);

  /// The order of the regression
  const unsigned int order;

  /// Use cubic weighting?
  bool cubicWeights;

  /// Perform cross validation?
  bool crossValidate;

  /// Maximum number of cross validation samples
  unsigned int maxCVSamps;

  std::vector<Eigen::MatrixXd> cvFits;

  /// The terms in the polynomial expansion
  std::shared_ptr<muq::Utilities::MultiIndexSet> terms;

  /// Variables to specify the tensor space
  std::shared_ptr<muq::Utilities::VariableCollection> variables;

  /// The coefficients after fitting
  Eigen::MatrixXd coefficients;

  /// The current center location
  Eigen::VectorXd currentCenter;

  //The current radius of the fit.  
  double currentRadius=0.0;
  
};
}
}

#endif // ifndef POLYNOMIALREGRESSOR_H_
