#ifndef BASISFACTORY_H_
#define BASISFACTORY_H_

#include "MUQ/Inference/TransportMaps/BasisSet.h"

#include <boost/property_tree/ptree.hpp>


namespace muq {
  namespace Inference{

    class BasisFactory{
    public:
      
      static std::vector<BasisSet> CreateLinear(int dim, bool isDiagonal=false);
      
      static std::vector<BasisSet> CreateDiagonal(Eigen::MatrixXd const&      samps,
                                                  boost::property_tree::ptree options = boost::property_tree::ptree());
      
      
    private:
      
    };
  }
}

#endif
