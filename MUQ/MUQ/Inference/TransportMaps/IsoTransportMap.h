
#ifndef IsoTransportMap_h_
#define IsoTransportMap_h_

// standard library includes
#include "MUQ/Inference/TransportMaps/PolynomialMap.h"

namespace muq {
  namespace Inference {
    
    /** @class IsoTransportMap
     @ingroup TransportMaps
     @brief Class defining transport maps where composed of 1D polynomials that are the same in each direction.
     */
    template<class PolyType>
    class IsoTransportMap : public PolynomialMap {
      
    public:
      
      // construct a rectangular transport map with a specified set of bases and coeffs
      IsoTransportMap(int inputDim,
                      const std::vector<std::shared_ptr<muq::Utilities::MultiIndexSet>>&               multisIn,
                      const std::vector<Eigen::VectorXd>&                                              coeffsIn) : PolynomialMap(inputDim,multisIn,coeffsIn){};
      
      // default virtual destructor
      virtual ~IsoTransportMap() = default;
      
      // grab a segment of the map (based on the output dimension)
      std::shared_ptr<TransportMap> segment(int startDim, int length) const override;
      
      // evaluate the inverse map using a bisection method build from Sturm sequences (Default)
      virtual Eigen::VectorXd EvaluateInverse(Eigen::VectorXd const& input, Eigen::VectorXd const& x0) const override;
      
      
      virtual std::shared_ptr<TransportMap> CreateFixedMap(int startInd, Eigen::VectorXd const& fixedInputIn) const override;
      
      
      virtual double GetSecondDerivative(Eigen::VectorXd const& input,
                                         int                    outputDim,
                                         int                    wrt1,
                                         int                    wrt2) const override;
      
      const std::vector<std::shared_ptr<muq::Utilities::RecursivePolynomialFamily1D>> GetPolys() const override;
      
    protected:
      
      virtual Eigen::MatrixXd EvaluateMultiImpl(Eigen::MatrixXd const& input) override;
      virtual Eigen::MatrixXd JacobianImpl(Eigen::VectorXd const& input) override;
      
      virtual void FillForConstruction(const Eigen::Ref<const Eigen::MatrixXd>& newSamps,
                                       std::vector < std::shared_ptr < Eigen::MatrixXd >> &fs,
                                       std::vector < std::shared_ptr < Eigen::MatrixXd >> &gs,
                                       int oldNumSamps,
                                       int newNumSamps) const override;
      
      virtual Eigen::MatrixXd GetBasisValues(const Eigen::Ref<const Eigen::MatrixXd>& samps, int dim) const override;
      virtual Eigen::MatrixXd GetDerivValues(const Eigen::Ref<const Eigen::MatrixXd>& samps, int dim) const override;
      
      
    }; // class TransportMap
    
  } // namespace Inference
} // namespace muq

#endif // ifndef IsoTransportMap_h_
