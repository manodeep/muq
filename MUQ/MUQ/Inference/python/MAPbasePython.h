
#ifndef MAPBASEPYTHON_H_
#define MAPBASEPYTHON_H_

#include <boost/python.hpp>
#include <boost/python/def.hpp>
#include <boost/python/class.hpp>

#include "MUQ/Utilities/python/PythonTranslater.h"

#include "MUQ/Inference/MAP/MAPbase.h"

namespace muq {
namespace Inference {
/// Tell python about muq::Inference::MAPbase
inline void ExportMAPBase()
{
  boost::python::class_<MAPbase, std::shared_ptr<MAPbase>, boost::noncopyable> exportMAPBase("MAPBase",
                                                                                             boost::python::no_init);

  // make constructors
  exportMAPBase.def("__init__", boost::python::make_constructor(&MAPbase::PyCreateFromDict));
  exportMAPBase.def("__init__", boost::python::make_constructor(&MAPbase::PyCreateFromString));

  // add functions
  exportMAPBase.def("Solve", &MAPbase::PySolve);

  // add members
  exportMAPBase.def_readwrite("OptSolver", &MAPbase::OptSolver);
}
} // namespace Inference
} // namespace muq

#endif // ifndef MAPBASEPYTHON_H_
