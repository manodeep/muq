#ifndef MARGINALSAMPLINGPROBLEMPYTHON_H_
#define MARGINALSAMPLINGPROBLEMPYTHON_H_

#include "MUQ/Modelling/GaussianPair.h"

#include "MUQ/Inference/ProblemClasses/MarginalSamplingProblem.h"

namespace muq {
  namespace Inference {
    void ExportMarginalSamplingProblem();
  } // namespace Inference
} // namespace muq


#endif // ifndef MARGINALSAMPLINGPROBLEMPYTHON_H_
