
#ifndef FISHERINFORMATIONMETRIC_H
#define FISHERINFORMATIONMETRIC_H

#include "MUQ/Inference/ProblemClasses/AbstractMetric.h"

#include "MUQ/Modelling/GaussianDensity.h"
#include "MUQ/Modelling/ModPiece.h"


namespace muq {
namespace Inference {
	/**
	 @class GaussianFisherInformationMetric
	@brief Implements the Expected Gaussian Fisher Information metric on a general inference problem.
	@details This class assumes a Gaussian error model and either a uniform prior or Gaussian prior.  
	         Assume \f$f(\theta)\f$ is the forward model, \f$L(\theta)\f$ is the likelihood function resulting from
	         \f$f(\theta)+\epsilon\f$ with \f$\epsilon\sim N(0,\Sigma_{\epsilon\epsilon})\f$, and
	         \f$p(\theta)\f$ is the prior.  If \f$p(\theta)\f$ is Gaussian, let \f$\Sigma_{\theta\theta}\f$ be the prior covariance.
	         Using these assumptions, the expected Fisher Information metric is given by:
	         \f[
	         G(\theta) = Df^T \Sigma_{\epsilon\epsilon}^{-1} Df + \Sigma_{\theta\theta}^{-1}
			 \f] 
	         where \f$Df\f$ is the Jacobian of \f$f\f$ at \f$\theta\f$.  If the prior is uniform (the default behavior of this class), then 
	         \f[
	         G(\theta) = Df^T \Sigma_{\epsilon\epsilon}^{-1} Df
			 \f] 
	*/
class GaussianFisherInformationMetric : public AbstractMetric {
public:

  /** @brief Basic constructor for uniform prior.
      @details This class takes the Gaussian error model and nonlinear forward model as inputs.
       If the prior term is not specified, (i.e. a null_ptr is found as the third argument), then a uniform prior is assumed.
      @param[in] density A shared_ptr to the Gaussian density defining the distribution of the additive error in \f$\epsilon\f$.
      @param[in] model The forward model in this problem.  Note that model->Jacobian should return the jacobian of the entire \f$\theta\f$ to \f$f(\theta)\f$ map.
                       If the forward model is constructed from many ModPieces, some care might be require to achieve this.
      @param[in] prior A pointer to a Gaussian prior density.  This will default to a null pointer in which case a uniform prior is assumed.
  */
  GaussianFisherInformationMetric(std::shared_ptr<muq::Modelling::GaussianDensity>  density,
                                  std::shared_ptr<muq::Modelling::ModPiece>  const& model,
								  std::shared_ptr<muq::Modelling::GaussianDensity>  prior=nullptr);

  virtual ~GaussianFisherInformationMetric() = default;

  /** This function actually gets the forward model Jacobian and computes the metric at the input point.
    @param[in] point The location to evaluate the metric.
  */
  virtual Eigen::MatrixXd ComputeMetric(Eigen::VectorXd const& point) override;


  void                    SetMask(Eigen::MatrixXd const& mask);

private:

  std::shared_ptr<muq::Modelling::ModPiece>   model;
  std::shared_ptr<muq::Modelling::GaussianDensity> errorDensity, priorDensity;

  boost::optional<Eigen::MatrixXd> mask = boost::optional<Eigen::MatrixXd>();
};
}
}

#endif // FISHERINFORMATIONMETRIC_H
