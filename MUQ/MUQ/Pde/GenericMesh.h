#ifndef GENERICMESH_H_
#define GENERICMESH_H_

#include <map>

// boost property tree
#include <boost/property_tree/ptree.hpp>

// mesh stuff
#include "libmesh/mesh.h"
#include "libmesh/mesh_generation.h"

#include "MUQ/Utilities/RegisterClassNameHelper.h"

namespace muq {
namespace Pde {

  /// An interface between MUQ and libMesh::Mesh
/**
   Store all the information corresponding to a mesh.  Children implement specific types of meshes.  
 */
class GenericMesh {
public:

  virtual ~GenericMesh() = default;

  /// A function to construct a muq::Pde::GenericMesh from a boost::property_tree::ptree options
  typedef std::function<std::shared_ptr<GenericMesh>(boost::property_tree::ptree const&)> MeshConstructorType;

  /// A map from the name of each mesh to the constructor that makes them
  typedef std::map<std::string, MeshConstructorType> AvailableMeshTypes;

  /// Construct a mesh from a boost::property_tree:ptree 
  /**
     -# The type of mesh (<EM>mesh.type</EM>)
        - Square mesh (<EM>SquareMesh</EM>)
        - Line mesh (<EM>LineMesh</EM>)

     Each mesh child will have its own options (see child class documentation).
     @param[in] param A boost ptree with all the user defined options 
   */
  static std::shared_ptr<GenericMesh> ConstructMesh(boost::property_tree::ptree const& param);

  /// Get the map of available mesh types
  /**
    Get a map that knows what the children are and how to construct them.
    \return A map from child class name to the constructor that creates it
   */
  static std::shared_ptr<AvailableMeshTypes> GetMeshMap();

  /// Return a pointer to the mesh
  /**
     \return Get the libMesh::Mesh this class wraps around.
   */
  std::shared_ptr<libMesh::Mesh> GetMeshPtr() const;

  /// Return the number of elements in the mesh
  /**
     \return The number of elements in this mesh
   */
  unsigned int NumElem() const;

protected:

  /// Construct a mesh
  /**
   * Only allowed to be called via GenericMesh::ConstructMesh
   */
  GenericMesh(boost::property_tree::ptree const& params);

  /// Pointer to the libMesh mesh this class wraps around
  std::shared_ptr<libMesh::Mesh> mesh;
};
} // namespace Pde
} // namespace muq

#define REGISTER_MESH(NAME) static auto NAME##_ = \
    muq::Pde::GenericMesh::GetMeshMap()->insert(std::make_pair(#NAME, boost::shared_factory<NAME>()));

#endif // ifndef GENERICMESH_H_
