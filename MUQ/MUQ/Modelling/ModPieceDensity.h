
#ifndef ModPieceDensity_h_
#define ModPieceDensity_h_

#include "MUQ/Modelling/ModPiece.h"
#include "MUQ/Modelling/Density.h"

namespace muq {
namespace Modelling {
	
/** @class ModPieceDensity
	@ingroup Modelling
	@brief Allows single output ModPieces to be treated as densities
	@details This is a very simple class that allows ModPiece instances with a single output to be treated as unnormalized densities.
	         This class takes a pointer to a general modpiece and simply passes any LogDensity, Gradient, Jacobian, etc... calls to 
	         the general ModPiece.
	*/
class ModPieceDensity : public Density {
public:

  ModPieceDensity(std::shared_ptr<ModPiece> generalPieceIn) :  Density(generalPieceIn->inputSizes,
	                                                                 generalPieceIn->hasDirectGradient,
																	 generalPieceIn->hasDirectJacobian,
																	 generalPieceIn->hasDirectJacobianAction,
																	 generalPieceIn->hasDirectHessian),
																	 generalPiece(generalPieceIn)
 {
	 assert(generalPiece->outputSize==1);
 }


  virtual ~ModPieceDensity() = default;


private:

  virtual double LogDensityImpl(std::vector<Eigen::VectorXd> const& input) override{
	  return generalPiece->Evaluate(input)(0);
  };
  
  virtual Eigen::VectorXd GradientImpl(std::vector<Eigen::VectorXd> const& input,
                                       Eigen::VectorXd const             & sensitivity,
                                       int const                           inputDimWrt) override{
	  return generalPiece->Gradient(input,sensitivity,inputDimWrt);
  };

  virtual Eigen::MatrixXd JacobianImpl(std::vector<Eigen::VectorXd> const& input,
	                                   int const                           inputDimWrt) override{
	  return generalPiece->Jacobian(input);
  };
  
  virtual Eigen::VectorXd JacobianActionImpl(std::vector<Eigen::VectorXd> const& input,
	                                         Eigen::VectorXd              const& target, 
	                                         int const                           inputDimWrt) override{
	  return generalPiece->JacobianAction(input,target);
  };
  
  virtual Eigen::MatrixXd HessianImpl(std::vector<Eigen::VectorXd> const& input,
	                                  Eigen::VectorXd              const& sens,
									  int const                           inputDimWrt) override{
	  return generalPiece->Hessian(input,sens);
  };
  
  std::shared_ptr<ModPiece> generalPiece;
};
} // namespace Modelling
} //namespace muq


#endif // ifndef Density_h_
