#ifndef ModParameter_h_
#define ModParameter_h_
#include <boost/property_tree/ptree.hpp> // needs to be ahead of Python includes to avoid "toupper" bug on osx

#include "MUQ/config.h"

#if MUQ_PYTHON==1
#include <boost/python.hpp>
#include <boost/python/def.hpp>
#include <boost/python/class.hpp>

#include "MUQ/Utilities/python/PythonTranslater.h"
#endif // MUQ_PYTHON==1

// othe muq-related includes
#include "MUQ/Modelling/ModPiece.h"

//#include "MUQ/Utilities/VectorTranslater.h"

namespace muq {
namespace Modelling {
class ModParameter : public ModPiece {
public:

  ModParameter(int dim, double val = 0.0) : ModParameter(val *Eigen::VectorXd::Ones(dim)) {}

  ModParameter(const Eigen::VectorXd& x) : ModPiece(Eigen::VectorXi::Zero(0), x.size(), true, true, true, false,
                                                    false),
                                           value(x)
  {}

#if MUQ_PYTHON==1
 ModParameter(boost::python::list const& x) : 
  ModParameter(muq::Utilities::GetEigenVector<Eigen::VectorXd>(x)) {}
#endif // MUQ_PYTHON==1


  /**
   * This hides the regular evaluate method, which you mostly shouldn't do, but is okay
   * here because we know ModParameter cannot take inputs and does not change. It thereby
   * skips over the error testing and caching code.
   **/
  Eigen::VectorXd Evaluate()
  {
    return value;
  }

  ///This implementation is still needed because more generic references won't find the Evaluate above.
  virtual Eigen::VectorXd EvaluateImpl(std::vector<Eigen::VectorXd> const& input) override
  {
    assert(input.size() == 0);
    return value;
  }

  void SetVal(const Eigen::VectorXd& x)
  {
    assert(x.size() == outputSize);
    value = x;
    InvalidateCache(); //the value returned changes here, so we must ensure the cache is cleared.
  }

protected:

  Eigen::VectorXd value;
};

// class ModParameter
} // namespace interface
} // namespace muq
#endif // ifndef ModParameter_h_
