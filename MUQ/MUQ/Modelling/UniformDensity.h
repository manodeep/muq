
#ifndef _UniformDensity_h
#define _UniformDensity_h

#include <functional>

#include "MUQ/Modelling/Density.h"
#include "MUQ/Modelling/UniformSpecification.h"
#include <Eigen/Core>


namespace muq {
namespace Modelling {
/** @class UniformDensityBase
 *  @ingroup Modelling
 *  @brief Defines a uniform density over some region defined by an input predicate.
 *  @details
 */
class UniformDensity : public Density {
public:

  UniformDensity(std::shared_ptr<UniformSpecification> specification) : Density(Eigen::VectorXi::Ones(1) * specification->dim,
                                                                                true, true, true, true),
                                                                        specification(specification)
  {}

  virtual ~UniformDensity() = default;

  std::shared_ptr<UniformSpecification> specification;

private:

  virtual double          LogDensityImpl(std::vector<Eigen::VectorXd> const& input) override;

  virtual Eigen::VectorXd GradientImpl(std::vector<Eigen::VectorXd> const& input,
                                       Eigen::VectorXd const             & sensitivity,
                                       int const                           inputDimWrt) override
  {
    return Eigen::VectorXd::Zero(inputSizes(inputDimWrt));
  }

  ///The jacobian is outputDim x inputDim
  virtual Eigen::MatrixXd JacobianImpl(std::vector<Eigen::VectorXd> const& input, int const inputDimWrt) override
  {
    return Eigen::MatrixXd::Zero(outputSize, inputSizes(inputDimWrt));
  }

  virtual Eigen::VectorXd JacobianActionImpl(std::vector<Eigen::VectorXd> const& input,
                                             Eigen::VectorXd const             & target,
                                             int const                           inputDimWrt) override
  {
    return Eigen::VectorXd::Zero(target.size());
  }
  
  virtual Eigen::MatrixXd HessianImpl(std::vector<Eigen::VectorXd> const& input,
                                      Eigen::VectorXd const             & sens,
                                      int const                           inputDimWrt) override
  {
    return Eigen::MatrixXd::Zero(inputSizes(inputDimWrt),inputSizes(inputDimWrt));
  }

};
} // namespace Modelling
} // namespace muq


#endif // ifndef _UniformDensity_h
