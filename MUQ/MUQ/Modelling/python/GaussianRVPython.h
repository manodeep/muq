#ifndef GAUSSIANRVPYTHON_H_
#define GAUSSIANRVPYTHON_H_

#include "MUQ/Modelling/GaussianRV.h"

namespace muq {
  namespace Modelling {
    void ExportGaussianRV();
  } // namespace Modelling
} // namespace muq

#endif // ifndef GAUSSIANRVPYTHON_H_
