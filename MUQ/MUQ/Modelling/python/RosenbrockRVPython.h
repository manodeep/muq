#ifndef ROSENBROCKRVPYTHON_H_
#define ROSENBROCKRVPYTHON_H_

#include "MUQ/Modelling/RosenbrockRV.h"

namespace muq { 
  namespace Modelling { 
    void ExportRosenbrockRV();
  } // namespace Modelling 
} // namespace muq

#endif
