#ifndef _SYMBOLICHEADERS_H_
#define _SYMBOLICHEADERS_H_
//
//  SymbolicExpressions.h
//
//
//  Created by Matthew Parno on 3/15/12.
//  Copyright (c) 2012 MIT. All rights reserved.
//


#include "MUQ/Modelling/AnalyticFunctions/ComponentwiseModel.h"
#include "MUQ/Modelling/AnalyticFunctions/ExpModel.h"
#include "MUQ/Modelling/AnalyticFunctions/SinModel.h"
#include "MUQ/Modelling/AnalyticFunctions/CosModel.h"
#include "MUQ/Modelling/AnalyticFunctions/TanModel.h"
#include "MUQ/Modelling/AnalyticFunctions/CscModel.h"
#include "MUQ/Modelling/AnalyticFunctions/SecModel.h"
#include "MUQ/Modelling/AnalyticFunctions/CotModel.h"
#include "MUQ/Modelling/AnalyticFunctions/Pow10Model.h"
#include "MUQ/Modelling/AnalyticFunctions/AbsModel.h"
#include "MUQ/Modelling/AnalyticFunctions/Pow2Model.h"
#include "MUQ/Modelling/AnalyticFunctions/LogModel.h"
#endif