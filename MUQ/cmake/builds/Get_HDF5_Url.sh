#!/bin/bash
www=http://www.hdfgroup.org/ftp/HDF5/current/src/
if hash curl 2>/dev/null; then
    filename=$(curl -s $www --list-only | grep -o -E 'href="([^"#]+).tar.gz"' | cut -d'"' -f2)
elif hash wget 2>/dev/null; then
    filename=$(wget -qO- $www | grep -o -E 'href="([^"#]+).tar.gz"' | cut -d'"' -f2)
else
    echo "Unable to find curl or wget, will not be able to automatically find HDF5 tarball."
    exit 1
fi
echo $www$filename
