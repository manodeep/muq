import unittest 
import numpy as np
from random import random

import libmuqModelling
import libmuqOptimization

# Nonlinear constraint
class TestNonLin(libmuqOptimization.ConstraintBase):
    def __init__(self, dim):
        libmuqOptimization.ConstraintBase.__init__(self, dim, dim)

    def Eval(self, xc):
        out = [0.0]*len(xc)
        for i in range(len(xc)):
            out[i] = xc[i]*xc[i] - 1.0
            
        return out
        
    def ApplyJacTrans(self, xc, vecIn):
        output = [0.0]*len(xc)
        for i in range(len(xc)):
            output[i] = 2.0*xc[i] * vecIn[i]

        return output

class ConstraintTest(unittest.TestCase):
    def setUp(self):
        # make nonlinear
        self.nonLin1 = TestNonLin(4)
        self.nonLin2 = TestNonLin(4)

        # make linear
        A = [[random()]*4 for i in range(2)]
        b = [1.0]*2
        
        self.lin1 = libmuqOptimization.LinearConstraint(A, b)

        # make bound
        self.bound1 = libmuqOptimization.BoundConstraint(4, 0, 1, False)
        self.bound2 = libmuqOptimization.BoundConstraint(4, 1, 0, True)
        self.bound3 = libmuqOptimization.BoundConstraint(4, 3, -1, True)

        # make list
        self.tester = libmuqOptimization.ConstraintList(4)

        self.tester.Add(self.nonLin1)
        self.tester.Add(self.nonLin2)
        self.tester.Add(self.lin1)
        self.tester.Add(self.bound1)
        self.tester.Add(self.bound2)
        self.tester.Add(self.bound3)

    def testAdd(self):
        self.assertEqual(self.tester.NumBound(), 3)
        self.assertEqual(self.tester.NumLinear(), 1)
        self.assertEqual(self.tester.NumNonlinear(), 2)

    def testEvaluate(self):
        xc = [0.5]*4
        cc = self.tester.Eval(xc)

        self.assertEqual(len(cc), 13)

    def testGrad(self):
        xc   = [0.5]*4
        sens = [1.0]*self.tester.NumConstraints()

        grad = self.tester.ApplyJacTrans(xc, sens)

        self.assertEqual(len(grad), 4)
        self.assertEqual(grad[1], grad[3])


