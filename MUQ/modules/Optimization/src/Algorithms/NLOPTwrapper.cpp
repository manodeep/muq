//
//  NLOPTwrapper.cpp
//
//
//  Created by Matthew Parno on 5/13/13.
//  Copyright (c) 2013 MIT. All rights reserved.
//

// self include
#include "MUQ/Optimization/Algorithms/NLOPTwrapper.h"

// standard library includes
#include <string>
#include <cmath>

// namespace stuff
using namespace muq::Optimization;
using namespace std;

typedef struct {
  std::shared_ptr<OptProbBase> ProbPtr;
} nlopt_func_data;

/* Declare functions based on double pointers that will call Optimizer::Optimizer_Objective.  This is useful in wrappers
 * where the external package requires a function pointer to the objective.
 */
double ObjFunc_NLOPT(unsigned dim, const double *x, double *grad, void *my_func_data)
{
  // cast my_func_data to the correct structure
  nlopt_func_data *muq_data = (nlopt_func_data *)my_func_data;

  // objective function
  double fout;

  Eigen::Map<Eigen::VectorXd> statemap((double *)x, dim);
  Eigen::VectorXd state = statemap;

  // if gradient is not NULL, return computed gradient, otherwise just return objective
  if (grad) {
    // build MuqVectors that wrap around the gradient and current point -- the MuqVectors do not manage memory
    Eigen::Map<Eigen::VectorXd> deltamap(grad, dim);
    Eigen::VectorXd delta = deltamap;

    // evaluate current this point and get gradient/objective
    fout = muq_data->ProbPtr->grad(state, delta);

    deltamap = delta;
  } else {
    // no gradient is needed, so just return objective function value
    fout = muq_data->ProbPtr->eval(state);
  }

  return fout;
}

typedef struct {
  ConstraintBase *MuqConst;
} nlopt_constraint_data;


void ConstraintFunc_NLOPT(unsigned m, double *result, unsigned dim, const double *x, double *gradient, void *data)
{
  // cast the void pointer back to the nlopt_constraint_data pointer
  nlopt_constraint_data *muq_data = (nlopt_constraint_data *)data;

  // create an eigen vector around the input
  Eigen::Map<Eigen::VectorXd> xcmap((double *)x, dim);
  Eigen::VectorXd xc = xcmap;

  if (gradient) {
    Eigen::Map<Eigen::MatrixXd> grad(gradient, dim, m);

    for (unsigned int i = 0; i < m; ++i) {
      // build the sensitivity -- just a 1 in dimension d
      Eigen::VectorXd sens = Eigen::VectorXd::Zero(m);
      sens[i] = 1;

      // evaluate current point and get gradient/objective
      grad.col(i)  = muq_data->MuqConst->ApplyJacTrans(xc, sens);
      grad.col(i) *= -1;
    }
  }

  // return constraint function value
  Eigen::Map<Eigen::VectorXd> cval((double *)result, m);
  cval = -1.0 * muq_data->MuqConst->eval(xc);
}

REGISTER_OPT_DEF_TYPE(NLOPT) nlopt_algorithm NLOPT::str2alg(const std::string& method)
{
  if (!method.compare("DIRECT")) {
    return NLOPT_GN_DIRECT;
  } else if (!method.compare("DIRECTL")) {
    return NLOPT_GN_DIRECT_L;
  } else if (!method.compare("CRS")) {
    return NLOPT_GN_CRS2_LM;
  } else if (!method.compare("MLSL")) {
    return NLOPT_G_MLSL_LDS;
  } else if (!method.compare("ISRES")) {
    return NLOPT_GN_ISRES;
  } else if (!method.compare("COBYLA")) {
    return NLOPT_LN_COBYLA;
  } else if (!method.compare("BOBYQA")) {
    return NLOPT_LN_BOBYQA;
  } else if (!method.compare("NEWUOA")) {
    return NLOPT_LN_NEWUOA_BOUND;
  } else if (!method.compare("PRAXIS")) {
    return NLOPT_LN_PRAXIS;
  } else if (!method.compare("NM")) {
    return NLOPT_LN_NELDERMEAD;
  } else if (!method.compare("Sbplx")) {
    return NLOPT_LN_SBPLX;
  } else if (!method.compare("MMA")) {
    return NLOPT_LD_MMA;
  } else if (!method.compare("SLSQP")) {
    return NLOPT_LD_SLSQP;
  } else if (!method.compare("LBFGS")) {
    return NLOPT_LD_LBFGS;
  } else if (!method.compare("PreTN")) {
    return NLOPT_LD_TNEWTON_PRECOND_RESTART;
  } else if (!method.compare("LMVM")) {
    return NLOPT_LD_VAR2;
  } else {
    return NLOPT_LN_BOBYQA;
  }
}

bool handlesUnconstrained(boost::property_tree::ptree& properties)
{
  string method = properties.get("Opt.NLOPT.Method", "BOBYQA");

  return (!method.compare("COBYLA")) || (!method.compare("BOBYQA")) || (!method.compare("NEWUOA")) ||
         (!method.compare("PRAXIS")) || (!method.compare("NM")) || (!method.compare("Sbplx")) ||
         (!method.compare("MMA")) ||
         (!method.compare("SLSQP")) || (!method.compare("LBFGS")) || (!method.compare("PreTN")) ||
         (!method.compare("LMVM"));
}

bool handlesPartialBound(boost::property_tree::ptree& properties)
{
  string method = properties.get("Opt.NLOPT.Method", "BOBYQA");


  return (!method.compare("COBYLA")) || (!method.compare("BOBYQA")) || (!method.compare("NEWUOA")) ||
         (!method.compare("PRAXIS")) || (!method.compare("NM")) || (!method.compare("Sbplx")) ||
         (!method.compare("MMA")) ||
         (!method.compare("SLSQP")) || (!method.compare("LBFGS")) || (!method.compare("PreTN")) ||
         (!method.compare("LMVM"));
}

bool requiresFullyBound(boost::property_tree::ptree& properties)
{
  string method = properties.get("Opt.NLOPT.Method", "BOBYQA");

  return (!method.compare("DIRECT")) || (!method.compare("DIRECTL")) || (!method.compare("CRS")) ||
         (!method.compare("MLSL")) || (!method.compare("ISRES"));
}

bool handlesLinear(boost::property_tree::ptree& properties)
{
  string method = properties.get("Opt.NLOPT.Method", "BOBYQA");

  return (!method.compare("ISRES")) || (!method.compare("COBYLA"));
}

bool handlesNonlinear(boost::property_tree::ptree& properties)
{
  string method = properties.get("Opt.NLOPT.Method", "BOBYQA");

  return (!method.compare("ISRES")) || (!method.compare("COBYLA"));
}

bool handlesEquality(boost::property_tree::ptree& properties)
{
  string method = properties.get("Opt.NLOPT.Method", "BOBYQA");

  return (!method.compare("ISRES")) || (!method.compare("COBYLA"));
}

bool handlesInequality(boost::property_tree::ptree& properties)
{
  string method = properties.get("Opt.NLOPT.Method", "BOBYQA");

  return (!method.compare("ISRES")) || (!method.compare("COBYLA"));
}

NLOPT::NLOPT(std::shared_ptr<OptProbBase> ProbPtr, boost::property_tree::ptree& properties) : OptAlgBase(ProbPtr,
                                                                                                         properties,
                                                                                                         handlesUnconstrained(
                                                                                                           properties),
                                                                                                         handlesPartialBound(
                                                                                                           properties),
                                                                                                         requiresFullyBound(
                                                                                                           properties),
                                                                                                         handlesLinear(
                                                                                                           properties),
                                                                                                         handlesNonlinear(
                                                                                                           properties),
                                                                                                         handlesEquality(
                                                                                                           properties),
                                                                                                         handlesInequality(
                                                                                                           properties))
{
  // get maximum number of outer iterations
  maxEvals = properties.get("Opt.NLOPT.MaxEvals", 1000);
  randSeed = properties.get("Opt.NLOPT.RandSeed",0);
  xtol_rel = properties.get("Opt.NLOPT.xtol_rel", 1e-4);
  xtol_abs = properties.get("Opt.NLOPT.xtol_abs", 1e-4);
  ftol_rel = properties.get("Opt.NLOPT.ftol_rel", 1e-4);
  ftol_abs = properties.get("Opt.NLOPT.ftol_abs", 1e-4);

  // get line search shrink parameters
  population = properties.get("Opt.NLOPT.Population", 10 * (ProbPtr->GetDim() + 1));

  localAlgorithm = str2alg(properties.get("Opt.NLOPT.LocalMethod", "BOBYQA"));

  // which NLOPT algorithm to use?
  algorithm = str2alg(properties.get("Opt.NLOPT.Method", "BOBYQA"));
}

/** Solve the optimization using NLOPT.  */
Eigen::VectorXd NLOPT::solve(const Eigen::VectorXd& x0)
{
  if(randSeed>0)
    nlopt_srand(randSeed);
  
  // set the problem dimension
  int dim = x0.size();

  // now, create NLOPT optimization routine from algorith mand dimension
  nlopt_opt opt      = nlopt_create(algorithm, dim);
  nlopt_opt localopt = nlopt_create(localAlgorithm, dim);

  // set various parameter of optimization algorithm -- here we call max because the density logeval is actually
  // -2*objective
  nlopt_func_data fdata;

  fdata.ProbPtr = OptProbPtr;

  // add the bound constraints
  Eigen::VectorXd lb = -HUGE_VAL *Eigen::VectorXd::Ones(dim);
  Eigen::VectorXd ub = HUGE_VAL * Eigen::VectorXd::Ones(dim);

  if (OptProbPtr->isBoundConstrained()) {
    for (auto i = OptProbPtr->inequalityConsts.Bounds.begin(); i < OptProbPtr->inequalityConsts.Bounds.end(); ++i) {
      if ((*i)->isLower()) {
        lb[(*i)->WhichDim()] = (*i)->GetVal();
      } else {
        ub[(*i)->WhichDim()] = (*i)->GetVal();
      }
    }

    nlopt_set_lower_bounds(opt, lb.data());
    nlopt_set_upper_bounds(opt, ub.data());
  }

  // add the general equality constraints
  nlopt_constraint_data equalConstData;
  Eigen::VectorXd cEqTols;
  if (OptProbPtr->equalityConsts.NumConstraints() > 0) {
    // constraint tolerances
    cEqTols = ctol * Eigen::VectorXd::Ones(OptProbPtr->equalityConsts.NumConstraints());

    // pass constraint to NLOPT constraint function
    equalConstData.MuqConst = (ConstraintBase *)&OptProbPtr->equalityConsts;

    // add constraint to nlopt_opt object
    nlopt_add_equality_mconstraint(opt,
                                   OptProbPtr->equalityConsts.NumConstraints(), ConstraintFunc_NLOPT, &equalConstData,
                                   cEqTols.data());
  }

  // add the general inequality constraints
  nlopt_constraint_data inequalConstData;
  Eigen::VectorXd cIneqTols;
  if (OptProbPtr->inequalityConsts.NumConstraints() > 0) {
    // constraint tolerances
    cIneqTols = ctol * Eigen::VectorXd::Ones(OptProbPtr->inequalityConsts.NumConstraints());

    // pass constraint to NLOPT constraint function
    inequalConstData.MuqConst = (ConstraintBase *)&OptProbPtr->inequalityConsts;

    // add constraint to nlopt_opt object
    nlopt_add_equality_mconstraint(opt,
                                   OptProbPtr->inequalityConsts.NumConstraints(), ConstraintFunc_NLOPT, &inequalConstData,
                                   cEqTols.data());
  }

  // set stopping criteria -- they are disabled by NLOPT if negative or zero
  nlopt_set_xtol_rel(opt, xtol_rel);
  nlopt_set_xtol_abs1(opt, xtol_abs);
  nlopt_set_ftol_rel(opt, ftol_rel);
  nlopt_set_ftol_abs(opt, ftol_abs);

  // set the local algorithm
  nlopt_set_local_optimizer(opt, localopt);

  // set the initial stepsize, if it was passed in XML file
  nlopt_set_initial_step1(opt, stepLength);

  // set the population -- only useful for some stochastic algorithms
  nlopt_set_population(opt, population);

  // set the maximum number of evaluations
  nlopt_set_maxeval(opt, maxEvals);

  nlopt_set_min_objective(opt, ObjFunc_NLOPT, &fdata);

  // optimum function value
  double fOpt;

  Eigen::VectorXd xOpt(x0);
  status = nlopt_optimize(opt, xOpt.data(), &fOpt);

  // deallocate optimizer
  nlopt_destroy(opt);

  // return the optimum value
  return xOpt;
}

