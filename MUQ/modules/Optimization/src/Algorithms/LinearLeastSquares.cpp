
#include "MUQ/Optimization/Algorithms/LinearLeastSquares.h"

// Eigen includes
#include <Eigen/Dense>
#include <Eigen/Jacobi>

// std library includes
#include <deque>
#include <iostream>

// other muq includes
#include "MUQ/Utilities/LinearOperators/Operators/LinOpBase.h"

using namespace muq::Optimization;
using namespace muq::Utilities;
using namespace std;


Eigen::VectorXd LinearLeastSquares::solve(const Eigen::MatrixXd& A, const Eigen::MatrixXd& d)
{
  return A.colPivHouseholderQr().solve(d);
}

/** Solve a quadratically constrained overdetermined(min(Ax-d) s.t. x^TBx = s) linear least squares problem using Eigen
 *  types. */
Eigen::VectorXd LinearLeastSquares::solve(const Eigen::MatrixXd& V,
                                          const Eigen::VectorXd& d,
                                          const Eigen::MatrixXd& B,
                                          double                 s)
{
  // first, transform the problem to be of the proper form -- assumes B is symmetric positive semi-definite
  Eigen::SelfAdjointEigenSolver<Eigen::MatrixXd> Bsolver(B.selfadjointView<Eigen::Lower>());

  //Eigen::MatrixXd L = sqrt(s)*(Bsolver.eigenvalues().cwiseSqrt().asDiagonal()*Bsolver.eigenvectors().transpose());
  //Eigen::MatrixXd Linv = B.selfadjointView<Eigen::Lower>().llt().matrixL();
  //Eigen::MatrixXd Linv = Bsolver.eigenvectors()*(Bsolver.eigenvalues().array().sqrt().matrix().asDiagonal());


  Eigen::VectorXd tempVals = Bsolver.eigenvalues();
  Eigen::MatrixXd tempVecs = Bsolver.eigenvectors();
  std::deque<int> nzInds;
  std::deque<int> zInds;
  int nnz = 0, nz = 0;

  for (int i = 0; i < tempVals.size(); ++i) {
    if (fabs(tempVals[i]) > 1e-14) {
      nzInds.push_back(i);
      nnz++;
    } else {
      nz++;
      zInds.push_back(i);
    }
  }

  Eigen::VectorXd eigVals(nnz);
  Eigen::MatrixXd eigVecs(B.cols(), nnz);

  int j = 0;
  for (auto i = nzInds.begin(); i != nzInds.end(); ++i, ++j) {
    eigVals(j)     = tempVals[*i];
    eigVecs.col(j) = tempVecs.col(*i);
  }

  // keep the nonzero eigenvalues and corresponding eigenvectors
  Eigen::MatrixXd L = eigVecs * (eigVals.array().inverse().sqrt().matrix().asDiagonal());


  Eigen::MatrixXd A = sqrt(s) * V * L;

  // compute the svd of A to get the eigenvalues and eigenvectors of A^TA
  Eigen::JacobiSVD<Eigen::MatrixXd> svdSolver(A, Eigen::ComputeThinU | Eigen::ComputeThinV);
  Eigen::MatrixXd Q = svdSolver.matrixV();
  Eigen::VectorXd D = svdSolver.singularValues().array().square().matrix();

  Eigen::VectorXd b = A.transpose() * d;
  Eigen::VectorXd h = Q.transpose() * b;

  // nonlinear solve for lagrange multiplier lambda
  double newLambda = D.minCoeff() - abs(h(0));
  double lambda    = newLambda + 1;
  while (fabs(lambda - newLambda) > 1e-10) {
    lambda = newLambda;

    double f      = (h.array() / (D.array() - lambda)).square().matrix().sum() - 1;
    double fderiv = ((2 * h.array().square()) / (D.array() - lambda).pow(3.0)).matrix().sum();
    newLambda = lambda - 2 * (f + 1) * (sqrt(f + 1) - 1.0) / fderiv;
  }

  // at this point, we have the lagrange multiplier so me can use the Lagrange equations to extract x and then alpha

  Eigen::VectorXd u     = (h.array() / (D.array() - lambda)).matrix();
  Eigen::VectorXd alpha = sqrt(s) * L * Q * u;

  // if the constraint matrix has a nullspace fill in the nullspace
  if (nnz != tempVals.size()) {
    eigVecs.resize(B.cols(), nz);

    j = 0;
    for (auto i = zInds.begin(); i != zInds.end(); ++i, ++j) {
      eigVecs.col(j) = tempVecs.col(*i);
    }

    alpha += eigVecs * ((V * eigVecs).colPivHouseholderQr().solve(d));
  }

  return alpha;
}

/** Solve a quadratically constrained overdetermined(min(Ax-d) s.t. x^TBx = s) linear least squares problem using Eigen
 *  types. */
Eigen::VectorXd LinearLeastSquares::solve(const Eigen::MatrixXd& V,
                                          const Eigen::VectorXd& d,
                                          const Eigen::VectorXd& B,
                                          double                 s)
{
  // first, transform the problem to be of the proper form -- assumes B is symmetric positive semi-definite
  std::deque<int> nzInds;
  std::deque<int> zInds;
  int nnz = 0, nz = 0;

  for (int i = 0; i < B.size(); ++i) {
    if (fabs(B[i]) > 1e-14) {
      nzInds.push_back(i);
      nnz++;
    } else {
      nz++;
      zInds.push_back(i);
    }
  }

  Eigen::MatrixXd A(V.rows(), nnz);
  int j = 0;
  for (auto i = nzInds.begin(); i != nzInds.end(); ++i, ++j) {
    A.col(j) = sqrt(s) * V.col(*i) * (1.0 / sqrt(B(*i)));
  }

  // compute the svd of A to get the eigenvalues and eigenvectors of A^TA
  Eigen::JacobiSVD<Eigen::MatrixXd> svdSolver(A, Eigen::ComputeThinU | Eigen::ComputeThinV);
  Eigen::MatrixXd Q = svdSolver.matrixV();
  Eigen::VectorXd D = svdSolver.singularValues().array().square().matrix();

  Eigen::VectorXd b = A.transpose() * d;
  Eigen::VectorXd h = Q.transpose() * b;

  // nonlinear solve for lagrange multiplier lambda
  int minInd;
  double minVal    = D.minCoeff(&minInd);
  double newLambda = minVal - abs(h(minInd));
  double lambda    = newLambda + 1;
  while (fabs(lambda - newLambda) > 1e-8) {
    lambda = newLambda;

    double f      = (h.array() / (D.array() - lambda)).square().matrix().sum() - 1;
    double fderiv = ((2 * h.array().square()) / (D.array() - lambda).pow(3.0)).matrix().sum();
    newLambda = lambda - 2 * (f + 1) * (sqrt(f + 1) - 1.0) / fderiv;
  }

  // at this point, we have the lagrange multiplier so me can use the Lagrange equations to extract x and then alpha

  Eigen::VectorXd u = (h.array() / (D.array() - lambda)).matrix();
  Eigen::VectorXd alpha(V.cols());
  Eigen::VectorXd temp = Q * u;

  j = 0;
  for (auto i = nzInds.begin(); i != nzInds.end(); ++i, ++j) {
      alpha(*i) = sqrt(s) * temp(j) * (1.0 / sqrt(B(*i))); //= sqrt(s)*L*Q*u;
  }

  // if the constraint matrix has a nullspace fill in the nullspace
  if (nnz != B.size()) {
    A.resize(V.rows(), nz);
    j = 0;
    for (auto i = zInds.begin(); i != zInds.end(); ++i, ++j) {
      A.col(j) = V.col(*i);
    }

    temp = (A.colPivHouseholderQr().solve(d));
    j    = 0;
    for (auto i = zInds.begin(); i != zInds.end(); ++i, ++j) {
      alpha(*i) = temp(j);
    }
  }

  return alpha;
}

/** Solve a least squares system defined by the linear operator AOp and the transpose of this linear operator in the
 *  operator ATransOp. This function implements the lsqr method which is equilavent to solving the normal equations with
 *  the conjugate gradient method.
 */
Eigen::VectorXd LinearLeastSquares::solve(std::shared_ptr<LinOpBase> AOp,
                                          std::shared_ptr<LinOpBase> ATransOp,
                                          const Eigen::VectorXd    & d)
{
  // get the size of the linear operator
  // int nrows = AOp->rows();
  int ncols = AOp->cols();

  // set up working parameters

  double beta       = d.norm();
  Eigen::VectorXd u = d / beta;

  Eigen::VectorXd v = ATransOp->apply(u);
  double alpha      = v.norm();

  v /= alpha;

  double Bnorm = alpha;

  Eigen::VectorXd x = Eigen::VectorXd::Zero(ncols);
  Eigen::VectorXd w = v;

  double phi;
  double rho;
  double phibar = beta;
  double rhobar = alpha;
  double c, s, theta;

  const double btol = 1e-10 * d.norm();
  const double atol = 1e-10;

  //const double conlim = 1e10;

  int  it       = 0;
  bool stopFlag = false;
  while (!stopFlag) {
    // update u
    u    = AOp->apply(v) - alpha * u;
    beta = u.norm();
    u   /= beta;

    Bnorm += beta * beta + alpha * alpha;

    // update v
    v     = ATransOp->apply(u) - beta * v;
    alpha = v.norm();
    v    /= alpha;

    // update rho
    rho = sqrt(rhobar * rhobar + beta * beta);

    // update c
    c = rhobar / rho;

    s = beta / rho;

    theta = s * alpha;

    rhobar = -c * alpha;

    phi    = c * phibar;
    phibar = s * phibar;


    //        std::cout << "\t\tApproximate residual = " << phibar << std::endl;
    //        std::cout << "Approximate ||A|| = " << Bnorm << std::endl;
    //        std::cout << "Approximate cond(A) = " << std::endl;
    //        std::cout << "\t\tApproximate A^Tr = " << phibar*alpha*abs(c) << std::endl << std::endl;

    // update x and w
    x += (phi / rho) * w;
    w  = v - (theta / rho) * w;

    // check for convergence
    if (phibar < btol + atol * Bnorm * x.norm()) {
      stopFlag = true;
    } else if (phibar * alpha * abs(c) / (Bnorm * phibar) < atol) {
      stopFlag = true;
    } else if (++it >= ncols) {
      stopFlag = true;
    }
  }

  return x;
}

