
# define the source files in this directory that we want to include in the
#   generic interface library
SET(Utility_SOURCES

PointWrapper.cpp
LogConfig.cpp
EigenUtils.cpp

HDF5Wrapper.cpp
HDF5Logging.cpp
HDF5Types.cpp
JobManager.cpp
RandomGenerator.cpp

Hmatrix/ElementPairNode.cpp
Hmatrix/ElementPairTree.cpp
Hmatrix/ElementNode.cpp
Hmatrix/ElementTree.cpp
Hmatrix/HMatrix.cpp
Hmatrix/ConvexHull.cpp

LinearOperators/Operators/EigenLinOp.cpp
LinearOperators/Solvers/CG.cpp
LinearOperators/Solvers/BiCG.cpp
LinearOperators/Solvers/MinRes.cpp

multiIndex/MultiIndex.cpp
multiIndex/MultiIndexSet.cpp
multiIndex/MultiIndexPool.cpp
multiIndex/MultiIndexFactory.cpp
multiIndex/MultiIndexLimiter.cpp

mesh/UnstructuredSimplicialMesh.cpp
mesh/StructuredQuadMesh.cpp
mesh/Mesh.cpp

SignalProcessing/HilbertTransform.cpp

Variable.cpp
VariableCollection.cpp

Polynomials/RecursivePolynomialFamily1D.cpp
Polynomials/LegendrePolynomials1DRecursive.cpp
Polynomials/HermitePolynomials1DRecursive.cpp
Polynomials/ProbabilistHermite.cpp
Polynomials/Monomial.cpp

Quadrature/QuadratureFamily1D.cpp
Quadrature/GaussianQuadratureFamily1D.cpp
Quadrature/GaussLegendreQuadrature1D.cpp
Quadrature/GaussHermiteQuadrature1D.cpp
Quadrature/GaussProbabilistHermiteQuadrature1D.cpp
Quadrature/FullTensorQuadrature.cpp
Quadrature/GaussPattersonQuadrature1D.cpp
Quadrature/GaussChebyshevQuadrature1D.cpp
Quadrature/ExpGrowthQuadratureFamily1D.cpp
Quadrature/LinearGrowthQuadratureFamily1D.cpp
Quadrature/ClenshawCurtisQuadrature1D.cpp

)

if(MUQ_USE_PYTHON)
  set(Utility_SOURCES ${Utility_SOURCES}
    ../python/UtilitiesPythonLibrary.cpp
    ../python/PythonTranslater.cpp    

    ../python/HDF5WrapperPython.cpp
    ../python/HDF5LoggingPython.cpp
    
    ../python/RandomGeneratorPython.cpp

    ../python/PolynomialsPython.cpp
    ../python/QuadraturePython.cpp
    ../python/VariableCollectionPython.cpp
    ../python/MultiIndexPython.cpp
    )
endif(MUQ_USE_PYTHON)


##########################################
#  Objects to build

# Define the GenericInterface library
add_library(muqUtilities SHARED ${Utility_SOURCES})
target_link_libraries(muqUtilities ${MUQ_LINK_LIBS})
add_dependencies(muqUtilities Boost)

if(APPLE AND MUQ_USE_PYTHON)
    set_property(TARGET muqUtilities PROPERTY PREFIX "lib")
    set_property(TARGET muqUtilities PROPERTY OUTPUT_NAME "muqUtilities.so")
    set_property(TARGET muqUtilities PROPERTY SUFFIX "")
    set_property(TARGET muqUtilities PROPERTY SOVERSION "32.1.2.0")
endif(APPLE AND MUQ_USE_PYTHON)

install(TARGETS muqUtilities 
  # IMPORTANT: Add the GeneralUtility library to the "export-set"
  EXPORT MUQDepends
  LIBRARY DESTINATION "${CMAKE_INSTALL_PREFIX}/lib"
  ARCHIVE DESTINATION "${CMAKE_INSTALL_PREFIX}/lib")

  
  #build the static library if asked for, or if matlab needs it
if(build_polychaos_matlab_bindings OR build_static_libs)
add_library(muqUtilitiesStatic STATIC ${Utility_SOURCES} )

set_target_properties(muqUtilitiesStatic PROPERTIES COMPILE_FLAGS -fPIC)

target_link_libraries(muqUtilitiesStatic hdf5 hdf5_hl ${MUQ_LINK_LIBS_STATIC} )

install(TARGETS  muqUtilitiesStatic
  # IMPORTANT: Add the GeneralUtility library to the "export-set"
  EXPORT MUQDepends
  LIBRARY DESTINATION "${CMAKE_INSTALL_PREFIX}/lib"
ARCHIVE DESTINATION "${CMAKE_INSTALL_PREFIX}/lib")
  
endif(build_polychaos_matlab_bindings OR build_static_libs)
