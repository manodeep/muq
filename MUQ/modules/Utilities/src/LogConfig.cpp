
#include "MUQ/Utilities/LogConfig.h"

#ifdef MUQ_GLOG
void muq::Utilities::InitializeLogging(int argc, char **argv)
{
  google::InitGoogleLogging(argv[0]);
}

#else // ifdef MUQ_GLOG
void        muq::Utilities::InitializeLogging(int argc, char **argv) {}

nullstream& operator<<(nullstream& s, std::ostream& (std::ostream &))
{
  return s;
}

#endif // ifdef MUQ_GLOG
