#include "MUQ/Pde/Transient.h"

using namespace std;
using namespace muq::Pde;

Transient::Transient(Eigen::VectorXi const& inSizes, shared_ptr<GenericEquationSystems> const& system, boost::property_tree::ptree const& para) :
  PDE(InputSizes(para.get("PDE.OutputSize", 0), inSizes), system, para) 
{
  isTransient = true;
}

Eigen::VectorXi Transient::InputSizes(unsigned int const outSize, Eigen::VectorXi const& paraInSizes) const
{
  Eigen::VectorXi inSizes(paraInSizes.size() + 2);
  inSizes(0) = 1; // time 
  inSizes(1) = outSize; // initial guess
  inSizes.tail(paraInSizes.size()) = paraInSizes; // other parameters

  return inSizes;
}

void Transient::ComputeMassMatrix()
{
  // store the number of variables
  const unsigned int nVars = vars.size();

  // get a constant reference to the mesh 
  const libMesh::MeshBase& mesh = eqnsystem->GetEquationSystemsPtr()->get_mesh();

  // get a reference to the system 
  libMesh::System& system = eqnsystem->GetEquationSystemsPtr()->get_system<libMesh::System>(GetName());

  // get a constant reference to the DOF map 
  const libMesh::DofMap& dofMap = system.get_dof_map();

  // element matrices and vectors 
  auto elemDOFs = make_shared<ElementDOFs>(nVars);

  // the mass matrix (only for transient problems)
  Eigen::MatrixXd massMatrix = Eigen::MatrixXd::Zero(outputSize, outputSize);

  // first element
  libMesh::MeshBase::const_element_iterator el = mesh.active_local_elements_begin();

  // last element 
  libMesh::MeshBase::const_element_iterator elEnd = mesh.active_local_elements_end();

  // element counter 
  unsigned int elNum = 0;
  
  // loop through the elements 
  for( ; el!=elEnd; ++el ) {
    // reset the degrees of freedom
    elemDOFs->Reset(el, vars, dofMap);

    // get information about this element 
    auto elInfo = elemInfo->Get(elNum);

    // set and increment the element number 
    eqnsystem->GetEquationSystemsPtr()->parameters.set<int>("elNum") = elNum++;

    // compute the element mass matrix
    ComputeElementMassMatrix(elInfo, elemDOFs);

    // get the local mass matrix
    libMesh::DenseMatrix<libMesh::Number> Me = elemDOFs->GetElementMassMatrix();
    vector<libMesh::dof_id_type> dofIndices  = elemDOFs->GetLocalToGlobalMap();

    // add to global
    for (unsigned int i = 0; i < dofIndices.size(); ++i) {
      for (unsigned int j = 0; j < dofIndices.size(); ++j) {
	// sum in each element
        massMatrix(dofIndices[i], dofIndices[j]) += Me(i, j);
      }
    }
  }

  EnforceMatrixBCs(massMatrix); 

  lumpedSumMassMatrix = massMatrix.rowwise().sum().array();
}

double Transient::MassMatrixImpl(string const& varName1, libMesh::Real const& phiV1, libMesh::RealGradient const& dphiV1, string const& varName2, libMesh::Real const& phiV2, libMesh::RealGradient const& dphiV2) const 
{
  // if the variables are the same
  if (varName1.compare(varName2) == 0) {
    // mass matrix
    return phiV1 * phiV2;
  }
  
  return 0.0;
}

void Transient::ComputeElementMassMatrix(std::shared_ptr<ElemInfo> const& elInfo, std::shared_ptr<ElementDOFs> const& elemDOFs) 
{
  // get the number of variables
  const unsigned int nVars = vars.size();

  // element jacobians (const reference to avoid the copy)
  const vector<libMesh::Real>& JxW = elInfo->JxW;
  
  // quadrature points on each element (const reference to avoid the copy)
  const vector<libMesh::Point>& qPoint = elInfo->qPoint;

  // loop over the quadrature points 
  assert(JxW.size() == qPoint.size());
  for( unsigned int qp=0; qp<qPoint.size(); ++qp ) {
    // loop over the variables (first time)
    for( bimap::const_iterator iter=vars.begin(); iter!=vars.end(); ++iter ) {
      // the variable number 
      const unsigned int var1 = iter->left; 
      
      // basis functions on the element (const reference to avoid the copy)
      const vector<vector<libMesh::Real> >& phiV1 = elInfo->basisMap[feType[var1]]->phi;
      
      // basis function derivatives on the element (const reference to avoid the copy)
      const vector<vector<libMesh::RealGradient> >& dphiV1 = elInfo->basisMap[feType[var1]]->dphi;

      // loop over the variables (second time)
      for( bimap::const_iterator jter=vars.begin(); jter!=vars.end(); ++jter ) {
	// the variable number 
	const unsigned int var2 = jter->left; 
	
	// basis functions on the element (const reference to avoid the copy)
	const vector<vector<libMesh::Real> >& phiV2 = elInfo->basisMap[feType[var2]]->phi;
	
	// basis function derivatives on the element (const reference to avoid the copy)
	const vector<vector<libMesh::RealGradient> >& dphiV2 = elInfo->basisMap[feType[var2]]->dphi;

	// loop through the basis functions (first time)
	for( unsigned int i=0; i<phiV1.size(); ++i ) {
	  // loop through the basis functions (second time)
	  for( unsigned int j=0; j<phiV2.size(); ++j ) {
	    double mass = MassMatrixImpl(vars.left.at(var1), phiV1[i][qp], dphiV1[i][qp], vars.left.at(var2), phiV2[j][qp], dphiV2[j][qp]);

	    elemDOFs->SumIntoElementMassMatrix(var1, var2, i, j, JxW[qp] * mass);
	  }
	}
      }
    }
  }
}
