import unittest
import numpy as np

import libmuqPde
import libmuqGeostatistics

class KernelModelTest(unittest.TestCase):
    def testBasic(self):
        L   = 0.7
        P   = 1.5
        sig = 1.0

        kernel = libmuqGeostatistics.PowerKernel(L, P, sig)

        Nx = 13
        Ny = 8

        dx = 1.0/(Nx-1.0)
        dy = 1.0/(Ny-1.0)

        x = [None]*(Nx*Ny)
        for i in range(Nx):
            for j in range(Ny):
                x[j + i*Ny] = [i*dx, j*dy]

        kernelMod = libmuqPde.KernelModel(x, Nx * Ny, kernel)

        cov     = kernelMod.GetCovariance()
        trueCov = kernel.BuildCov(x)

        for i in range(Nx):
            for j in range(Ny):
                self.assertAlmostEqual(cov[i][j], trueCov[i][j], 10)

    def testTrucated(self):
        L   = 0.7
        P   = 2.0
        sig = 1.0

        kernel = libmuqGeostatistics.PowerKernel(L, P, sig)

        Nx = 13
        Ny = 8

        numBasis = (Nx * Ny) / 4

        dx = 1.0/(Nx-1.0)
        dy = 1.0/(Ny-1.0)

        x = [None]*(Nx*Ny)
        for i in range(Nx):
            for j in range(Ny):
                x[j + i*Ny] = [i*dx, j*dy]

        kernelMod = libmuqPde.KernelModel(x, numBasis, kernel)

        cov     = kernelMod.GetCovariance()
        trueCov = kernel.BuildCov(x)

        for i in range(Nx):
            for j in range(Ny):
                self.assertAlmostEqual(cov[i][j], trueCov[i][j], 6)

