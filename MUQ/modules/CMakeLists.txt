# begin accumulating the tests
set(all_gtest_sources RunTests.cpp)
set(all_long_gtest_sources RunLongTests.cpp) 

set(test_link_libs )

if(UtilitiesAndModelling_build)
add_subdirectory(Modelling)
add_subdirectory(Utilities)

  #alway link in modelling and general utilities
  list(APPEND test_link_libs muqModelling muqUtilities)
  
  # if we need to use sacado, add the necessary libraries
  if(MUQ_Sacado)
    list(APPEND test_link_libs ${Trilinos_LIBRARIES} ${Trilinos_TPL_LIBRARIES} )
  endif(MUQ_Sacado)

endif(UtilitiesAndModelling_build)

if(Geostats_build)
  add_subdirectory(Geostatistics)
  if(Geostats_tests)
    list(APPEND test_link_libs muqGeostatistics)
  endif(Geostats_tests)
endif(Geostats_build)

if(Approximation_build)
  add_subdirectory(Approximation)
 if(Approximation_tests)
  list(APPEND test_link_libs muqApproximation)
 endif(Approximation_tests)
endif(Approximation_build)

if(Inference_build)
  add_subdirectory(Inference)
  if(Inference_tests)
    list(APPEND test_link_libs  muqOptimization muqInference)
  endif(Inference_tests)
endif(Inference_build)

if(Optimization_build)
  add_subdirectory(Optimization)

  if(Optimization_tests)
    list(APPEND test_link_libs muqOptimization)
  endif(Optimization_tests)
endif(Optimization_build)

if(PDE_build)
  add_subdirectory(Pde)

  if(PDE_tests)
    list(APPEND test_link_libs muqPde)
  endif(PDE_tests)

endif(PDE_build)

#only build the tests if some of them should be built
IF(Inference_tests OR Optimization_tests OR Geostats_tests 
  OR UtilitiesAndModelling_tests OR Approximation_tests OR PDE_tests)
  
  set(CMAKE_CXX_FLAGS  "${CMAKE_CXX_FLAGS} -DGTEST_USE_OWN_TR1_TUPLE=1")
  
  CHECK_CXX_COMPILER_FLAG("-std=c++11" HAS_PTHREAD)
  if(HAS_PTHREAD)
	  set(CMAKE_CXX_FLAGS  "${CMAKE_CXX_FLAGS} -pthread")
  endif()
  
  ADD_EXECUTABLE(RunAllTests ${all_gtest_sources})
  ADD_EXECUTABLE(RunAllLongTests ${all_long_gtest_sources})
  #add_dependencies(RunAllTests  test_link_libs MUQ_LINK_LIBS)
  message("MUQ_LINK_LIBS=${MUQ_LINK_LIBS}")
  TARGET_LINK_LIBRARIES(RunAllTests ${test_link_libs} ${GTEST_LIBRARY} ${external_test_libs})
  TARGET_LINK_LIBRARIES(RunAllLongTests ${test_link_libs} ${GTEST_LIBRARY} ${external_test_libs})
  
  if(USE_INTERNAL_BOOST)
  add_dependencies(RunAllTests Boost)
  add_dependencies(RunAllLongTests Boost)
endif()
ENDIF()
