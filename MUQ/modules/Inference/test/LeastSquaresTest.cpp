
#include <iostream>

#include <Eigen/Cholesky>

// include the google testing header
#include "gtest/gtest.h"

#include "MUQ/Inference/MAP/MAPFactory.h"
#include "MUQ/Inference/MAP/MAP.h"
#include "MUQ/Inference/MAP/LinearLeastSquares.h"


using namespace muq::Inference;
using namespace muq::Modelling;
using namespace std;

TEST(InferenceLeastSquaresTest, BasicLinearTest)
{
  int Dim     = 10;
  int DataDim = 2;

  // create a prior covariance matrix
  Eigen::MatrixXd PriorSig = Eigen::MatrixXd::Zero(Dim, Dim);

  for (int i = 0; i < Dim; ++i) {
    PriorSig(i,     i) = 2;
  }
  for (int i = 0; i < Dim - 1; ++i) {
    PriorSig(i + 1, i)     = 0.5;
    PriorSig(i,     i + 1) = 0.5;
  }

  // create an error covariance
  Eigen::MatrixXd ErrorCov = Eigen::MatrixXd::Zero(DataDim, DataDim);
  for (int i = 0; i < DataDim; ++i) {
    ErrorCov(i, i) = 0.2;
  }

  // create a linear operator
  Eigen::MatrixXd LinOpt = Eigen::MatrixXd::Zero(DataDim, Dim);
  for (int i = 0; i < DataDim; ++i) {
    LinOpt(i, i * floor(Dim / DataDim)) = 1;
  }

  // create a prior mean
  Eigen::VectorXd PriorMu = 2. * Eigen::VectorXd::Ones(Dim);

  // make up some data
  Eigen::VectorXd Data = 0.5 * Eigen::VectorXd::Ones(DataDim);


  // perform the linear conditioning
  Eigen::VectorXd PostMu;
  PostMu = LinearLeastSquares::SolveIterative(PriorMu, PriorSig, Data, ErrorCov, LinOpt);


  EXPECT_NEAR(PostMu[0], 0.636363636363636, 1e-7);
  EXPECT_NEAR(PostMu[1], 1.659090909090909, 1e-7);
  EXPECT_EQ(PostMu[2], 2);
  EXPECT_EQ(PostMu[3], 2);
  EXPECT_NEAR(PostMu[4], 1.659090909090909, 1e-7);
  EXPECT_NEAR(PostMu[5], 0.636363636363636, 1e-7);
  EXPECT_NEAR(PostMu[6], 1.659090909090909, 1e-7);
  EXPECT_EQ(PostMu[7], 2);
  EXPECT_EQ(PostMu[8], 2);
  EXPECT_EQ(PostMu[9], 2);
}

