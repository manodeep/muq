import unittest
import numpy as np

import libmuqUtilities
import libmuqModelling
import libmuqInference

class BasicMCMCTest(unittest.TestCase):
    def testBasicMH(self):
        tempSeed = libmuqUtilities.RandomGeneratorTemporarySetSeed(56335740)

        tempMean = [1.0]*2
        tempCov  = [[1.0, 1.0], [1.0, 4.0]]

        # create density we want to sample from
        dens = libmuqModelling.GaussianDensity(tempMean, tempCov, libmuqModelling.GaussianSpecification.CovarianceMatrix)

        # create basic sampling problem
        gaussTest = libmuqInference.SamplingProblem(dens)

        # set MCMC parameters
        params = dict()
        params["MCMC.Methods"]     = "SingleChainMCMC"
        params["MCMC.Kernel"]      = "MHKernel"
        params["MCMC.Proposal"]    = "MHProposal"
        params["MCMC.Steps"]       = 200
        params["MCMC.BurnIn"]      = 10
        params["MCMC.MH.PropSize"] = 1
        params["Verbose"]          = 0

        # construct
        testSolver = libmuqInference.MCMCBase(gaussTest, params)

        # sample; creates libmuqModelling.EmpiricalRandVar
        testPost = testSolver.Sample(tempMean)

        mean = testPost.GetMean()
        cov  = testPost.GetCov()

        recordedMean = [1.2214169594710333,1.7525567240294524]
        recordedCov  = [[0.8839224533725156, 0.8175738219042619], [0.8175738219042619, 3.1940574951587424]]

        for i in range(2):
            self.assertAlmostEqual(mean[i], recordedMean[i],1)

    def testBasicMALATest(self):
        tempSeed = libmuqUtilities.RandomGeneratorTemporarySetSeed(89347173)
        tempMean = [1.0]*2
        tempCov  = [[4.0, 2.0], [2.0, 6.0]]

        graph = libmuqModelling.ModGraph()

        # create models
        inferenceTarget = libmuqModelling.VectorPassthroughModel(2)
        likelihood      = libmuqModelling.GaussianDensity(tempMean, tempCov, libmuqModelling.GaussianSpecification.CovarianceMatrix)
        prior           = libmuqModelling.UniformDensity(libmuqModelling.UniformBox([-1000.0]*2, [1000.0]*2))
        posterior       = libmuqModelling.DensityProduct(2)
        forwardModel    = libmuqModelling.VectorPassthroughModel(2)

        # add nodes
        graph.AddNode(inferenceTarget, "inferenceTarget")
        graph.AddNode(likelihood, "likelihood")
        graph.AddNode(prior, "prior")
        graph.AddNode(posterior, "posterior")
        graph.AddNode(forwardModel, "forwardModel")

        # add edges
        graph.AddEdge("inferenceTarget", "forwardModel", 0)
        graph.AddEdge("forwardModel", "likelihood", 0);
        graph.AddEdge("inferenceTarget", "prior", 0);
        graph.AddEdge("likelihood", "posterior", 0);
        graph.AddEdge("prior", "posterior", 1);

        graph.writeGraphViz("results/tests/BasicMALATestModelPython.pdf");
        
        # create the inference problem
        test = libmuqInference.InferenceProblem(graph)
        
        # create MCMC instance
        params = dict()
        params["MCMC.Proposal"]            = "MMALA"
        params["MCMC.Kernel"]              = "MHKernel"
        params["MCMC.Method"]              = "SingleChainMCMC"
        params["MCMC.Steps"]               = 300
        params["MCMC.BurnIn"]              = 2
        params["MCMC.MHProposal.PropSize"] = 1
        params["MCMC.MMALA.AdaptDelay"]    = 4
        params["Ve"]                  = 0
   
        mcmc = libmuqInference.MCMCBase(test, params)

        # sample
        testPost = mcmc.Sample(tempMean)

        mean = testPost.GetMean()
        cov  = testPost.GetCov()

        recordedMean = [1.2710669883395054, 0.9673363621607011]
        recordedCov  = [[3.7172942000533498, 1.9050727321584324], [1.9050727321584324, 6.151689856931992]]
     
        for i in range(2):
            self.assertAlmostEqual(mean[i], recordedMean[i],1)

    def testBasicPreMALATest(self):
        tempSeed = libmuqUtilities.RandomGeneratorTemporarySetSeed(56395740)

        tempMean = [1.0]*2
        tempCov  = [[4.0, 2.0], [2.0, 6.0]]

        graph = libmuqModelling.ModGraph()

        # create models
        inferenceTarget = libmuqModelling.VectorPassthroughModel(2)
        likelihood      = libmuqModelling.GaussianDensity(tempMean, tempCov, libmuqModelling.GaussianSpecification.CovarianceMatrix)
        prior           = libmuqModelling.UniformDensity(libmuqModelling.UniformBox([-1000.0]*2, [1000.0]*2))
        posterior       = libmuqModelling.DensityProduct(2)
        forwardModel    = libmuqModelling.VectorPassthroughModel(2)

        # add nodes
        graph.AddNode(inferenceTarget, "inferenceTarget")
        graph.AddNode(likelihood, "likelihood")
        graph.AddNode(prior, "prior")
        graph.AddNode(posterior, "posterior")
        graph.AddNode(forwardModel, "forwardModel")

        # add edges
        graph.AddEdge("inferenceTarget", "forwardModel", 0)
        graph.AddEdge("forwardModel", "likelihood", 0);
        graph.AddEdge("inferenceTarget", "prior", 0);
        graph.AddEdge("likelihood", "posterior", 0);
        graph.AddEdge("prior", "posterior", 1);

        # create inference problem
        metricMat = np.linalg.solve(tempCov,np.matrix([[1.0, 0.0],[0.0, 1.0]])).tolist()
        test      = libmuqInference.InferenceProblem(graph, libmuqInference.EuclideanMetric(metricMat))

        # create MCMC instance
        params = dict()
        params["MCMC.Method"]             = "SingleChainMCMC"
        params["MCMC.Kernel"]             = "MHKernel"
        params["MCMC.Proposal"]           = "PreMALA"
        params["MCMC.Steps"]              = 500
        params["MCMC.BurnIn"]             = 5
        params["MCMC.PreMALA.StepLength"] = 1
        params["MCMC.PreMALA.AdaptDelay"] = 5
        params["Verbose"]                 = 0
   
        mcmc = libmuqInference.MCMCBase(test, params)

        # sample
        testPost = mcmc.Sample(tempMean)

        mean = testPost.GetMean()
        cov  = testPost.GetCov()

        recordedMean = [0.9394426418886298, 0.9169982842313243]
        recordedCov  = [[3.7714616248886528, 2.2044292866055457], [2.2044292866055457, 5.63178485899422]]

        for i in range(2):
            self.assertAlmostEqual(recordedMean[i], mean[i],1)

    def testBasicManifoldMALATest(self):
        tempSeed = libmuqUtilities.RandomGeneratorTemporarySetSeed(56395740)

        tempMean = [1.0]*2
        tempCov  = [[4.0, 2.0], [2.0, 6.0]]

        graph = libmuqModelling.ModGraph()

        # create models
        inferenceTarget = libmuqModelling.VectorPassthroughModel(2)
        likelihood      = libmuqModelling.GaussianDensity(tempMean, tempCov, libmuqModelling.GaussianSpecification.CovarianceMatrix)
        prior           = libmuqModelling.UniformDensity(libmuqModelling.UniformBox([-1000.0]*2, [1000.0]*2))
        posterior       = libmuqModelling.DensityProduct(2)
        forwardModel    = libmuqModelling.VectorPassthroughModel(2)

        # add nodes
        graph.AddNode(inferenceTarget, "inferenceTarget")
        graph.AddNode(likelihood, "likelihood")
        graph.AddNode(prior, "prior")
        graph.AddNode(posterior, "posterior")
        graph.AddNode(forwardModel, "forwardModel")

        # add edges
        graph.AddEdge("inferenceTarget", "forwardModel", 0)
        graph.AddEdge("forwardModel", "likelihood", 0);
        graph.AddEdge("inferenceTarget", "prior", 0);
        graph.AddEdge("likelihood", "posterior", 0);
        graph.AddEdge("prior", "posterior", 1);

        # create inference problem
        metric = libmuqInference.GaussianFisherInformationMetric(likelihood, forwardModel)
        test   = libmuqInference.InferenceProblem(graph, metric)

        # create MCMC instance
        params = dict()
        params["MCMC.Proposal"]            = "MMALA"
        params["MCMC.Kernel"]              = "MHKernel"
        params["MCMC.Method"]              = "SingleChainMCMC"
        params["MCMC.Steps"]               = 2000
        params["MCMC.BurnIn"]              = 200
        params["MCMC.MHProposal.PropSize"] = 1
        params["MCMC.MMALA.AdaptDelay"]    = 200
        params["Verbose"]                  = 0
    
        mcmc = libmuqInference.MCMCBase(test, params)

        # sample
        testPost = mcmc.Sample(tempMean)

        mean = testPost.GetMean()
        cov  = testPost.GetCov()

        recordedMean = [1.0253059986555924, 1.076988937221206]
        recordedCov  = [[3.7086719197900346, 2.0126578457264017], [2.0126578457264017, 5.783416432389334]]
     
        for i in range(2):
            self.assertAlmostEqual(recordedMean[i], mean[i],1)

    #def testBasicParallelMHTest(self):
        #tempMean = [1.0]*2
        #tempCov  = [[1.0, 1.0], [1.0, 4.0]]

        #graph = libmuqModelling.ModGraph()

        ## create models
        #inferenceTarget = libmuqModelling.VectorPassthroughModel(2)
        #likelihood      = libmuqModelling.GaussianDensity(tempMean, tempCov, False)
        #prior           = libmuqModelling.UniformDensity(libmuqModelling.UniformBox([-1000.0]*2, [1000.0]*2))
        #posterior       = libmuqModelling.DensityProduct(2)

        ## add nodes
        #graph.AddNode(inferenceTarget, "inferenceTarget")
        #graph.AddNode(likelihood, "likelihood")
        #graph.AddNode(prior, "prior")
        #graph.AddNode(posterior, "posterior")

        ## add edges
        #graph.AddEdge("inferenceTarget", "likelihood", 0)
        #graph.AddEdge("inferenceTarget", "prior", 0);
        #graph.AddEdge("likelihood", "posterior", 0);
        #graph.AddEdge("prior", "posterior", 1);

        #graph.writeGraphViz("results/tests/MHModelPython.pdf")

        ## make inference problem
        #test = libmuqInference.InferenceProblem(graph)

        ## create MCMC instance
        #params = dict()
        #params["MCMC.Method"]                    = "ParallelChainMCMC"
        #params["MCMC.Steps"]                     = 5000
        #params["MCMC.BurnIn"]                    = 1000
        #params["MCMC.MH.PropSize"]               = 1
        #params["MCMC.ParallelChain.ChildMethod"] = "MH"
        #params["MCMC.ParallelChain.NumChains"]   = 10
        #params["Verbose"]                        = 0
    
        #mcmc = libmuqInference.MCMCBase(test, params)

        ## sample
        #testPost = mcmc.Sample(tempMean)

        #mean = testPost.GetMean()
        #cov  = testPost.GetCov()
     
        #for i in range(2):
            #self.assertLess(abs(mean[i]-tempMean[i]), 0.12)
            #for j in range(2):
                #self.assertLess(abs(cov[i][j]-tempCov[i][j]), 0.3)

    #def testBasicTemperedMCMC(self):
        #tempMean = [1.0]*2
        #tempCov  = [[4.0, 2.0], [2.0, 6.0]]

        #priorMean = [-1.0]*2
        #priorCov  = [[1.0, 1.0], [1.0, 4.0]]

        #graph = libmuqModelling.ModGraph()

        ## create models
        #inferenceTarget = libmuqModelling.VectorPassthroughModel(2)
        #likelihood      = libmuqModelling.GaussianDensity(tempMean, tempCov, False)
        #prior           = libmuqModelling.GaussianDensity(priorMean, priorCov, False)
        #posterior       = libmuqModelling.DensityProduct(2)

        ## add nodes
        #graph.AddNode(inferenceTarget, "inferenceTarget")
        #graph.AddNode(likelihood, "likelihood")
        #graph.AddNode(prior, "prior")
        #graph.AddNode(posterior, "posterior")

        ## add edges
        #graph.AddEdge("inferenceTarget", "likelihood", 0)
        #graph.AddEdge("inferenceTarget", "prior", 0);
        #graph.AddEdge("likelihood", "posterior", 0);
        #graph.AddEdge("prior", "posterior", 1);

        ## make inference problem
        #metric  = libmuqInference.EuclideanMetric()
        #priorRV = libmuqModelling.GaussianRV(priorMean, priorCov, False)
        #test    = libmuqInference.InferenceProblem(graph, metric, priorRV)

        ## create MCMC instance
        #params = dict()
        #params["MCMC.Method"]                    = "TemperedMCMC"
        #params["MCMC.Steps"]                     = 3000
        #params["MCMC.BurnIn"]                    = 100
        #params["MCMC.MH.PropSize"]               = 1
        #params["MCMC.ParallelChain.ChildMethod"] = "MH"
        #params["MCMC.ParallelChain.NumChains"]   = 10
        #params["Verbose"]                        = 0
    
        #mcmc = libmuqInference.MCMCBase(test, params)

        ## sample
        #testPost = mcmc.Sample(tempMean)

        #mean = testPost.GetMean()
        #cov  = testPost.GetCov()

        #posteriorMean = [-0.5586893958564082, -0.26561613270378859]
        #posteriorCov  = [[0.77674854772463431, 0.62605821647766635], [0.62605821647766635, 2.382183597618706]]
     
        #for i in range(2):
            #self.assertLess(abs(mean[i]-posteriorMean[i]), 0.14)
            #for j in range(2):
                #self.assertLess(abs(cov[i][j]-posteriorCov[i][j]), 0.3)

    def testBasicAMALATest(self):
        tempSeed = libmuqUtilities.RandomGeneratorTemporarySetSeed(56395740)

        tempMean = [1.0]*2
        tempCov  = [[1.0, 1.0], [1.0, 4.0]]

        graph = libmuqModelling.ModGraph()

        # create models
        inferenceTarget = libmuqModelling.VectorPassthroughModel(2)
        likelihood      = libmuqModelling.GaussianDensity(tempMean, tempCov, libmuqModelling.GaussianSpecification.CovarianceMatrix)
        prior           = libmuqModelling.UniformDensity(libmuqModelling.UniformBox([-1000.0]*2, [1000.0]*2))
        posterior       = libmuqModelling.DensityProduct(2)
        forwardModel    = libmuqModelling.VectorPassthroughModel(2)

        # add nodes
        graph.AddNode(inferenceTarget, "inferenceTarget")
        graph.AddNode(likelihood, "likelihood")
        graph.AddNode(prior, "prior")
        graph.AddNode(posterior, "posterior")
        graph.AddNode(forwardModel, "forwardModel")

        # add edges
        graph.AddEdge("inferenceTarget", "forwardModel", 0)
        graph.AddEdge("forwardModel", "likelihood", 0);
        graph.AddEdge("inferenceTarget", "prior", 0);
        graph.AddEdge("likelihood", "posterior", 0);
        graph.AddEdge("prior", "posterior", 1);

        # create inference problem
        metric = libmuqInference.GaussianFisherInformationMetric(likelihood, forwardModel)
        test   = libmuqInference.InferenceProblem(graph, metric)

        # create MCMC instance
        params = dict()
        params["MCMC.Proposal"]         = "AMALA"
        params["MCMC.Kernel"]           = "MHKernel"
        params["MCMC.Method"]           = "SingleChainMCMC"
        params["MCMC.Steps"]            = 10000
        params["MCMC.BurnIn"]           = 200
        params["MCMC.AMALA.AdaptSteps"] = 10
        params["MCMC.AMALA.AdaptStart"] = 50
        params["MCMC.AMALA.AdaptScale"] = 1
        params["MCMC.AMALA.MaxDrift"]   = 2.0
        params["Verbose"]               = 0
    
        mcmc = libmuqInference.MCMCBase(test, params)

        # sample
        testPost = mcmc.Sample(tempMean)

        mean = testPost.GetMean()
        cov  = testPost.GetCov()

        recordedMean = [1.0192995177389816, 1.03328786869913]
        recordedCov  = [[1.0283255944633396, 1.0884059559362822], [1.0884059559362822, 4.257297261370405]]
     
        for i in range(2):
            self.assertAlmostEqual(recordedMean[i], mean[i],1)

    def testBasicAM(self):
        tempSeed = libmuqUtilities.RandomGeneratorTemporarySetSeed(31600051)
        
        tempMean = [1.0]*2
        tempCov  = [[1.0, 1.0], [1.0, 4.0]]

        # make models
        inferenceTarget = libmuqModelling.VectorPassthroughModel(2)
        likelihood      = libmuqModelling.GaussianDensity(tempMean, tempCov,libmuqModelling.GaussianSpecification.CovarianceMatrix)
        prior           = libmuqModelling.UniformDensity(libmuqModelling.UniformBox([-1000.0]*2, [1000.0]*2))
        posterior       = libmuqModelling.DensityProduct(2)

        # make graph
        graph = libmuqModelling.ModGraph()

        graph.AddNode(inferenceTarget, "inferenceTarget")
        graph.AddNode(likelihood, "likelihood")
        graph.AddNode(prior, "prior")
        graph.AddNode(posterior, "posterior")

        graph.AddEdge("inferenceTarget", "likelihood", 0)
        graph.AddEdge("inferenceTarget", "prior", 0)
        graph.AddEdge("likelihood", "posterior", 0)
        graph.AddEdge("prior", "posterior", 1)

        # make inference problem
        test = libmuqInference.SamplingProblem(graph)
        graphDensity = libmuqModelling.ModGraphDensity(graph, "posterior")
        test = libmuqInference.SamplingProblem(graphDensity)

        # create MCMC instance
        params = dict()
        params["MCMC.Proposal"]            = "AM"
        params["MCMC.Kernel"]              = "MHKernel"
        params["MCMC.Method"]              = "SingleChainMCMC"
        params["MCMC.Steps"]               = 100
        params["MCMC.BurnIn"]              = 10
        params["MCMC.MHProposal.PropSize"] = 0.5
        params["MCMC.AM.AdaptSteps"]       = 2
        params["MCMC.AM.AdaptStart"]       = 5
        params["MCMC.AM.AdaptScale"]       = 1.2
        params["Verbose"]                  = 0
        
        mcmc = libmuqInference.MCMCBase(test, params)

        # sample
        testPost = mcmc.Sample(tempMean)

        mean = testPost.GetMean()
        cov  = testPost.GetCov()

        recordedMean = [1.3773589332834204, 1.24468776056571]
        recordedCov  = [[0.7966554647485108, 0.9723947607851763], [0.9723947607851763, 2.962828272471045]]
     
        for i in range(2):
            self.assertAlmostEqual(recordedMean[i], mean[i],1)
        
    def testBasicDRAM(self):
        tempSeed = libmuqUtilities.RandomGeneratorTemporarySetSeed(68436633)

        tempMean = [1.0]*2
        tempCov  = [[1.0, 1.0], [1.0, 4.0]]

        test = libmuqInference.SamplingProblem(libmuqModelling.GaussianDensity(tempMean, tempCov, libmuqModelling.GaussianSpecification.CovarianceMatrix))

        params = dict()
        params["MCMC.Method"]                   = "SingleChainMCMC"
        params["MCMC.Kernel"]                   = "DR"
        params["MCMC.DR.stages"]                = 3
        params["MCMC.DR.ProposalSpecification"] = "DRAM"
        params["MCMC.Steps"]                    = 100
        params["MCMC.BurnIn"]                   = 5
        params["MCMC.MHProposal.PropSize"]      = 1.5
        params["MCMC.DR.stages"]                = 3
        params["MCMC.DR.NumSteps"]              = 500000
        params["MCMC.AM.AdaptSteps"]            = 2
        params["MCMC.AM.AdaptStart"]            = 5
        params["MCMC.AM.AdaptScale"]            = 2.4 * 2.4 / 2.0
        params["Verbose"]                       = 0
        params["MCMC.HDF5OutputGroup"]          = "/test/dram/"
        params["MCMC.HDF5OutputDetails"]        = True

        params["HDF5.GlobalFilename"] = "results/tests/MCMCLogging.h5"

        hdf5file = libmuqUtilities.HDF5Wrapper()
        hdf5log  = libmuqUtilities.HDF5Logging()

        hdf5file.OpenFile(params)
        hdf5log.Configure(params)

        mcmc = libmuqInference.MCMCBase(test, params)
 
        # sample
        testPost = mcmc.Sample(tempMean)

        hdf5file.CloseFile()

        mean = testPost.GetMean()
        cov  = testPost.GetCov()

        recordedMean = [0.7557497707417075, 0.6461448077639229]
        recordedCov  = [[1.2465326392484892, 1.4133719176802009], [1.4133719176802009, 4.36939563070272]]
     
        for i in range(2):
            self.assertAlmostEqual(recordedMean[i], mean[i],1)

