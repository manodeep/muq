
#include "gtest/gtest.h"

#include <boost/property_tree/ptree.hpp>
#include <boost/math/distributions/students_t.hpp>

#include "MUQ/Utilities/EigenTestUtils.h"
#include "MUQ/Utilities/mesh/StructuredQuadMesh.h"
#include "MUQ/Utilities/RandomGenerator.h"

#include "MUQ/Inference/TransportMaps/TransportMap.h"
#include "MUQ/Inference/TransportMaps/MapFactory.h"

// #include "MUQ/Geostats/PowerKernel.h"
//#include "MUQ/Inference/MCMC/pCN.h"
#include "MUQ/Inference/MCMC/MCMCBase.h"
#include <MUQ/Inference/MCMC/MCMCProposal.h>
#include "MUQ/Modelling/ModGraph.h"
#include "MUQ/Modelling/GaussianPair.h"
#include "MUQ/Modelling/EmpiricalRandVar.h"
#include "MUQ/Utilities/EigenTestUtils.h"
#include "MUQ/Modelling/UniformDensity.h"
#include "MUQ/Modelling/UniformSpecification.h"
#include "MUQ/Inference/ProblemClasses/InferenceProblem.h"
#include "MUQ/Inference/ProblemClasses/MarginalInferenceProblem.h"
#include "MUQ/Inference/ProblemClasses/SamplingProblem.h"
#include "MUQ/Inference/ProblemClasses/GaussianFisherInformationMetric.h"
#include "MUQ/Inference/ProblemClasses/EuclideanMetric.h"

#include "MUQ/Modelling/UniformDensity.h"
#include "MUQ/Modelling/UniformSpecification.h"
#include "MUQ/Modelling/ModPieceTemplates.h"
#include "MUQ/Modelling/VectorPassthroughModel.h"
#include "MUQ/Modelling/DensityProduct.h"
#include "MUQ/Modelling/ModGraph.h"
#include "MUQ/Utilities/HDF5Wrapper.h"
#include "MUQ/Modelling/LinearModel.h"
#include "MUQ/Modelling/SumModel.h"

using namespace muq::Modelling;
using namespace muq::Inference;
using namespace muq::Utilities;

// using namespace muq::geostats;
using namespace std;
using namespace Eigen;


// The fixture for testing class Foo.
class MCMCConvergenceTests : public ::testing::Test {
 protected:

  MCMCConvergenceTests() : trueMean(2),
			   trueCov(2,2), 
			   testTol(2e-5)
  {};

  virtual void SetUp() {
    RandomGeneratorTemporarySetSeed tempSeed(89347173);

    // create a mean vector of dimension 10, filled with ones
    trueMean << 1.5, 2.5;
    trueCov << 4, 2,
               2, 6;


    auto targetDensity = std::make_shared<GaussianDensity>(trueMean, trueCov);
    gaussTest = make_shared<SamplingProblem>(targetDensity);


    // create an MCMC instance
    params.put("MCMC.Method", "SingleChainMCMC");
    params.put("Verbose", 0);
    params.put("MCMC.Steps", 50000);
    params.put("MCMC.BurnIn", 20000); // burnin only comes from adaptation, we use a sample of the true target density to start the MCMC chain
 
  }

  virtual void TearDown() {
    
    auto targetRV = make_shared<GaussianRV>(trueMean,trueCov);
    
    const int numRepeats = 75;
    Eigen::MatrixXd allMeans(2,numRepeats); // store the mean estimate from each run
    Eigen::MatrixXd allFs(2,numRepeats); // store the marginal variances from each run
    
    for(int r=0; r<numRepeats; ++r){
      // sample the distribution from a random starting point -- no burn in is necessary in this case
      auto mcmc = MCMCBase::ConstructMCMC(gaussTest, params);
      EmpRvPtr TestPost = mcmc->Sample( targetRV->Sample() );
      allMeans.col(r) = TestPost->getMean();
      
      for(int s=0; s<TestPost->NumSamps(); ++s)
        allFs.col(r) = TestPost->getSamp(s).array().pow(3.0).matrix();
    }

    // compute the bias in both the mean and var
    Eigen::VectorXd meanMeans = allMeans.rowwise().sum()/numRepeats;
    Eigen::VectorXd varMeans  = (allMeans.colwise()-meanMeans).array().pow(2).matrix().rowwise().sum()/(numRepeats-1);
    
    // blasphemy as it may be to Bayesians, here we perform a two sided t-test on the MCMC mean
    boost::math::students_t_distribution<double> tdist(numRepeats-1);
    for(int d=0; d<trueMean.size(); ++d){
      double t = (meanMeans(d)-trueMean(d))*sqrt(double(numRepeats))/sqrt(varMeans(d));
      double probDiff =  2*boost::math::cdf(boost::math::complement(tdist, fabs(t))); // the 2 comes from the fact that this is a 2 sided test
      if(probDiff<0.02)
        std::cout << "Dimension " << d << " of mean failed with p = " << probDiff << std::endl;
      
      EXPECT_GT(probDiff, 0.02); // the probability that the MCMC estimator is different from the true mean should be less than 2%
    }
  }

  boost::property_tree::ptree params;
  Eigen::VectorXd trueMean;
  Eigen::MatrixXd trueCov;
  std::shared_ptr<SamplingProblem> gaussTest;
  const double testTol;
  
};

// Tests that the Foo::Bar() method does Abc.
TEST_F(MCMCConvergenceTests, MALA) {
  params.put("MCMC.Proposal", "MMALA");
  params.put("MCMC.Kernel", "MHKernel");
  params.put("MCMC.MHProposal.PropSize", 1);
  params.put("MCMC.MMALA.AdaptDelay", 100);
}

TEST_F(MCMCConvergenceTests, MH)
{
  params.put("MCMC.Proposal", "MHProposal");
  params.put("MCMC.Kernel", "MHKernel");
  params.put("MCMC.MHProposal.PropSize", 2);
}

TEST_F(MCMCConvergenceTests, preMALA)
{
  params.put("MCMC.Kernel", "MHKernel");
  params.put("MCMC.Proposal", "PreMALA");
  params.put("MCMC.PreMALA.StepLength", 1);
  params.put("MCMC.PreMALA.AdaptDelay", 100);
}


TEST_F(MCMCConvergenceTests, AMALA)
{
  params.put("MCMC.Kernel", "MHKernel");
  params.put("MCMC.Proposal", "AMALA");
  params.put("MCMC.AMALA.AdaptSteps", 10);
  params.put("MCMC.AMALA.AdaptStart", 100);
  params.put("MCMC.AMALA.AdaptScale", 1);
  params.put("MCMC.AMALA.MaxDrift",   2.0);
}


TEST_F(MCMCConvergenceTests, AM)
{
  params.put("MCMC.Kernel", "MHKernel");
  params.put("MCMC.Proposal", "AM");
  params.put("MCMC.MHProposal.PropSize", 0.5);
  params.put("MCMC.AM.AdaptSteps", 2);
  params.put("MCMC.AM.AdaptStart", 100);
  params.put("MCMC.AM.AdaptScale", 1.2);
}

TEST_F(MCMCConvergenceTests,  DRDistinctProposals)
{
  params.put("MCMC.Kernel", "DR");

  params.put("MCMC.DR.ProposalSpecification", "Proposal_0,Proposal_1,Proposal_2");
  params.put("MCMC.DR.Proposal_0.Proposal", "MHProposal");
  params.put("MCMC.DR.Proposal_1.Proposal", "MMALA");
  params.put("MCMC.DR.Proposal_2.Proposal", "AM");

  params.put("MCMC.DR.Proposal_2.AM.AdaptSteps", 2);
  params.put("MCMC.DR.Proposal_2.AM.AdaptStart", 5);
  params.put("MCMC.DR.Proposal_2.AM.AdaptScale", 1.2);

  params.put("MCMC.DR.Proposal_0.MHProposal.PropSize", 1.5);
  params.put("MCMC.DR.stages", 3);
  params.put("MCMC.DR.NumSteps", 5000000);

  params.put("MCMC.DR.Proposal_1.MMALA.AdaptDelay", 4);
}


TEST_F(MCMCConvergenceTests, Mixture)
{
  params.put("MCMC.Kernel", "MHKernel");

  params.put("MCMC.Proposal", "MixtureProposal");

  params.put("MCMC.MixtureProposal.Proposal_0.Proposal", "MHProposal");
  params.put("MCMC.MixtureProposal.Proposal_1.Proposal", "MMALA");
  params.put("MCMC.MixtureProposal.Proposal_2.Proposal", "AM");

  params.put("MCMC.MixtureProposal.Proposal_0.MHProposal.PropSize", 1.5);
  params.put("MCMC.MixtureProposal.Proposal_0.Weight", 0.4);

  params.put("MCMC.MixtureProposal.Proposal_1.MMALA.AdaptDelay", 4);
  params.put("MCMC.MixtureProposal.Proposal_1.Weight", 0.3);

  params.put("MCMC.MixtureProposal.Proposal_2.Weight", 0.3);
  params.put("MCMC.MixtureProposal.Proposal_2.AM.AdaptSteps", 2);
  params.put("MCMC.MixtureProposal.Proposal_2.AM.AdaptStart", 5);
  params.put("MCMC.MixtureProposal.Proposal_2.AM.AdaptScale", 1.2);
}

TEST_F(MCMCConvergenceTests, DRAM)
{
  params.put("MCMC.Kernel", "DR");
  params.put("MCMC.DR.stages", 3);
  params.put("MCMC.DR.ProposalSpecification", "DRAM");
  params.put("MCMC.MHProposal.PropSize", 1.5);
  params.put("MCMC.DR.stages", 3);
  params.put("MCMC.DR.NumSteps", 500000);
  params.put("MCMC.AM.AdaptSteps", 2);
  params.put("MCMC.AM.AdaptStart", 100);
  params.put("MCMC.AM.AdaptScale", 2.4 * 2.4 / 2.0);
}

TEST_F(MCMCConvergenceTests, StanHMC)
{
  params.put("MCMC.Kernel", "StanHMC");
  params.put("MCMC.StanHMC.Leaps", 5);
}

TEST_F(MCMCConvergenceTests, StanNUTS)
{
  params.put("MCMC.Kernel", "StanNUTS");
}
