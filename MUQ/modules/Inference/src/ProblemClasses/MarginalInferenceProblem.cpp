#include <boost/property_tree/ptree.hpp> // needed to avoid weird toupper bug on osx

#include "MUQ/Inference/ProblemClasses/MarginalInferenceProblem.h"

using namespace std;
using namespace muq::Utilities;
using namespace muq::Modelling;
using namespace muq::Inference;

shared_ptr<MCMCState> MarginalInferenceProblem::ConstructState(Eigen::VectorXd const& state, int const currIteration, shared_ptr<HDF5LogEntry> logEntry)
{
  assert(posteriorGraph->inputSizes().rows() == 2);

  //compute the prior first, in case it's not finite
  auto   logPrior =  boost::optional<double>(prior->LogDensity(state));
  double logLikelihood;

  boost::optional<Eigen::VectorXd> likelihoodGrad;

  boost::optional<Eigen::MatrixXd> metric;
  boost::optional<Eigen::VectorXd> priorGrad;

  //next compute the likelihood
  if (std::isfinite(*logPrior)) {
    //likelihoodIP->SetSamplingInputs(state);
    logLikelihood = likelihoodIP->Evaluate(state) (0);
  } else {
    return nullptr; //invalid if prior is not finite
  }
  //also invalid if likelihood is not finite
  if (!std::isfinite(logLikelihood)) {
    return nullptr;
  }

  boost::optional<Eigen::VectorXd> forwardModel;
  /*  if (forwardMod) {
   *  forwardModel =  boost::optional<Eigen::VectorXd>(forwardMod->Evaluate(state));
   *  } else {
   *  forwardModel =  boost::optional<Eigen::VectorXd>();
   *  }*/

  //compute metric as desired
  if (useMetric) {
    metric = boost::optional<Eigen::MatrixXd>(metricObject->ComputeMetric(state));
  } else {
    metric = boost::optional<Eigen::MatrixXd>();
  }

  //compute prior grad as desired
  if (usePriorGrad) {
    priorGrad = boost::optional<Eigen::MatrixXd>(prior->Gradient(state, Eigen::VectorXd::Constant(1, 1.0), 0));
  } else {
    priorGrad = boost::optional<Eigen::VectorXd>();
  }

  auto mcmcState = make_shared<MCMCState>(state, logLikelihood, likelihoodGrad, logPrior, priorGrad, metric,
                                          forwardModel);

  return mcmcState;
}

