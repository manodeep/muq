#include "MUQ/Inference/MCMC/MCMCProposal.h"

#include <assert.h>

#include "MUQ/Inference/ProblemClasses/SamplingProblem.h"
#include "MUQ/Utilities/LogConfig.h"
#include "MUQ/Utilities/HDF5Logging.h"

using namespace muq::Inference;
using namespace muq::Utilities;

MCMCProposal::MCMCProposal(std::shared_ptr<AbstractSamplingProblem> samplingProblem,
                           boost::property_tree::ptree            & properties) : samplingProblem(samplingProblem)
{
  assert(this->samplingProblem->samplingDim > 0);
}

std::shared_ptr<MCMCProposal> MCMCProposal::Create(std::shared_ptr<AbstractSamplingProblem> samplingProblem,
                                                   boost::property_tree::ptree            & properties)
{
  auto map        = GetFactoryMap();
  auto propString = ReadAndLogParameter<std::string>(properties, "MCMC.Proposal", "MHProposal");

  LOG(INFO) << "Trying to construct proposal: " << propString;
  if(map->find(propString) == map->end()){
	  std::cerr << "ERROR: The MCMC proposal " << propString << " is invalid.  Valid options are:\n";
	  for(auto iter=map->begin(); iter!=map->end(); ++iter)
	    std::cerr << iter->first << "\n";
	  std::cerr << std::endl << std::endl;
	  assert(map->find(propString) != map->end());
  }
  
  //lookup the one we want and construct it
  return map->at (propString) (samplingProblem, properties);
}

