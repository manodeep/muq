#include "MUQ/Inference/TransportMaps/Sigmoids.h"

#include <boost/math/special_functions/erf.hpp>
#include <boost/math/constants/constants.hpp>

using namespace boost::math;
using namespace boost::math::constants;
using namespace muq::Inference;
using namespace std;


double sigmoid_sign(double val) {
  return static_cast<double>((0.0 < val) - (val < 0.0));
}

double Sigmoid1::Evaluate(const double x) const{
  const double diff = x-center;
  const double r = abs(diff);
  return erf(r/a)*r*diff;
}

double Sigmoid1::Derivative(const double x) const{
  const double diff = x-center;
  const double r = abs(diff);
  return  (r * erf(r / a)) +  diff * fabs(diff) /  diff *  erf(r / a) + 2.0 *  diff *  r * pow(pi<double>(), -0.5) * exp(- ( pow( r,  2) *  pow( a,  (-2)))) * r /  diff /  a;
}

double Sigmoid1::SecondDerivative(const double x) const{
  const double diff = x-center;
  const double r = abs(diff);
  double temp = exp(-r*r/(a*a));
  const double sign = sigmoid_sign(diff);
  
  return 2*sign*erf(r/a)+4.0*r*temp*sign/(sqrt(pi<double>())*a) +4*diff*temp/(sqrt(pi<double>())*a) - 4.0*diff*pow(r,2)*temp/(sqrt(pi<double>())*pow(a,3));
}


double Sigmoid2::Evaluate(const double x) const{
  const double diff = x-center;
  const double r = abs(diff);
  return (x-center)/(a+r);
}

double Sigmoid2::Derivative(const double x) const{
  const double diff = x-center;
  const double r = abs(diff);
  return 1.0 / (r + a) - diff * pow((r + a), -2.0) * r / diff;
}

double Sigmoid2::SecondDerivative(const double x) const{
  const double diff = x-center;
  const double r = abs(diff);
  const double sign = sigmoid_sign(diff);
  return -2.0*sign/pow(r+a,2.0) + 2.0*diff/pow(r+a,3.0);
}


double Sigmoid3::Evaluate(const double x) const{
  const double diff = x-center;
  const double r = abs(diff);
  if(r<a){
    return (x-center)*pow(1-r/a,2.0)/a;
  }else{
    return 0;
  }
}

double Sigmoid3::Derivative(const double x) const{
  const double diff = x-center;
  const double r = abs(diff);
  if(r<a){
    return -2.0 * (1 - r / a) * diff * pow(a, -2.0) * r / diff + pow((1 - r / a), 2) / a;
  }else{
    return 0;
  }
}

double Sigmoid3::SecondDerivative(const double x) const{
  const double diff = x-center;
  const double r = abs(diff);
  if(r<a){
    return 2.0*diff/pow(a,3) - 4.0*(1-r/a)*sigmoid_sign(diff)/pow(a,2);
  }else{
    return 0;
  }
}

