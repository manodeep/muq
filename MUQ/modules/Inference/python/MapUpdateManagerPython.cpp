#include "MUQ/Inference/python/MapUpdateManagerPython.h"

using namespace std;
using namespace muq::Utilities;
using namespace muq::Inference;

MapUpdateManagerPython::MapUpdateManagerPython(std::shared_ptr<TransportMap> mapIn,
                                               int                           expectedSamps,
                                               double                        priorPrecisionIn) :
  MapUpdateManager(mapIn, expectedSamps, priorPrecisionIn) {}

MapUpdateManagerPython::MapUpdateManagerPython(std::shared_ptr<TransportMap> mapIn, int expectedSamps) :
  MapUpdateManager(mapIn, expectedSamps) {}

void MapUpdateManagerPython::PyUpdateMap(boost::python::list const& pyNewSamps)
{
  UpdateMap(GetEigenMatrix(pyNewSamps));
}

boost::python::list MapUpdateManagerPython::PyGetInducedDensities(int startInd, int segmentLength) const
{
  return GetPythonVector<Eigen::VectorXd>(GetInducedDensities(startInd, segmentLength));
}

void muq::Inference::ExportMapUpdateManager()
{
  boost::python::class_<MapUpdateManagerPython, shared_ptr<MapUpdateManagerPython>, boost::noncopyable> exportMapUp(
    "MapUpdateManager", boost::python::init<shared_ptr<TransportMap>, int, double>());

  exportMapUp.def(boost::python::init<shared_ptr<TransportMap>, int>());

  exportMapUp.def("UpdateMap", &MapUpdateManagerPython::PyUpdateMap);
  exportMapUp.def("GetInducedDensities", &MapUpdateManagerPython::PyGetInducedDensities);
}

