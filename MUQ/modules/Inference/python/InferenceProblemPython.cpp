#include "MUQ/Inference/python/InferenceProblemPython.h"

using namespace std;
using namespace muq::Modelling;
using namespace muq::Inference;


void muq::Inference::ExportProblemClasses()
{
  // sampling problem
  boost::python::class_<SamplingProblem, std::shared_ptr<SamplingProblem>, boost::noncopyable> exportSamplingProblem(
    "SamplingProblem",
    boost::python::init<std::shared_ptr<muq::Modelling::Density> >());
  exportSamplingProblem.def(boost::python::init<std::shared_ptr<muq::Modelling::ModGraph>,
                                                 std::shared_ptr<muq::Inference::AbstractMetric> >());
  exportSamplingProblem.def(boost::python::init<std::shared_ptr<muq::Modelling::ModGraph>>());
  
  // Inference problem
  boost::python::class_<InferenceProblem, std::shared_ptr<InferenceProblem>,
                        boost::noncopyable> exportInferenceProblem("InferenceProblem",
                                                                   boost::python::init<std::shared_ptr<muq::Modelling::ModGraph> >());

  exportInferenceProblem.def(boost::python::init<std::shared_ptr<muq::Modelling::ModGraph>,
                                                 std::shared_ptr<muq::Inference::AbstractMetric> >());

  // register AbstractSamplingProblem pointer to boost::python
  boost::python::register_ptr_to_python<std::shared_ptr<AbstractSamplingProblem> >();

  // allow conversion to AbstractSamplingProblem
  boost::python::implicitly_convertible<std::shared_ptr<SamplingProblem>, std::shared_ptr<AbstractSamplingProblem> >();
  boost::python::implicitly_convertible<std::shared_ptr<InferenceProblem>,
                                        std::shared_ptr<AbstractSamplingProblem> >();
}
