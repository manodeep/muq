
#include "MUQ/Modelling/python/ModGraphPython.h"

#include <boost/graph/copy.hpp>

#include "MUQ/Utilities/python/PythonTranslater.h"
#include "MUQ/Modelling/ModGraphOperators.h"
#include "MUQ/Modelling/python/ModGraphOperationsPython.h"

using namespace std;
using namespace muq::Modelling;


ModGraphPython::ModGraphPython(std::shared_ptr<ModGraph> base_ptr) : ModGraph()
{
  boost::copy_graph(base_ptr->ModelGraph, ModelGraph);
}

void ModGraphPython::PySwapNode(string const& name, std::shared_ptr<ModPiece> const& input)
{
  // add the node
  SwapNode(name, input);
}

void ModGraphPython::PyBindNode(string const& name, boost::python::list const& in)
{
  Eigen::VectorXd eigIn = muq::Utilities::GetEigenVector<Eigen::VectorXd>(in);

  BindNode(name, eigIn);
}

void ModGraphPython::PyBindEdge(std::string const& name, int inputDim, boost::python::list const& in)
{
  Eigen::VectorXd eigIn = muq::Utilities::GetEigenVector<Eigen::VectorXd>(in);

  BindEdge(name, inputDim, eigIn);
}

bool ModGraphPython::PyIsConstant(std::string const& name)
{
  return isConstant(name);
}

shared_ptr<ModGraphPython> ModGraphPython::PyDependentCut(const std::string& nameOut)
{
  // copy everything upstream of this node to a new graph and trim constant branches to a single ModParameter
  auto newGraph = make_shared<ModGraphPython>();

  auto oldV = GetNodeIterator(nameOut);

  // add a new node to the output graph to hold the output node
  auto newV = boost::add_vertex(newGraph->ModelGraph);

  // copy the output node
  newGraph->ModelGraph[newV] = ModelGraph[*oldV];

  // recurse through graph
  RecursiveCopyAndCut(*oldV, newV, newGraph);

  // return result
  return newGraph;
}

bool ModGraphPython::PyNodesConnected(const std::string& name1, const std::string& name2) const
{
  return NodesConnected(name1, name2);
}

shared_ptr<ModGraphPiece> ModGraphPython::PyConstructModPiece(std::shared_ptr<ModGraph> graph, const string& outNode)
{
  return ModGraph::ConstructModPiece(graph, outNode);
}

shared_ptr<ModGraphPiece> ModGraphPython::PyConstructModPieceOrder(std::shared_ptr<ModGraph>  graph,
                                                                   const string             & outNode,
                                                                   const boost::python::list& inputOrder)
{
  const unsigned int N = boost::python::len(inputOrder);

  vector<string> inOrder(N);

  for (unsigned int i = 0; i < N; ++i) {
    inOrder[i] = boost::python::extract<string>(inputOrder[i]);
  }

  return ModGraph::ConstructModPiece(graph, outNode, inOrder);
}

shared_ptr<ModPieceDensity> ModGraphPython::PyConstructDensity(std::shared_ptr<ModGraph> graph, const string& outNode)
{
  return ModGraph::ConstructDensity(graph, outNode);
}

shared_ptr<ModPieceDensity> ModGraphPython::PyConstructDensityOrder(std::shared_ptr<ModGraph>  graph,
							    const string             & outNode,
							    const boost::python::list& inputOrder)
{
  const unsigned int N = boost::python::len(inputOrder);

  vector<string> inOrder(N);

  for (unsigned int i = 0; i < N; ++i) {
    inOrder[i] = boost::python::extract<string>(inputOrder[i]);
  }

  return ModGraph::ConstructDensity(graph, outNode, inOrder);
}

void ModGraphPython::PyWriteGraphViz0(const string& filename) const
{
  writeGraphViz(filename);
}

void ModGraphPython::PyWriteGraphViz1(const string& filename, colorOptionsType colorOption) const
{
  writeGraphViz(filename, colorOption);
}

void ModGraphPython::PyWriteGraphViz2(const string& filename, colorOptionsType colorOption, bool addDerivLabel) const
{
  writeGraphViz(filename, colorOption, addDerivLabel);
}



void muq::Modelling::ExportModGraph()
{
  boost::python::class_<ModGraphPython, shared_ptr<ModGraphPython>, boost::noncopyable> exportModGraph("ModGraph");

  boost::python::enum_<ModGraphPython::colorOptionsType>("colorOptionsType")
    .value("DEFAULT_COLOR",ModGraph::DEFAULT_COLOR)
    .value("DERIV_COLOR",ModGraph::DERIV_COLOR)
    .value("CALLS_COLOR",ModGraph::CALLS_COLOR)
    .value("TIME_COLOR",ModGraph::TIME_COLOR)
    .export_values()
  ;
  
  exportModGraph.def(boost::python::init < std::shared_ptr < muq::Modelling::ModGraph >> ());
  exportModGraph.def(boost::python::init < std::shared_ptr < muq::Modelling::ModPiece >> ());
  exportModGraph.def("AddNode", &ModGraph::AddNode);
  exportModGraph.def("NumOutputs", &ModGraph::NumOutputs);
  exportModGraph.def("NumInputs", &ModGraph::NumInputs);
  exportModGraph.def("AddEdge", &ModGraph::AddEdge);
  exportModGraph.def("GetNodeModel", &ModGraph::GetNodeModel);
  exportModGraph.def("SwapNode", &ModGraph::SwapNode);
  exportModGraph.def("SwapNode", &ModGraphPython::PySwapNode);
  exportModGraph.def("BindNode", &ModGraphPython::PyBindNode);
  exportModGraph.def("BindEdge", &ModGraphPython::PyBindEdge);
  exportModGraph.def("isConstant", &ModGraphPython::PyIsConstant);
  exportModGraph.def("DependentCut", &ModGraphPython::PyDependentCut);
  exportModGraph.def("NumNodes", &ModGraph::NumNodes);
  exportModGraph.def("writeGraphViz", &ModGraphPython::PyWriteGraphViz0);
  exportModGraph.def("writeGraphViz", &ModGraphPython::PyWriteGraphViz1);
  exportModGraph.def("writeGraphViz", &ModGraphPython::PyWriteGraphViz2);
  exportModGraph.def("NodesConnected", &ModGraphPython::PyNodesConnected);
  exportModGraph.def("ConstructModPiece", &ModGraphPython::PyConstructModPiece);
  exportModGraph.def("ConstructModPiece", &ModGraphPython::PyConstructModPieceOrder);
  exportModGraph.def("ConstructDensity", &ModGraphPython::PyConstructDensity);
  exportModGraph.def("ConstructDensity", &ModGraphPython::PyConstructDensityOrder);
  exportModGraph.def("RemoveEdge", &ModGraph::RemoveEdge);
  exportModGraph.def("GetOutputNodeName", &ModGraph::GetOutputNodeName);


  exportModGraph.def("__add__",
                     static_cast<std::shared_ptr<muq::Modelling::ModGraphPython>(*)(const std::shared_ptr<ModGraph>&,
                                                                                    const std::shared_ptr<ModPiece>&)>(&
                                                                                                                       muq
                                                                                                                       ::
  Modelling::PyAdd));
  exportModGraph.def("__add__",
                     static_cast<std::shared_ptr<muq::Modelling::ModGraphPython>(*)(const std::shared_ptr<ModGraph>& a,
                                                                                    const std::shared_ptr<ModGraph>& b)>(
                       &muq::Modelling::PyAdd));
  exportModGraph.def("__sub__",
                     static_cast<std::shared_ptr<muq::Modelling::ModGraphPython>(*)(const std::shared_ptr<ModGraph>& a,
                                                                                    const std::shared_ptr<ModGraph>& b)>(
                       &muq::Modelling::PySub));
  exportModGraph.def("__sub__",
                     static_cast<std::shared_ptr<muq::Modelling::ModGraphPython>(*)(const std::shared_ptr<ModGraph>& a,
                                                                                    const std::shared_ptr<ModPiece>& b)>(
                       &muq::Modelling::PySub));
  exportModGraph.def("__mul__",
                     static_cast<std::shared_ptr<muq::Modelling::ModGraphPython>(*)(const std::shared_ptr<ModGraph>& a,
                                                                                    const std::shared_ptr<ModGraph>& b)>(
                       &muq::Modelling::PyMul));
  exportModGraph.def("__mul__",
                     static_cast<std::shared_ptr<muq::Modelling::ModGraphPython>(*)(const std::shared_ptr<ModGraph>& a,
                                                                                    const std::shared_ptr<ModPiece>& b)>(
                       &muq::Modelling::PyMul));
  exportModGraph.def("__neg__",
                     static_cast<std::shared_ptr<muq::Modelling::ModGraphPython>(*)(
                                   const std::shared_ptr<ModGraph>& a)>(&muq::Modelling::
                                                                        PySub));

  //constant ops
  exportModGraph.def("__add__",
                     static_cast<std::shared_ptr<muq::Modelling::ModGraphPython>(*)(const std::shared_ptr<ModGraph>& a,
                                                                                    const double& b)>(&muq::Modelling::
                                                                                                      PyAdd));
  exportModGraph.def("__radd__",
                     static_cast<std::shared_ptr<muq::Modelling::ModGraphPython>(*)(const std::shared_ptr<ModGraph>& a,
                                                                                    const double& b)>(&muq::Modelling::
                                                                                                      PyAdd));
  exportModGraph.def("__sub__",
                     static_cast<std::shared_ptr<muq::Modelling::ModGraphPython>(*)(const std::shared_ptr<ModGraph>& a,
                                                                                    const double& b)>(&muq::Modelling::
                                                                                                      PySub));
  exportModGraph.def("__rsub__",
                     static_cast<std::shared_ptr<muq::Modelling::ModGraphPython>(*)(const std::shared_ptr<ModGraph>& b,
                                                                                    const double& a)>(&muq::Modelling::
                                                                                                      PyRSub));
  exportModGraph.def("__mul__",
                     static_cast<std::shared_ptr<muq::Modelling::ModGraphPython>(*)(const std::shared_ptr<ModGraph>& a,
                                                                                    const double& b)>(&muq::Modelling::
                                                                                                      PyMul));
  exportModGraph.def("__rmul__",
                     static_cast<std::shared_ptr<muq::Modelling::ModGraphPython>(*)(const std::shared_ptr<ModGraph>& a,
                                                                                    const double& b)>(&muq::Modelling::
                                                                                                      PyMul));
  exportModGraph.def("__div__",
                     static_cast<std::shared_ptr<muq::Modelling::ModGraphPython>(*)(const std::shared_ptr<ModGraph>& a,
                                                                                    const double& b)>(&muq::Modelling::
                                                                                                      PyDiv));


  //vector ops
  exportModGraph.def("__rmul__",
                     static_cast<std::shared_ptr<muq::Modelling::ModGraphPython>(*)(const std::shared_ptr<ModGraph>& x,
                                                                                    const  boost::python::list& A)>(&muq
                                                                                                                    ::
                                                                                                                    Modelling
  ::PyRMul));
  exportModGraph.def("__radd__",
                     static_cast<std::shared_ptr<muq::Modelling::ModGraphPython>(*)(const std::shared_ptr<ModGraph>& x,
                                                                                    const boost::python::list& b)>(&muq
                                                                                                                   ::
                                                                                                                   Modelling
  ::PyAdd));
  exportModGraph.def("__add__",
                     static_cast<std::shared_ptr<muq::Modelling::ModGraphPython>(*)(const std::shared_ptr<ModGraph>& x,
                                                                                    const boost::python::list& b)>(&muq
                                                                                                                   ::
                                                                                                                   Modelling
  ::PyAdd));
  exportModGraph.def("__sub__",
                     static_cast<std::shared_ptr<muq::Modelling::ModGraphPython>(*)(const std::shared_ptr<ModGraph>& x,
                                                                                    const boost::python::list& b)>(&muq
                                                                                                                   ::
                                                                                                                   Modelling
  ::PySub));
  exportModGraph.def("__rsub__",
                     static_cast<std::shared_ptr<muq::Modelling::ModGraphPython>(*)(const std::shared_ptr<ModGraph>& x,
                                                                                    const boost::python::list& b)>(&muq
                                                                                                                   ::
                                                                                                                   Modelling
  ::PyRSub));

  boost::python::def("GraphCompose", &muq::Modelling::GraphCompose_graph_impl_py);


  // allow conversion to parent
  boost::python::implicitly_convertible<shared_ptr<ModGraphPython>, shared_ptr<ModGraph> >();
}

