/*
 *  This program is free software; you can redistribute it and/or modify it under the terms of the GNU General Public
 *License as published by the Free Software Foundation; either version 2 of the License, or (at your option) any later
 *version.
 *
 *  This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied
 *warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License along with this program; if not, write to the Free
 *Software Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.
 *
 *  MIT UQ Library Copyright (C) 2013 MIT
 */

#include "MUQ/Modelling/python/ModGraphPiecePython.h"

using namespace std;
using namespace muq::Modelling;

shared_ptr<ModGraphPiece> ModGraphPiecePython::PyCreateDefaultOrder(shared_ptr<ModGraphPython>& inGraph,
                                                                    string const                   & outNode)
{
  return Create(inGraph, outNode);
}

shared_ptr<ModGraphPiece> ModGraphPiecePython::PyCreateUserOrder(shared_ptr<ModGraphPython>& inGraph,
                                                                 string const              & outNode,
                                                                 boost::python::list const & inputOrder)
{
  vector<string> inOrd(boost::python::len(inputOrder));
  for (int i = 0; i < boost::python::len(inputOrder); ++i) {
    const string name = boost::python::extract<string>(inputOrder[i]);
    inOrd[i] = name;
  }

  return Create(inGraph, outNode, inOrd);
}

void muq::Modelling::ExportModGraphPiece()
{
  boost::python::class_<ModGraphPiecePython, shared_ptr<ModGraphPiece>, boost::noncopyable,
                        boost::python::bases<ModPiece> > exportModGraphPiece("ModGraphPiece", boost::python::no_init);

  // create methods
  exportModGraphPiece.def("__init__", boost::python::make_constructor(&ModGraphPiecePython::PyCreateDefaultOrder));
  exportModGraphPiece.def("__init__", boost::python::make_constructor(&ModGraphPiecePython::PyCreateUserOrder));

  // explicitly say which implementation to use.  If we don't do this it might try to use one of the "OneInput"
  // implentations and get confused...
  exportModGraphPiece.def("Gradient", &ModPiece::PyGradient);
  exportModGraphPiece.def("Jacobian", &ModPiece::PyJacobian);
  exportModGraphPiece.def("JacobianAction", &ModPiece::PyJacobianAction);
  exportModGraphPiece.def("Hessian", &ModPiece::PyHessian);

  // write graph to file
  exportModGraphPiece.def("writeGraphViz", &ModGraphPiece::PyWriteGraphViz0);
  exportModGraphPiece.def("writeGraphViz", &ModGraphPiece::PyWriteGraphViz1);
  exportModGraphPiece.def("writeGraphViz", &ModGraphPiece::PyWriteGraphViz2);

  boost::python::implicitly_convertible<shared_ptr<ModGraphPiecePython>, shared_ptr<ModGraphPiece> >();
  boost::python::implicitly_convertible<shared_ptr<ModGraphPiece>, shared_ptr<ModPiece> >();
}

shared_ptr<ModGraphDensity> ModGraphDensityPython::PyCreateDefaultOrder(shared_ptr<ModGraphPython>& inGraph,
                                                                        string const                   & outNode)
{
  return Create(inGraph, outNode);
}

void muq::Modelling::ExportModGraphDensity()
{
  boost::python::class_<ModGraphDensityPython, shared_ptr<ModGraphDensity>, boost::noncopyable,
                        boost::python::bases<Density> > exportModGraphPiece("ModGraphDensity", boost::python::no_init);

  // create methods
  exportModGraphPiece.def("__init__", boost::python::make_constructor(&ModGraphDensityPython::PyCreateDefaultOrder));

  // explicitly say which implementation to use.  If we don't do this it might try to use one of the "OneInput"
  // implentations and get confused...
  exportModGraphPiece.def("LogDensity", &Density::PyLogDensity);

  boost::python::implicitly_convertible<shared_ptr<ModGraphDensityPython>, shared_ptr<ModGraphDensity> >();
  boost::python::implicitly_convertible<shared_ptr<ModGraphDensityPython>, shared_ptr<Density> >();
  boost::python::implicitly_convertible<shared_ptr<ModGraphDensity>, shared_ptr<Density> >();
  boost::python::implicitly_convertible<shared_ptr<ModGraphDensity>, shared_ptr<ModPiece> >();
}

