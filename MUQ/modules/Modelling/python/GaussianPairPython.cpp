#include "MUQ/Modelling/python/GaussianPairPython.h"

using namespace std;
namespace py = boost::python;
using namespace muq::Utilities;
using namespace muq::Modelling;

GaussianPairPython::GaussianPairPython(unsigned int const dimIn) : GaussianPair(dimIn) {}

GaussianPairPython::GaussianPairPython(unsigned int const dimIn, py::list const& inputSizes) :
  GaussianPair(dimIn, GetEigenVector<Eigen::VectorXi>(inputSizes)) {}

GaussianPairPython::GaussianPairPython(unsigned int const dimIn, double const scalarCov) :
  GaussianPair(dimIn, scalarCov) {}

GaussianPairPython::GaussianPairPython(py::list const& mu, double const scaledCov) :
  GaussianPair(GetEigenVector<Eigen::VectorXd>(mu), scaledCov) {}

GaussianPairPython::GaussianPairPython(py::list const& mu,
                                       py::list const& diag) :
  GaussianPair(GetEigenVector<Eigen::VectorXd>(mu), GetEigenVector<Eigen::VectorXd>(diag)) {}

GaussianPairPython::GaussianPairPython(Eigen::VectorXd const                         & mu,
                                       Eigen::VectorXd const                         & diag,
                                       GaussianSpecification::SpecificationMode const& mode) :
  GaussianPair(mu, diag, mode) {}

GaussianPairPython::GaussianPairPython(Eigen::VectorXd const                         & mu,
                                       Eigen::MatrixXd const                         & mat,
                                       GaussianSpecification::SpecificationMode const& mode) :
  GaussianPair(mu, mat, mode) {}

GaussianPairPython::GaussianPairPython(unsigned int const                              dim,
                                       Eigen::VectorXd const                         & mu,
                                       Eigen::MatrixXd const                         & mat,
                                       GaussianSpecification::SpecificationMode const& mode) :
  GaussianPair(dim, mu, mat, mode) {}

shared_ptr<GaussianPairPython> GaussianPairPython::Create(
  py::list const                                & mu,
  py::list const                                & cov_or_prec,
  GaussianSpecification::SpecificationMode const& mode)
{
  if ((mode == GaussianSpecification::DiagonalPrecision) || (mode == GaussianSpecification::DiagonalCovariance)) {
    return make_shared<GaussianPairPython>(GetEigenVector<Eigen::VectorXd>(mu),
                                           GetEigenVector<Eigen::VectorXd>(cov_or_prec),
                                           mode);
  }

  if ((mode == GaussianSpecification::PrecisionMatrix) || (mode == GaussianSpecification::CovarianceMatrix)) {
    return make_shared<GaussianPairPython>(GetEigenVector<Eigen::VectorXd>(mu),
                                           GetEigenMatrix(cov_or_prec),
                                           mode);
  }

  assert((mode == GaussianSpecification::DiagonalPrecision) ||
         (mode == GaussianSpecification::DiagonalCovariance) ||
         (mode == GaussianSpecification::CovarianceMatrix) ||
         (mode == GaussianSpecification::PrecisionMatrix));

  return nullptr;
}

shared_ptr<GaussianPairPython> GaussianPairPython::CreateConditional(
  unsigned int const                              dim,
  py::list const                                & mu,
  py::list const                                & cov_or_prec,
  GaussianSpecification::SpecificationMode const& mode)
{
  if ((mode == GaussianSpecification::PrecisionMatrix) || (mode == GaussianSpecification::CovarianceMatrix)) {
    return make_shared<GaussianPairPython>(dim,
                                           GetEigenVector<Eigen::VectorXd>(mu),
                                           GetEigenMatrix(cov_or_prec),
                                           mode);
  }

  assert((mode == GaussianSpecification::CovarianceMatrix) ||
         (mode == GaussianSpecification::PrecisionMatrix));

  return nullptr;
}

shared_ptr<GaussianDensity> GaussianPairPython::GetDensity() const 
{
  return dynamic_pointer_cast<GaussianDensity>(density);
}
  
shared_ptr<GaussianRV> GaussianPairPython::GetRV() const
{
  return dynamic_pointer_cast<GaussianRV>(rv);
}

void muq::Modelling::ExportGaussianPair()
{
  py::class_<GaussianPairPython, shared_ptr<GaussianPairPython>, boost::noncopyable> exportGaussianPair(
    "GaussianPair", py::init<unsigned int const>());

  exportGaussianPair.def(py::init<unsigned int const, py::list const>());
  exportGaussianPair.def(py::init<unsigned int const, double const>());
  exportGaussianPair.def(py::init<py::list const&, double const>());
  exportGaussianPair.def(py::init<py::list const&, py::list const&>());
  exportGaussianPair.def("__init__", py::make_constructor(&GaussianPairPython::Create));
  exportGaussianPair.def("__init__", py::make_constructor(&GaussianPairPython::CreateConditional));

  exportGaussianPair.def("GetDensity", &GaussianPairPython::GetDensity);
  exportGaussianPair.def("GetRV", &GaussianPairPython::GetRV);

  // convert to parent, ModPiece, and RandVar
  py::implicitly_convertible<shared_ptr<GaussianPairPython>, shared_ptr<GaussianPair> >();
}

