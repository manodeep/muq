
#include "MUQ/Modelling/python/MvNormDensPython.h"

using namespace std;
using namespace muq::Modelling;
using namespace muq::Utilities;

mvNormDensPython::mvNormDensPython(std::shared_ptr<ConstantMVNormSpecification> const& specification) : mvNormDens(
                                                                                                          specification)
{}

mvNormDensPython::mvNormDensPython(int dimIn) : mvNormDens(dimIn) {}

mvNormDensPython::mvNormDensPython(boost::python::list const& meanIn,
                                   double                     scalarCovariance) : mvNormDens(GetEigenVector<Eigen::
                                                                                                            VectorXd>(
                                                                                               meanIn),
                                                                                             scalarCovariance)
{}


mvNormDensPython::mvNormDensPython(Eigen::VectorXd const                        & meanIn,
                                   Eigen::VectorXd const                        & covDiag,
                                   ConstantMVNormSpecification::SpecificationMode mode) : mvNormDens(meanIn, covDiag,
                                                                                                     mode)
{}

mvNormDensPython::mvNormDensPython(Eigen::VectorXd const                        & meanIn,
                                   Eigen::MatrixXd const                        & cov,
                                   ConstantMVNormSpecification::SpecificationMode mode) : mvNormDens(meanIn, cov,
                                                                                                     mode)
{}

shared_ptr<mvNormDensPython> mvNormDensPython::Create(boost::python::list const& meanIn,
                                                      boost::python::list const& covIn,
                                                      bool                       isDiag)
{
  if (isDiag) {
    return make_shared<mvNormDensPython>(GetEigenVector<Eigen::VectorXd>(meanIn),
                                         GetEigenVector<Eigen::VectorXd>(covIn));
  } else {
    return make_shared<mvNormDensPython>(GetEigenVector<Eigen::VectorXd>(meanIn), GetEigenMatrix(covIn));
  }
}

shared_ptr<mvNormDensPython> mvNormDensPython::CreateMode(boost::python::list const                    & meanIn,
                                                          boost::python::list const                    & covIn,
                                                          bool                                           isDiag,
                                                          ConstantMVNormSpecification::SpecificationMode mode)
{
  if (isDiag) {
    return make_shared<mvNormDensPython>(GetEigenVector<Eigen::VectorXd>(meanIn), GetEigenVector<Eigen::VectorXd>(
                                           covIn), mode);
  } else {
    return make_shared<mvNormDensPython>(GetEigenVector<Eigen::VectorXd>(meanIn), GetEigenMatrix(covIn), mode);
  }
}

boost::python::list mvNormDensPython::Mean() const
{
  return GetPythonVector<Eigen::VectorXd>(specification->Mean);
}

void muq::Modelling::ExportMvNormDens()
{
  // expose constructors
  boost::python::class_<mvNormDensPython, std::shared_ptr<mvNormDensPython>,
                        boost::python::bases<Density, ModPiece>, boost::noncopyable> exportMvNormDens(
    "mvNormDens",
    boost::python::
    init<std::shared_ptr<ConstantMVNormSpecification> const&>());

  exportMvNormDens.def(boost::python::init<int>());
  exportMvNormDens.def(boost::python::init<boost::python::list const&, double>());
  exportMvNormDens.def("__init__", boost::python::make_constructor(&mvNormDensPython::Create));
  exportMvNormDens.def("__init__", boost::python::make_constructor(&mvNormDensPython::CreateMode));

  exportMvNormDens.def("Mean", &mvNormDensPython::Mean);

  // convert to parent, ModPiece, and Density
  boost::python::implicitly_convertible<std::shared_ptr<mvNormDensPython>, std::shared_ptr<mvNormDens> >();
  boost::python::implicitly_convertible<std::shared_ptr<mvNormDensPython>, std::shared_ptr<ModPiece> >();
  boost::python::implicitly_convertible<std::shared_ptr<mvNormDensPython>, std::shared_ptr<Density> >();
}

