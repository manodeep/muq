#include "MUQ/Modelling/python/MvNormRVPython.h"

using namespace std;
using namespace muq::Modelling;
using namespace muq::Utilities;

mvNormRVPython::mvNormRVPython(std::shared_ptr<ConstantMVNormSpecification> const& specification) : mvNormRV(
                                                                                                      specification)
{}

mvNormRVPython::mvNormRVPython(int dimIn) : mvNormRV(dimIn) {}

mvNormRVPython::mvNormRVPython(boost::python::list const& meanIn,
                               double                     scalarCovariance) : mvNormRV(GetEigenVector<Eigen::VectorXd>(
                                                                                         meanIn),
                                                                                       scalarCovariance)
{}


mvNormRVPython::mvNormRVPython(Eigen::VectorXd const                        & meanIn,
                               Eigen::VectorXd const                        & covDiag,
                               ConstantMVNormSpecification::SpecificationMode mode) : mvNormRV(meanIn, covDiag,
                                                                                               mode)
{}

mvNormRVPython::mvNormRVPython(Eigen::VectorXd const                        & meanIn,
                               Eigen::MatrixXd const                        & cov,
                               ConstantMVNormSpecification::SpecificationMode mode) : mvNormRV(meanIn, cov,
                                                                                               mode)
{}

shared_ptr<mvNormRVPython> mvNormRVPython::Create(boost::python::list const& meanIn,
                                                  boost::python::list const& covIn,
                                                  bool                       isDiag)
{
  if (isDiag) {
    return make_shared<mvNormRVPython>(GetEigenVector<Eigen::VectorXd>(meanIn), GetEigenVector<Eigen::VectorXd>(covIn));
  } else {
    return make_shared<mvNormRVPython>(GetEigenVector<Eigen::VectorXd>(meanIn), GetEigenMatrix(covIn));
  }
}

shared_ptr<mvNormRVPython> mvNormRVPython::CreateMode(boost::python::list const                    & meanIn,
                                                      boost::python::list const                    & covIn,
                                                      bool                                           isDiag,
                                                      ConstantMVNormSpecification::SpecificationMode mode)
{
  if (isDiag) {
    return make_shared<mvNormRVPython>(GetEigenVector<Eigen::VectorXd>(meanIn), GetEigenVector<Eigen::VectorXd>(
                                         covIn), mode);
  } else {
    return make_shared<mvNormRVPython>(GetEigenVector<Eigen::VectorXd>(meanIn), GetEigenMatrix(covIn), mode);
  }
}

boost::python::list mvNormRVPython::PySampleInputs(boost::python::list const& pyInput,
                                                   int                        numSamps)
{
  vector<Eigen::VectorXd>
  input = PythonListToVector(pyInput);
  return
    GetPythonMatrix(Sample(input, numSamps));
}

boost::python::list mvNormRVPython::PySampleInputsOneSamp(boost::python::list const& pyInput)
{
  vector<Eigen::VectorXd>
  input = PythonListToVector(pyInput);
  return
    GetPythonVector<Eigen::VectorXd>(Sample(input, 1));
}

boost::python::list mvNormRVPython::PySampleOneSamp()
{
  return
    GetPythonVector<Eigen::VectorXd>(Sample());
}

boost::python::list mvNormRVPython::PySample(int numSamps)
{
  return
    GetPythonMatrix(Sample(numSamps));
}

void muq::Modelling::ExportMvNormRV()
{
  // expose constructors
  boost::python::class_<mvNormRVPython, std::shared_ptr<mvNormRVPython>,
                        boost::python::bases<RandVarPython, ModPiece>, boost::noncopyable > exportMvNormRV(
    "mvNormRV",
    boost::python::
    init<std::shared_ptr<ConstantMVNormSpecification> const&>());

  exportMvNormRV.def(boost::python::init<int>());
  exportMvNormRV.def(boost::python::init<boost::python::list const&, double>());
  exportMvNormRV.def("__init__", boost::python::make_constructor(&mvNormRVPython::Create));
  exportMvNormRV.def("__init__", boost::python::make_constructor(&mvNormRVPython::CreateMode));

  exportMvNormRV.def("Sample", &mvNormRVPython::PySampleInputs);
  exportMvNormRV.def("Sample", &mvNormRVPython::PySampleInputsOneSamp);
  exportMvNormRV.def("Sample", &mvNormRVPython::PySample);
  exportMvNormRV.def("Sample", &mvNormRVPython::PySampleOneSamp);

  exportMvNormRV.enable_pickling();

  // convert to parent, ModPiece, and RandVar
  boost::python::implicitly_convertible<std::shared_ptr<mvNormRVPython>, std::shared_ptr<mvNormRV> >();
  boost::python::implicitly_convertible<std::shared_ptr<mvNormRVPython>, std::shared_ptr<ModPiece> >();
  boost::python::implicitly_convertible<std::shared_ptr<mvNormRVPython>, std::shared_ptr<RandVar> >();
}

