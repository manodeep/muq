
#include "MUQ/Modelling/python/ConstantMVNormSpecificationPython.h"

using namespace std;
using namespace muq::Modelling;
using namespace muq::Utilities;

ConstantMVNormSpecificationPython::ConstantMVNormSpecificationPython(int dimIn) : ConstantMVNormSpecification(dimIn) {}

ConstantMVNormSpecificationPython::ConstantMVNormSpecificationPython(boost::python::list const& meanIn,
                                                                     double                     scalarCovariance) :
  ConstantMVNormSpecification(GetEigenVector<Eigen::VectorXd>(
                                meanIn),
                              scalarCovariance)
{}


ConstantMVNormSpecificationPython::ConstantMVNormSpecificationPython(
  Eigen::VectorXd const                        & meanIn,
  Eigen::VectorXd const                        & covDiag,
  ConstantMVNormSpecification::SpecificationMode mode)
   : ConstantMVNormSpecification(meanIn,
                                 covDiag,
                                 mode)
{}

ConstantMVNormSpecificationPython::ConstantMVNormSpecificationPython(
  Eigen::VectorXd const                        & meanIn,
  Eigen::MatrixXd const                        & cov,
  ConstantMVNormSpecification::SpecificationMode mode)
   : ConstantMVNormSpecification(meanIn,
                                 cov,
                                 mode)
{}

shared_ptr<ConstantMVNormSpecificationPython> ConstantMVNormSpecificationPython::Create(
  boost::python::list const& meanIn,
  boost::python::list const& covIn,
  bool                       isDiag)
{
  if (isDiag) {
    return make_shared<ConstantMVNormSpecificationPython>(GetEigenVector<Eigen::VectorXd>(meanIn),
                                                          GetEigenVector<Eigen::VectorXd>(covIn));
  } else {
    return make_shared<ConstantMVNormSpecificationPython>(GetEigenVector<Eigen::VectorXd>(meanIn),
                                                          GetEigenMatrix(covIn));
  }
}

shared_ptr<ConstantMVNormSpecificationPython> ConstantMVNormSpecificationPython::CreateMode(
  boost::python::list const                    & meanIn,
  boost::python::list const                    & covIn,
  bool                                           isDiag,
  ConstantMVNormSpecification::SpecificationMode mode)
{
  if (isDiag) {
    return make_shared<ConstantMVNormSpecificationPython>(GetEigenVector<Eigen::VectorXd>(meanIn),
                                                          GetEigenVector<Eigen::VectorXd>(covIn), mode);
  } else {
    return make_shared<ConstantMVNormSpecificationPython>(GetEigenVector<Eigen::VectorXd>(meanIn), GetEigenMatrix(
                                                            covIn), mode);
  }
}

void muq::Modelling::ExportConstantMVNormSpecification()
{
  boost::python::class_<ConstantMVNormSpecificationPython, std::shared_ptr<ConstantMVNormSpecificationPython>,
                        boost::noncopyable> exportMvNormRV("ConstantMVNormSpecification", boost::python::init<int>());

  exportMvNormRV.def(boost::python::init<boost::python::list const&, double>());
  exportMvNormRV.def("__init__", boost::python::make_constructor(&ConstantMVNormSpecificationPython::Create));
  exportMvNormRV.def("__init__", boost::python::make_constructor(&ConstantMVNormSpecificationPython::CreateMode));

  // allow conversion to parent
  boost::python::implicitly_convertible<std::shared_ptr<ConstantMVNormSpecificationPython>,
                                        std::shared_ptr<ConstantMVNormSpecification> >();

  // expose specification mode enum
  boost::python::scope X = exportMvNormRV;
  boost::python::enum_<ConstantMVNormSpecification::SpecificationMode> exportSpecMode("SpecificationMode");
  exportSpecMode.value("ScaledIdentity", ConstantMVNormSpecification::ScaledIdentity);
  exportSpecMode.value("CovarianceMatrix", ConstantMVNormSpecification::CovarianceMatrix);
  exportSpecMode.value("PrecisionMatrix", ConstantMVNormSpecification::PrecisionMatrix);
  exportSpecMode.value("DiagonalCovariance", ConstantMVNormSpecification::DiagonalCovariance);
  exportSpecMode.value("DiagonalPrecision", ConstantMVNormSpecification::DiagonalPrecision);
}
