
#include "MUQ/Modelling/EmpiricalRandVar.h"

#include <sys/time.h>
#include <stdio.h>
#include <unistd.h>
#include <assert.h>
#include <iostream>
#include <algorithm>
#include <fstream>

#include <unsupported/Eigen/FFT>

#include "MUQ/Utilities/EigenUtils.h"
#include "MUQ/Utilities/RandomGenerator.h"
#include "MUQ/Utilities/HDF5Wrapper.h"
#include "MUQ/Utilities/LogConfig.h"

#if HAVE_HDF5 == 1
# include <hdf5.h>
#endif // if HAVE_HDF5 == 1

using namespace muq::Utilities;
using namespace muq::Modelling;
using namespace std;


template<typename inType>
double Ess(const inType& trace, int const size)
{
  // must have a positive number of samples
  assert( size>=0 );

  // ESS needs at least 2 samples; just return ESS=0.0 if there aren't enough
  if( size<2 ) {
    return 0.0;
  }

  double traceMean = trace.segment(0, size).sum() / size;

  Eigen::FFT<double> fft;

  int tmax    = floor(size / 2);
  double Stau = 1.5;

  Eigen::Matrix<std::complex<double>, Eigen::Dynamic, 1> freqVec;
  Eigen::Matrix<std::complex<double>, Eigen::Dynamic,
                1> timeVec = Eigen::Matrix<std::complex<double>, Eigen::Dynamic, 1>::Zero(size + tmax);
  for (int i = 0; i < size; ++i) {
    timeVec(i) = std::complex<double>(trace(i) - traceMean, 0.0);
  }


  fft.fwd(freqVec, timeVec);


  // compute out1*conj(out2) and store in out1 (x+yi)*(x-yi)
  double real;

  for (int i = 0; i < size + tmax; ++i) {
    real       = freqVec(i).real() * freqVec(i).real() + freqVec(i).imag() * freqVec(i).imag();
    freqVec(i) = std::complex<double>(real, 0.0);
  }


  // now compute the inverse fft to get the autocorrelation (stored in timeVec)
  fft.inv(timeVec, freqVec);

  for (int i = 0; i < tmax + 1; ++i) {
    timeVec(i) = std::complex<double>(timeVec(i).real() / double(size - i), 0.0);
  }

  // the following loop uses ideas from "Monte Carlo errors with less errors." by Ulli Wolff to figure out how far we
  // need to integrate
  //int MaxLag = 0;
  double Gint = 0;
  int    Wopt = 0;
  for (int i = 1; i < tmax + 1; ++i) {
    Gint += timeVec(i).real() / timeVec(0).real(); // in1[i][0] /= scale;

    double tauW;
    if (Gint <= 0) {
      tauW = 1.0e-15;
    } else {
      tauW = Stau / log((Gint + 1) / Gint);
    }
    double gW = exp(-double(i) / tauW) - tauW / sqrt(double(i) * size);

    if (gW < 0) {
      Wopt = i;
      tmax = min(tmax, 2 * i);
      break;
    }
  }


  // correct for bias
  double CFbbopt = timeVec(0).real();
  for (int i = 0; i < Wopt + 1; ++i) {
    CFbbopt += 2 * timeVec(i + 1).real();
  }

  CFbbopt = CFbbopt / size;

  for (int i = 0; i < tmax + 1; ++i) {
    timeVec(i) += std::complex<double>(CFbbopt, 0.0);
  }

  // compute the normalized autocorrelation
  double scale = timeVec(0).real();
  for (int i = 0; i < Wopt; ++i) {
    timeVec(i) = std::complex<double>(timeVec(i).real() / scale, 0.0);
  }

  double tauint = 0;
  for (int i = 0; i < Wopt; ++i) {
    tauint += timeVec(i).real();
  }

  tauint -= 0.5;

  // return the effective sample size
  double frac = 1.0 / (2.0 * tauint);
  frac = fmin(1.0, frac);
  return size * frac;
}

/** build an empty RandVar with the given dimension*/
EmpiricalRandVar::EmpiricalRandVar(int dim) : RandVar(Eigen::VectorXi(), dim, false, false, false, false)
{
  samples = Eigen::MatrixXd::Zero(outputSize, 1);
  Nsamps  = 0;
}

/** construct the object from a single sample */
EmpiricalRandVar::EmpiricalRandVar(const Eigen::VectorXd& input) : RandVar(Eigen::VectorXi(),
                                                                           input.rows(), false, false, false, false)
{
  samples = Eigen::MatrixXd::Zero(outputSize, 1);
  Nsamps  = 0;
  addSamp(input);
}

/** build a RandVar starting with the samples in this vector.  When using this constructor, the user should make sure
 * that all vectors passed as input have the same dimension. */
EmpiricalRandVar::EmpiricalRandVar(const Eigen::MatrixXd& samps) : RandVar(Eigen::VectorXi(),
                                                                           samps.rows(), false, false, false, false)
{
  samples = Eigen::MatrixXd::Zero(outputSize, 1);
  Nsamps  = 0;
  addSamp(samps);
}

EmpiricalRandVar::~EmpiricalRandVar() {}


/** return a sample */
Eigen::VectorXd EmpiricalRandVar::EvaluateImpl(std::vector<Eigen::VectorXd> const& input)
{
  // first, generate a random integer between 0 and the number of samples we have
  int index = RandomGenerator::GetUniformInt(0, Nsamps);

  // copy this into the sample
  return samples.col(index);
}

Eigen::VectorXd EmpiricalRandVar::getQuantile(double frac) const
{
  // make sure frac is a value we expect
  assert((frac <= 1.0) && (frac >= 0));

  if (frac == 1.0) {
    return samples.block(0, 0, outputSize, Nsamps).rowwise().maxCoeff();
  } else if (frac == 0.0) {
    return samples.block(0, 0, outputSize, Nsamps).rowwise().minCoeff();
  } else {
    // a vector to store the median
    Eigen::VectorXd output(outputSize);
    int n1 = floor(frac * (Nsamps - 1));
    int n2 = ceil(frac * (Nsamps - 1));

    if (n1 == n2) {
      // compute this for each dimension
      for (int d = 0; d < outputSize; ++d) {
        Eigen::VectorXd rowSamps = samples.row(d).head(Nsamps);

        // compute the n1 sample
        double *valPtr = rowSamps.data() + n1;
        nth_element(rowSamps.data(), valPtr, rowSamps.data() + Nsamps);
        output(d) = *valPtr;
      }
    } else {
      // compute this for each dimension
      for (int d = 0; d < outputSize; ++d) {
        Eigen::VectorXd rowSamps = samples.row(d).head(Nsamps);

        // compute the n1 sample
        double *valPtr = rowSamps.data() + n1;
        double  v1;
        nth_element(rowSamps.data(), valPtr, rowSamps.data() + Nsamps);
        v1 = *valPtr;

        // compute the n2 sample
        valPtr = rowSamps.data() + n2;
        nth_element(rowSamps.data(), valPtr, rowSamps.data() + Nsamps);
        output(d) = 0.5 * (v1 + *valPtr);
      }
    }
    return output;
  }
}

/** compute and fill in the mean of the samples*/
Eigen::VectorXd EmpiricalRandVar::getMean() const
{
  Eigen::VectorXd meanOut;

  // loop through the samples and compute sum -- make pairwise and recursive in future
  meanOut  = samples.block(0, 0, samples.rows(), Nsamps).rowwise().sum();
  meanOut /= Nsamps;

  return meanOut;
}

/** compute and fill in the variance of the samples*/
Eigen::VectorXd EmpiricalRandVar::getVar() const
{
  Eigen::VectorXd mean = getMean();

  Eigen::VectorXd varOut =
    (samples.block(0, 0, samples.rows(), Nsamps).colwise() - mean).array().square().matrix().rowwise().sum();

  varOut /= (Nsamps - 1.0);

  return varOut;
}

/** compute and fill in the covariance of the samples */
Eigen::MatrixXd EmpiricalRandVar::getCov() const
{
  // first, compute the mean
  auto mean = getMean();

  //Eigen::MatrixXd meanMat = mean.replicate(1,Nsamps);
  auto diff           = samples.block(0, 0, samples.rows(), Nsamps).colwise() - mean;
  Eigen::MatrixXd cov = diff * diff.transpose();

  // scale the covariance -- unbiased estimator
  cov /= (Nsamps - 1);
  return cov;
}

/** Declare functions to add new samples */
void EmpiricalRandVar::addSamp(const Eigen::VectorXd& samp)
{
  // first, check the dimension
  assert(samp.size() == outputSize);

  Nsamps++;
  if (Nsamps > samples.cols()) {
    Eigen::MatrixXd tempMat = Eigen::MatrixXd::Zero(outputSize, Nsamps + 1);
    tempMat.block(0, 0, outputSize, samples.cols()) = samples;
    tempMat.col(Nsamps - 1)                         = samp;
    samples                                         = tempMat;

    //samples.conservativeResize(tempMat.rows(), tempMat.cols()+1);
    //samples.block(0,0,tempMat.rows(),tempMat.cols()) = tempMat;
    //samples.col(Nsamps-1) = samp;
  } else {
    samples.col(Nsamps - 1) = samp;
  }
}

/** Allocate memory for approximately N samples.*/
void EmpiricalRandVar::ExpectedSize(int N)
{
  LOG(INFO) << "expected size expanding " << N << " " << samples.cols();
  if (N > samples.cols()) {
    Eigen::MatrixXd tempMat = samples;
    samples.resize(tempMat.rows(), N);
    samples.block(0, 0, tempMat.rows(), tempMat.cols()) = tempMat;
  }
}

void EmpiricalRandVar::addSamp(const Eigen::MatrixXd& samps)
{
  ExpectedSize(Nsamps + samps.cols());

  for (int i = 0; i < samps.cols(); ++i) {
    addSamp(Eigen::VectorXd(samps.col(i)));
  }
}

/** function to access a sample */
Eigen::VectorXd EmpiricalRandVar::getSamp(int sampID) const
{
  return samples.col(sampID);
}

/** return the number of samples in the RandVar */
int EmpiricalRandVar::NumSamps() const
{
  return Nsamps;
}

/** compute and return the effective sample size, assuming the samples vector is order */
Eigen::VectorXd EmpiricalRandVar::getEss()
{
  // create a vector for the effective sample size over each dimension
  Eigen::VectorXd output(outputSize);

  // loop through the dimensions, computing the effective sample size for each
  for (int i = 0; i < outputSize; ++i) {
    output(i) = Ess(samples.row(i), Nsamps);
  }

  return output;
}

/** write sample to a raw binary file. */
void EmpiricalRandVar::WriteRawBinary(const string& file) const
{
  // open the file
  ofstream binOut;

  binOut.open(file.c_str(), ios::binary);
  assert(binOut.good());

  // write an integer specifying the number of samples
  binOut.write((char *)&Nsamps, sizeof(int));


  // write an integer with the sample dimension
  binOut.write((char *)&outputSize, sizeof(int));

  // write all the samples to the file
  for (int i = 0; i < Nsamps; ++i) {
    binOut.write((char *)&(samples(0, i)), outputSize * sizeof(double));
  }

  // close the file
  binOut.close();
}

void EmpiricalRandVar::WriteHDF5(const std::string& hdf5Path) const
{
  HDF5Wrapper::WriteMatrix(hdf5Path, GetAllSamples());
}

std::shared_ptr<EmpiricalRandVar> EmpiricalRandVar::CreateFromRawBinary(const string& file)
{
  // open the file
  ifstream binIn;

  binIn.open(file.c_str(), ios::binary);
  assert(binIn.good());

  int NumSamps;
  // write an integer specifying the number of samples
  binIn.read((char *)&NumSamps, sizeof(int));


  // write an integer with the sample dimension
  int dimension;
  binIn.read((char *)&dimension, sizeof(int));

  auto output = make_shared<EmpiricalRandVar>(dimension);
  output->ExpectedSize(NumSamps);

  // write all the samples to the file
  binIn.read((char *)output->samples.data(), NumSamps * dimension * sizeof(double));


  // close the file
  binIn.close();

  return output;
}

void EmpiricalRandVar::SwapLastSample(EmpRvPtr otherSample)
{
  Eigen::VectorXd temp(otherSample->samples.col(otherSample->Nsamps - 1));

  otherSample->samples.col(Nsamps - 1) = samples.col(Nsamps - 1);
  samples.col(Nsamps - 1)              = temp;
}

void EmpiricalRandVar::WriteRawText(const std::string& file) const
{
  muq::Utilities::PrintMatrixToFile(GetAllSamples(), file);
}

Eigen::MatrixXd EmpiricalRandVar::GetAllSamples() const
{
  return samples.leftCols(Nsamps);
}

#if MUQ_PYTHON == 1

boost::python::list EmpiricalRandVar::PyGetMean() const
{
  return GetPythonVector<Eigen::VectorXd>(getMean());
}

boost::python::list EmpiricalRandVar::PyGetCov() const
{
  return GetPythonMatrix(getCov());
}

void EmpiricalRandVar::PyAddSamp(boost::python::list const& samp)
{
  addSamp(GetEigenVector<Eigen::VectorXd>(samp));
}

void EmpiricalRandVar::PyAddSamps(boost::python::list const& samps)
{
  addSamp(GetEigenMatrix(samps));
}

boost::python::list EmpiricalRandVar::PyGetSamp(int sampID) const
{
  return GetPythonVector<Eigen::VectorXd>(getSamp(sampID));
}

boost::python::list EmpiricalRandVar::PyGetAllSamples()
{
  return GetPythonMatrix<double>(GetAllSamples().transpose());
}

boost::python::list EmpiricalRandVar::PyGetEss() 
{
  return GetPythonVector<Eigen::VectorXd>(getEss());
}

#endif // if MUQ_PYTHON == 1
