#include "MUQ/Modelling/ModPieceTemplates.h"

using namespace muq::Modelling;


OneInputTemplate::OneInputTemplate(int                inputSize,
                                   int                outputSize,
                                   bool const         hasDirectGradient,
                                   bool const         hasDirectJacobian,
                                   bool const         hasDirectJacobianAction,
                                   bool const         hasDirectHessian,
                                   bool const         isRandom,
                                   std::string const& name) : ModPiece(inputSize * Eigen::VectorXi::Ones(1),
                                                                       outputSize,
                                                                       hasDirectGradient,
                                                                       hasDirectJacobian,
                                                                       hasDirectJacobianAction,
                                                                       hasDirectHessian,
                                                                       isRandom,
                                                                       name)
{};

Eigen::MatrixXd OneInputTemplate::EvaluateMultiImpl(Eigen::MatrixXd const& input)
{
  const int numCopies = input.cols();
  Eigen::MatrixXd output(outputSize,numCopies);
  
  for(int i=0; i<numCopies; ++i)
  output.col(i) = EvaluateImpl(input.col(i));
  
  return output;
};

Eigen::MatrixXd OneInputTemplate::EvaluateMultiImpl(std::vector<Eigen::MatrixXd> const& input)
{
  return EvaluateMultiImpl(input.at(0));
};

Eigen::VectorXd OneInputTemplate::EvaluateImpl(std::vector<Eigen::VectorXd> const& input)
{
  return EvaluateImpl(input.at(0));
};


OneInputNoDerivModPiece::OneInputNoDerivModPiece(int const inputSize, int const outputSize) : OneInputTemplate(inputSize,
                                                                                                               outputSize,
                                                                                                               false,
                                                                                                               false,
                                                                                                               false,
                                                                                                               false,
                                                                                                               false)
{};


WrapVectorFunctionModPiece::WrapVectorFunctionModPiece(std::function<Eigen::VectorXd(Eigen::VectorXd const&)> fn,
                           int const inputSize,
                           int const outputSize) : OneInputNoDerivModPiece(inputSize,outputSize),
fn(fn)
{}

Eigen::VectorXd WrapVectorFunctionModPiece::EvaluateImpl(Eigen::VectorXd const& input)
{
  return fn(input);
}



OneInputAdjointModPiece::OneInputAdjointModPiece(int const inputSize, int const outputSize) : OneInputTemplate(inputSize, outputSize,
                                                                                      true, false,false,false,false)
{}


Eigen::VectorXd OneInputAdjointModPiece::GradientImpl(std::vector<Eigen::VectorXd> const& input,
                                                      Eigen::VectorXd const             & sensitivity,
                                                      int const                           inputDimWrt)
{
  return GradientImpl(input.at(0), sensitivity);
}


OneInputJacobianModPiece::OneInputJacobianModPiece(int const inputSize, int const outputSize) : OneInputTemplate(inputSize, outputSize,
                                                                                       false,true,false,false,false)
{}


///The jacobian is outputDim x inputDim
Eigen::MatrixXd OneInputJacobianModPiece::JacobianImpl(std::vector<Eigen::VectorXd> const& input, int const inputDimWrt)
{
  assert(inputDimWrt == 0);
  return JacobianImpl(input.at(0));
}


OneInputAdjointJacobianModPiece::OneInputAdjointJacobianModPiece(int const inputSize, int const outputSize) : OneInputTemplate(inputSize,outputSize,
                                                                                              true,
                                                                                              true,
                                                                                              true,
                                                                                              false,
                                                                                              false)
{}


///The jacobian is outputDim x inputDim
Eigen::MatrixXd OneInputAdjointJacobianModPiece::JacobianImpl(std::vector<Eigen::VectorXd> const& input, int const inputDimWrt)
{
  assert(inputDimWrt == 0);
  return JacobianImpl(input.at(0));
}

Eigen::VectorXd OneInputAdjointJacobianModPiece::GradientImpl(std::vector<Eigen::VectorXd> const& input,
                                     Eigen::VectorXd const             & sensitivity,
                                     int const                           inputDimWrt)
{
  return GradientImpl(input.at(0), sensitivity);
}


Eigen::VectorXd OneInputAdjointJacobianModPiece::JacobianActionImpl(std::vector<Eigen::VectorXd> const& input,
                                           Eigen::VectorXd const             & target,
                                           int const                           inputDimWrt)
{
  return JacobianActionImpl(input.at(0), target);
}



OneInputFullModPiece::OneInputFullModPiece(int const inputSize, int const outputSize) : OneInputTemplate(inputSize, outputSize,
                                                                                   true,true,true,true,false)
{}


Eigen::VectorXd OneInputFullModPiece::GradientImpl(std::vector<Eigen::VectorXd> const& input,
                                     Eigen::VectorXd const             & sensitivity,
                                     int const                           inputDimWrt)
{
  return GradientImpl(input.at(0), sensitivity);
}

Eigen::MatrixXd OneInputFullModPiece::JacobianImpl(std::vector<Eigen::VectorXd> const& input, int const inputDimWrt)
{
  return JacobianImpl(input.at(0));
}

Eigen::VectorXd OneInputFullModPiece::JacobianActionImpl(std::vector<Eigen::VectorXd> const& input,
                                           Eigen::VectorXd const             & target,
                                           int const                           inputDimWrt)
{
  return JacobianActionImpl(input.at(0), target);
}

Eigen::MatrixXd OneInputFullModPiece::HessianImpl(std::vector<Eigen::VectorXd> const& input,
                                    Eigen::VectorXd const             & sensitivity,
                                    int const                           inputDimWrt)
{
  return HessianImpl(input.at(0), sensitivity);
}
