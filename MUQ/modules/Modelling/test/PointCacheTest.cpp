#include <boost/property_tree/ptree.hpp> // needed here to avoid weird osx "toupper" bug when compiling MUQ with python
#include "gtest/gtest.h"

#include "MUQ/Utilities/EigenTestUtils.h"

#include "MUQ/Modelling/PointCache.h"

using namespace std;
using namespace muq::Utilities;
using namespace muq::Modelling;

TEST(PointCache, BasicTest) 
{
  const unsigned int N = 5; // number of points

  Eigen::MatrixXd inputs  = Eigen::MatrixXd::Random(3, N);
  Eigen::MatrixXd outputs = Eigen::MatrixXd::Random(2, N);

  auto pntCache = make_shared<PointCache>(inputs, outputs);

EXPECT_PRED_FORMAT2(MatrixEqual, outputs.col(2), pntCache->Evaluate(inputs.col(2)));
}
