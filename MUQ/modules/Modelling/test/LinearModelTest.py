import unittest
import numpy as np

import libmuqModelling

class LinearModelTest(unittest.TestCase):
    def testMatVecEval(self):
        dim = 20
        
        # matrix
        A = [[np.random.random() for i in range(dim)] for j in range(dim)]

        # model
        mod = libmuqModelling.LinearModel(A)

        # input
        x = [np.random.random() for i in range(dim)]

        # compute
        truth    = np.dot(np.array(A), np.array(x)).tolist()
        computed = mod.Evaluate(x)

        # compare
        for i in range(dim):
            self.assertAlmostEqual(truth[i], computed[i], 10)

    def testAdditiveEval(self):
        dim = 20
        
        # vector
        b = [np.random.random() for i in range(dim)]

        # model
        mod = libmuqModelling.LinearModel(b, True)

        # input
        x = [np.random.random() for i in range(dim)]

        # compute
        truth    = (np.array(b) + np.array(x)).tolist()
        computed = mod.Evaluate(x)

        # compare
        for i in range(dim):
            self.assertAlmostEqual(truth[i], computed[i], 10)

    def testMatVecAdditiveEval(self):
        dim = 20
        
        # matrix and vector
        A = [[np.random.random() for i in range(dim)] for j in range(dim)]
        b = [np.random.random() for i in range(dim)]

        # model
        mod = libmuqModelling.LinearModel(b, A)

        # input
        x = [np.random.random() for i in range(dim)]

        # compute
        truth    = (np.dot(np.array(A),np.array(x))+np.array(b)).tolist()
        computed = mod.Evaluate(x)

        # compare
        for i in range(dim):
            self.assertAlmostEqual(truth[i], computed[i], 10)

