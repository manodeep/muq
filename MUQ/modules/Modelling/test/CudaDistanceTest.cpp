
#include <iostream>

#include <Eigen/Core>

// google testing library
#include "gtest/gtest.h"

// muq related includes
#include "MUQ/Modelling/CudaDistance.h"

using namespace std;


TEST(ModellingCudaDistanceTest, BasicSmall)
{
  int Npts = 22270;

  // create an matrix full of 2 dimensional points
  Eigen::MatrixXd pts(2, Npts);

  for (int i = 0; i < Npts; ++i) {
    pts(0, i) = 100 * double(i) / Npts;
    pts(1, i) = 100 * double(i) / Npts;
  }

  // compute the distance between these points
  Eigen::MatrixXd dists(Npts, Npts);
  ComputeDistanceMat(2, Npts, pts.data(), dists.data());

  //   for(int k=0; k<Npts; ++k){
  //     for(int i=0; i<Npts; ++i){
  //      Eigen::VectorXd diff = pts.col(i)-pts.col(k);
  //       EXPECT_DOUBLE_EQ(diff.transpose()*diff,dists(k,i));
  //     }
  //   }
}
