#include "MUQ/Approximation/Regression/LowOrderPolynomialRegressor.h"

#include <boost/property_tree/ptree.hpp>
#include <Eigen/Eigenvalues>

#include "MUQ/Utilities/LogConfig.h"
#include "MUQ/Utilities/RandomGenerator.h"

#include "MUQ/Approximation/Regression/LinearRegressorHelper.h"
#include "MUQ/Approximation/Regression/QuadraticRegressorHelper.h"

#include "MUQ/Utilities/HDF5Logging.h"

using namespace std;
using namespace Eigen;
using namespace muq::Utilities;
using namespace muq::Approximation;

REGISTER_REGRESS_DEF_TYPE(LowOrderPolynomialRegressor);

struct muq::Approximation::LowOrderPolynomialFitResult {
  LowOrderPolynomialFitResult(std::shared_ptr<muq::Modelling::ModPiece> primaryFit,
                              std::vector < std::shared_ptr < muq::Modelling::ModPiece >>
                              crossValidationFits) : primaryFit(primaryFit),
                                                     crossValidationFits(
                                                       crossValidationFits)
  {}

  std::shared_ptr<muq::Modelling::ModPiece> primaryFit;
  std::vector < std::shared_ptr < muq::Modelling::ModPiece >> crossValidationFits;
};


Eigen::VectorXd LowOrderPolynomialRegressor::EvaluateImpl(const Eigen::VectorXd& x)
{
  assert(currentFit);             //check there is a fit
  assert(currentFit->primaryFit); //check there is a fit
  return currentFit->primaryFit->Evaluate(x);
}

Eigen::MatrixXd LowOrderPolynomialRegressor::JacobianImpl(const Eigen::VectorXd& x)
{
  assert(currentFit);             //check there is a fit
  assert(currentFit->primaryFit); //check there is a fit
  return currentFit->primaryFit->Jacobian(x);
}

std::map<std::string,
         LowOrderPolynomialRegressor::FitTypes> LowOrderPolynomialRegressor::FitTypesMap =
{ { "linear", linear }, { "quadratic", quadratic } };

LowOrderPolynomialRegressor::LowOrderPolynomialRegressor(int                                dimIn,
                                                         int                                dimOut,
                                                         boost::property_tree::ptree const& properties) : RegressorBase(
                                                                                                            dimIn,
                                                                                                            dimOut)
{
  LOG(INFO) << "Construct LowOrderPolynomialRegressor";
  string fitTypeName = ReadAndLogParameter<string>(properties, "Regressor.LowOrderPolynomial.FitType", "quadratic");
  fitType = FitTypesMap.at(fitTypeName);

  maxEmpiricalCVSamples = ReadAndLogParameter(properties,
                                              "Regressor.LowOrderPolynomial.MaxEmpiricalCVSamples",
                                              maxEmpiricalCVSamples);
}

void LowOrderPolynomialRegressor::Fit(const Eigen::MatrixXd& xs, const Eigen::MatrixXd& ys)
{
  //just call through with no center
  FitWithCenter(xs, ys, Eigen::VectorXd::Zero(xs.rows()), 0);
  InvalidateCache();
}

void LowOrderPolynomialRegressor::FitWithCenter(const Eigen::MatrixXd& xs,
                                                const Eigen::MatrixXd& ys,
                                                const Eigen::VectorXd& center,
                                                double const           radius)
{
  //check that there are enough samples
  assert(xs.cols() >= NumberOfPointsToInterpolate() + 1);
  assert(xs.cols() == ys.cols());
  assert(xs.rows() == inputSizes(0));


  auto normalizedSamples = ShiftAndScalePoints(xs, center, radius);

  MatrixXd weight = ComputeTricubeWeightSqrt(normalizedSamples.first, NumberOfPointsToInterpolate());

  //normally this would go on the left, but here it goes on the right
  MatrixXd weightedYs = ys * weight;

  //compute the appropriate regression matrix
  MatrixXd fullRegressionMatrix;

  if (fitType == quadratic) {
    fullRegressionMatrix = weight * QuadraticRegressorHelper::ComputeRegressionMatrix(normalizedSamples.first);
  } else {
    fullRegressionMatrix = weight * LinearRegressorHelper::ComputeRegressionMatrix(normalizedSamples.first);
  }


  JacobiSVD<MatrixXd> svd(fullRegressionMatrix, ComputeThinU | ComputeThinV);
  auto solvedCoeffs = svd.solve(weightedYs.transpose());

  if (fitType == quadratic) {
    auto primaryFit =  QuadraticRegressorHelper::UnwrapCoefficients(inputSizes(
                                                                      0), solvedCoeffs, normalizedSamples.second,
                                                                    center);

    //no cv fits
    currentFit =  make_shared<LowOrderPolynomialFitResult>(primaryFit,
                                                           std::vector < std::shared_ptr < muq::Modelling::ModPiece >>
                                                           ());
  } else {
    auto primaryFit = LinearRegressorHelper::UnwrapCoefficients(inputSizes(
                                                                  0), solvedCoeffs, normalizedSamples.second, center);

    //no cv fits
    currentFit =  make_shared<LowOrderPolynomialFitResult>(primaryFit,
                                                           std::vector < std::shared_ptr < muq::Modelling::ModPiece >>
                                                           ());
  }

  InvalidateCache();
}

///local utility function
MatrixXd RemoveRow(MatrixXd mat, int rowIndex)
{
  MatrixXd removed(mat.rows() - 1, mat.cols());

  removed << mat.topRows(rowIndex), mat.bottomRows(mat.rows() - rowIndex - 1);
  return removed;
}

///Based on fast updates here: http://eprints.ma.man.ac.uk/1192/01/covered/MIMS_ep2008_111.pdf
void ReSolveWithRemovedRow(ColPivHouseholderQR<MatrixXd>& lhs_qr, MatrixXd const&  rhs, int rowToRemove, MatrixXd& dst)
{
  MatrixXd r =  lhs_qr.matrixQR().triangularView<Upper>();

  //    LOG(INFO) << "r " << r;

  assert(r.rows() == rhs.rows());
  assert(rowToRemove < r.rows());
  assert(r.rows() > r.cols());
//   assert(lhs_qr.rank() == r.cols()); //if not full rank, we might have to deal with zero pivots that become non-zero?


  MatrixXd newRhs = rhs;

  newRhs.applyOnTheLeft(lhs_qr.householderQ().transpose());


  //Get the row of Q we need by applying it to a unit vector
  VectorXd q_to_zero = VectorXd::Unit(rhs.rows(), rowToRemove);
  q_to_zero.applyOnTheLeft(lhs_qr.householderQ().transpose());


  //Each iteration, work on
  JacobiRotation<double> rot;                          //q_to_zero(i), q_to_zero(i+1));
  for (int i = q_to_zero.rows() - 1; i > 0; --i) {
    rot.makeGivens(q_to_zero(i - 1), q_to_zero(i));
    q_to_zero.applyOnTheLeft(i - 1, i, rot.adjoint()); //the actual vector defining the work we have to do
    newRhs.applyOnTheLeft(i - 1, i, rot.adjoint());    //rotate the rhs
    r.applyOnTheLeft(i - 1, i, rot.adjoint());         //work on R
  }


  r.block(1, 0, r.cols(), r.cols()).triangularView<Upper>().solveInPlace(newRhs.block(1, 0, r.cols(), newRhs.cols()));
  //    return newRhs.block(1,0,r.cols(), newRhs.cols());


  dst.resize(r.cols(), rhs.cols());

  //Now undo the column pivoting, offset by 1, since we're discarding the first row
  for (int i = 0; i < lhs_qr.nonzeroPivots(); ++i) {
    dst.row(lhs_qr.colsPermutation().indices().coeff(i)) = newRhs.row(i + 1);
  }
      for(int i = lhs_qr.nonzeroPivots(); i < lhs_qr.cols(); ++i) {
		  dst.row(lhs_qr.colsPermutation().indices().coeff(i)).setZero();
	  }

  //    return dst;
}

void LowOrderPolynomialRegressor::FitWithCrossValidation(const Eigen::MatrixXd& xs, const Eigen::MatrixXd& ys)
{
  FitWithCrossValidationWithCenter(xs, ys, VectorXd::Zero(inputSizes(0)), 0);
  InvalidateCache();
}

void LowOrderPolynomialRegressor::FitWithCrossValidationWithCenter(const Eigen::MatrixXd& xs,
                                                                   const Eigen::MatrixXd& ys,
                                                                   const Eigen::VectorXd& center,
                                                                   double const           radius)
{
  //check that there are enough samples
  assert(xs.cols() >= NumberOfPointsToInterpolate() + 1);
  assert(xs.cols() == ys.cols());
  assert(xs.rows() == inputSizes(0));


  auto normalizedSamples = ShiftAndScalePoints(xs, center, radius);

  MatrixXd weight = ComputeTricubeWeightSqrt(normalizedSamples.first, NumberOfPointsToInterpolate());

  //normally this would go on the left, but here it goes on the right
  MatrixXd weightedYs = ys * weight;


  //compute the appropriate regression matrix
  MatrixXd fullRegressionMatrix;

  if (fitType == quadratic) {
    fullRegressionMatrix = weight * QuadraticRegressorHelper::ComputeRegressionMatrix(normalizedSamples.first);
  } else {
    fullRegressionMatrix = weight * LinearRegressorHelper::ComputeRegressionMatrix(normalizedSamples.first);
  }

  ColPivHouseholderQR<MatrixXd> fullqr(fullRegressionMatrix);
  auto solvedCoeffs = fullqr.solve(weightedYs.transpose());

  //create an actual model from the primary fit
  std::shared_ptr<muq::Modelling::ModPiece> primaryFit;

  if (fitType == quadratic) {
    primaryFit = QuadraticRegressorHelper::UnwrapCoefficients(inputSizes(
                                                                0), solvedCoeffs, normalizedSamples.second, center);
  } else {
    primaryFit =
      LinearRegressorHelper::UnwrapCoefficients(inputSizes(0), solvedCoeffs, normalizedSamples.second, center);
  }

  //a list for the cross validation fits
  std::vector < std::shared_ptr < muq::Modelling::ModPiece >> crossValidationFits;

  //number of CV samples is either the max from the class or one for each sample provided
  int numProvidedSamples = xs.cols();
  unsigned numSamples    = static_cast<unsigned>(std::min(numProvidedSamples, static_cast<int>(maxEmpiricalCVSamples)));

  //a column with indices we'll try leave one out cross validation with
  VectorXi sampledRows(numSamples); //important constant, try at most 20 cross validations

  //generate a list of random integers
  //from stack overflow, kunth algorithm
  //http://stackoverflow.com/questions/1608181/unique-random-numbers-in-an-integer-array-in-the-c-programming-language
  int im = 0;
  for (int in = 0; in < static_cast<int>(numSamples) && im < sampledRows.rows(); ++in) {
    int rn = static_cast<int>(numSamples) - in;
    int rm = sampledRows.rows() - im;
    if (RandomGenerator::GetUniform() < static_cast<double>(rm) / static_cast<double>(rn)) {
      sampledRows(im++) = in;
    }
  }
  assert(im == sampledRows.rows());


  MatrixXd cvSolvedCoeffs; //where the result goes
  for (MatrixXd::Index j = 0; j < sampledRows.rows(); ++j) {
    MatrixXd::Index i = sampledRows(j);


    ReSolveWithRemovedRow(fullqr, weightedYs.transpose(), i, cvSolvedCoeffs);

    if (fitType == quadratic) {
      crossValidationFits.push_back(QuadraticRegressorHelper::UnwrapCoefficients(inputSizes(0), cvSolvedCoeffs,
                                                                                 normalizedSamples.second, center));
    } else {
      crossValidationFits.push_back(LinearRegressorHelper::UnwrapCoefficients(inputSizes(0), cvSolvedCoeffs,
                                                                              normalizedSamples.second, center));
    }
  }

  currentFit =  make_shared<LowOrderPolynomialFitResult>(primaryFit, crossValidationFits);
  InvalidateCache();
}

Eigen::MatrixXd LowOrderPolynomialRegressor::GetCrossValidationSamples(Eigen::VectorXd const& x)
{
  assert(currentFit);                                             //check there is a fit
  assert(currentFit->crossValidationFits.size() > 0);             //check there is a fit


  int actualNumToReturn = currentFit->crossValidationFits.size(); //return all

  MatrixXd cvSamples(outputSize, actualNumToReturn);

  for (int i = 0; i < actualNumToReturn; ++i) {
    cvSamples.col(i) = currentFit->crossValidationFits.at(i)->Evaluate(x);
  }

  return cvSamples;
}

std::vector < std::shared_ptr < muq::Modelling::ModPiece >> LowOrderPolynomialRegressor::GetCrossValidationFits()
{
  assert(currentFit);                                 //check there is a fit
  assert(currentFit->crossValidationFits.size() > 0); //check there is a fits
  return currentFit->crossValidationFits;
}

int LowOrderPolynomialRegressor::NumberOfPointsToInterpolate()
{
  if (fitType == quadratic) {
    return QuadraticRegressorHelper::NumberOfPointsToInterpolate(inputSizes(0));
  } else {
    return LinearRegressorHelper::NumberOfPointsToInterpolate(inputSizes(0));
  }
}

///Compute the sqrt because that's what's needed for making an ordinary least squares problem
MatrixXd LowOrderPolynomialRegressor::ComputeTricubeWeightSqrt(MatrixXd const& centeredSamples,
                                                               int const       numberBeforeFadeOut)
{
  VectorXd diagWeights = centeredSamples.colwise().norm();


  VectorXd sortedDistances = diagWeights;
  std::sort(sortedDistances.data(), sortedDistances.data() + sortedDistances.size());

  double fadeOutDistance = sortedDistances(numberBeforeFadeOut);

  double maxDistance  = diagWeights.maxCoeff();
  double fadeOutWidth = (maxDistance - fadeOutDistance);

  //rewrite the vector with the tricube weights
  for (unsigned int i = 0; i < centeredSamples.cols(); ++i) {
    if (diagWeights(i) < fadeOutDistance) {
        diagWeights(i) = 1.0;
    } else {
      if (fadeOutWidth == 0.0) { //special case for when fadeOutWidth is 0
        diagWeights(i) = 0;
      } else {
        diagWeights(i) = pow(1 - pow(abs((diagWeights(i) - fadeOutDistance) / fadeOutWidth), 3), 1.5);
      }
    }
  }


  return diagWeights.asDiagonal();
}

std::pair<Eigen::MatrixXd, double> LowOrderPolynomialRegressor::ShiftAndScalePoints(Eigen::MatrixXd const& points,
                                                                                    Eigen::VectorXd const& center,
                                                                                    double const           expectedRadius)
{
  MatrixXd centered = points.colwise() - center;

  //scaling could be zero if points is just one point, the center
  double scaling = centered.colwise().norm().maxCoeff();


    assert(!(scaling == 0 && expectedRadius == 0));

  //if the scaling is nonzero, use it
  if (scaling > 0) {
    return std::pair<Eigen::MatrixXd, double>(centered.array() / scaling, scaling);
  } else {
    //otherwise, just return the one point
    assert(points.cols() == 1);                                          //I think this is necessary for this to happen
                                                                         // - else something else may have gone wrong

    return std::pair<Eigen::MatrixXd, double>(centered, expectedRadius); //don't rescale, go with the expected value
  }
}

Eigen::MatrixXd LowOrderPolynomialRegressor::UnShiftAndScalePoints(Eigen::MatrixXd const& normalizedPoints,
                                                                   Eigen::VectorXd const& center,
                                                                   double const           radius)
{
  return (normalizedPoints.array() * radius).matrix().colwise() + center;
}

