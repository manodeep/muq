#include "MUQ/Modelling/CachedModPiece.h" // this needs to be here to avoid a weird "toupper" bug on osx when compiling with python

#include "MUQ/Approximation/smolyak/SmolyakQuadrature.h"

#include <string.h>
#include <iostream>
#include <fstream>

#include <boost/property_tree/ptree.hpp>
#include <boost/serialization/base_object.hpp>
#include <boost/serialization/export.hpp>
#include <boost/archive/text_oarchive.hpp>
#include <boost/archive/text_iarchive.hpp>
#include <boost/serialization/shared_ptr.hpp>
#include <boost/function.hpp>

#include "MUQ/Utilities/LogConfig.h"

#include "MUQ/Modelling/ModPiece.h"
#include "MUQ/Utilities/Quadrature/FullTensorQuadrature.h"
#include "MUQ/Utilities/Quadrature/QuadratureFamily1D.h"
#include "MUQ/Utilities/VariableCollection.h"
#include "MUQ/Utilities/multiIndex/MultiIndexSet.h"

#include "MUQ/Approximation/utilities/PolychaosXMLHelpers.h"

using namespace std;
using namespace Eigen;
using namespace muq::Utilities;
using namespace muq::Modelling;
using namespace muq::Approximation;

SmolyakQuadrature::SmolyakQuadrature() {}

SmolyakQuadrature::SmolyakQuadrature(boost::property_tree::ptree const& pt,
                                     std::shared_ptr<muq::Modelling::ModPiece> inFn) : SmolyakQuadrature(ConstructVariableCollection(pt),inFn){};

SmolyakQuadrature::SmolyakQuadrature(VariableCollection::Ptr inVariables,
                                     shared_ptr<ModPiece>    inFnWrapper) : SmolyakEstimate<VectorXd>(inVariables->length(), GetLimiter(inVariables)),
                                                                            variables(inVariables), fnWrapper(inFnWrapper)
{
  assert(static_cast<int>(variables->length()) == fnWrapper->inputSizes(0)); //check that the vars and fn match
}

SmolyakQuadrature::~SmolyakQuadrature() {}


std::shared_ptr<MultiIndexLimiter> SmolyakQuadrature::GetLimiter(std::shared_ptr<muq::Utilities::VariableCollection> inVariables)
{
  Eigen::VectorXi maxQuads(inVariables->length());
  for(int i=0; i<inVariables->length(); ++i)
    maxQuads(i) = inVariables->GetVariable(i)->quadFamily->GetMaximumPolyOrder();
  
  return make_shared<MaxOrderLimiter>(maxQuads);
  
}

// SmolyakQuadrature::Ptr SmolyakQuadrature::LoadProgress(string baseName, boost::function<Eigen::VectorXd
// (Eigen::VectorXd const&)> fn)
// {
//      SmolyakQuadrature::Ptr progress(new SmolyakQuadrature());
//              // create and open an archive for input
//              std::string fullName = baseName;
//              std::ifstream ifs(fullName.c_str());
//              boost::archive::text_iarchive ia(ifs);
//              // read class state from archive
//              ia >> progress;
//
//              progress->fnWrapper->SetVectorFunction(fn);
//              return progress;
// }
//
// void SmolyakQuadrature::SaveProgress(SmolyakQuadrature::Ptr quad, string baseName)
// {
//
//              std::string fullString = baseName;
//
//              std::ofstream ofs(fullString.c_str());
//
//              if(!ofs.good())
//              {
//               LOG(ERROR) << "SmolyakQuadrature::SaveProgress file name not valid. Falling back to
// \"quadratureProgress.dat\"";
//              std::ofstream ofs("quadratureProgress.dat");
//              }
//              assert(ofs.good());
//
//              // save data to archive
//              {
//                      boost::archive::text_oarchive oa(ofs);
//                      // write class instance to archive
//                      oa << quad;
//                      // archive and stream closed when destructors are called
//              }
//      }


VectorXd SmolyakQuadrature::ComputeOneEstimate(RowVectorXu const& multiIndex)
{
  //Else, one estimate is the full tensor quadrature of the given order
  FullTensorQuadrature quad(variables, multiIndex + RowVectorXu::Ones(multiIndex.cols()));

  return quad.ComputeIntegral(fnWrapper);
}

double SmolyakQuadrature::CostOfOneTerm(Eigen::RowVectorXu const& multiIndex)
{
  FullTensorQuadrature quad(variables, multiIndex + RowVectorXu::Ones(multiIndex.cols()));
  MatrixXd nodes;
  VectorXd weights;

  quad.GetNodesAndWeights(nodes, weights);
  auto cachedPtr = dynamic_pointer_cast<CachedModPiece>(fnWrapper);
  if (!cachedPtr) {
    return nodes.cols();
  } else {
    return cachedPtr->CostOfEvaluations(nodes);
  }
}

VectorXd SmolyakQuadrature::WeightedSumOfEstimates(VectorXd const& weights) const
{
  BOOST_LOG_POLY("smolyak", debug) << "Computing a weighted sum";

  Eigen::VectorXd output = Eigen::VectorXd::Zero(fnWrapper->outputSize);
  for (unsigned int i = 0; i < weights.size(); i++) // could probably replace with std::accumulate
    output += weights(i)*termEstimates.at(i);
  
  return output;
}

double SmolyakQuadrature::ComputeMagnitude(VectorXd estimate)
{
  //return the largest magnitude component
  return estimate.array().abs().matrix().maxCoeff();
}

MatrixXu SmolyakQuadrature::GetEffectiveIncludedTerms()
{
  MatrixXu result = MatrixXu::Zero(termsIncluded->GetNumberOfIndices(), termsIncluded->GetMultiIndexLength());

  for (unsigned int i = 0; i < termsIncluded->GetNumberOfIndices(); ++i) {
    RowVectorXu multiIndex = termsIncluded->IndexToMulti(i);
    for (unsigned int j = 0; j < termsIncluded->GetMultiIndexLength(); ++j) {
      result(i, j) = variables->GetVariable(j)->quadFamily->GetPrecisePolyOrder(multiIndex(j) + 1);
    }
  }

  return result;
}

unsigned int SmolyakQuadrature::ComputeWorkDone() const
{
  auto cachedPtr = dynamic_pointer_cast<CachedModPiece>(fnWrapper);

  if (!cachedPtr) {
    LOG(INFO) << "Work done metric not valid!";
    return -1;
  } else {
    return cachedPtr->GetNumOfEvals();
  }
}

// template<class Archive>
// void SmolyakQuadrature::serialize(Archive & ar, const unsigned int version)
// {
//      ar & boost::serialization::base_object<SmolyakEstimate<VectorXd> >(*this);
//      ar & variables;
//      ar & fnWrapper;
// }
//
// BOOST_CLASS_EXPORT(SmolyakQuadrature)
