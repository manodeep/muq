
##########################################
#  Objects to build


# files containing google tests
set(test_approximation_sources

    Approximation/test/RegressorTest.cpp
    Approximation/test/RegressionTest.cpp
    
    Approximation/test/PolynomialExpansionTest.cpp
    Approximation/test/PolynomialChaosExpansionTest.cpp

    Approximation/test/SmolyakQuadratureTest.cpp
    Approximation/test/SmolyakPCEFactoryTest.cpp

    Approximation/test/FullTensorPCETest.cpp
)

if(MUQ_USE_NLOPT)
    set(test_approximation_sources
        ${test_approximation_sources}
    Approximation/test/PolynomialApproximatorTest.cpp

	)
	
	#this file requires both inference and approximation
	if(Inference_build)
	set(LongTest_Inference_Sources
	    Approximation/test/ApproximateMCMCTests.cpp
      PARENT_SCOPE
	)
	endif(Inference_build)
	
endif(MUQ_USE_NLOPT)

set(test_approximation_sources
     ${test_approximation_sources}

     PARENT_SCOPE
)



